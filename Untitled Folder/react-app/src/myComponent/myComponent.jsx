import React, { Component } from 'react';

class MyComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageUrl: 'http://picsum.photos/500',
      count: 1,
      arr: [1, "dfs", 2, "fsf", 3, "fdf"],
      name: ['raj', 'ajldf', 'lajfkd'],
      isShow:true
    };
 this.handleIncreament = this.handleIncreament.bind(this);

  };
 

  styles = {
    color: 'orange'
  }

  renderMethod() {
    console.log();
    if (this.state.arr.length === 0) return <p>your list is empty please send another</p>;// the length of arr is then
    return <ol>
      {this.state.arr.map(array => <li key={array}>{array}</li>)}
    </ol>;//so it will render over the list

  }
  handleIncreament (product) {   ///arrow (=>) fuction rebinds the this to the method (object) return the object.
    console.log('we increament the value',this);
    this.setState({ count: this.state.count + 1 }); //for updating the value of the state properties we use setState and inside 
    // this.setState({ name: product });
    if (this.state.count >= 4){
      this.setState({ isShow:false });
    }
    console.log(product);
    //we send the object   
  }
  formatCounter = () => {
    const { count } = this.state; //It is a method destructuring  instead of this.state.count we replace with count
    const v = count === 0 ? "zero" : count;
    return v;
  }
  changeState = (chaneName) => {
    this.setState({
      name: chaneName
    });
  }
  printName = () => {
    const { name } = this.state;
    return name;
  }
  changeName = (event) => {
    this.setState({
      name: event.target.value
    })
  }
  // sendParam = () => {  instead we send arrow function
  //   this.handleIncreament({ id: 1 });  instead of writing this method we can write in 
  //on click event so we will do 
  // }
  render() { //() => this.handleIncreament({id:1}) this is the anonymous function return object withe id;
    return (  //onclick is event and this.handler is event method with call but here this is undefined
      <div>
        <img src={this.state.imageUrl} alt="img" />
        { this.state.isShow &&
        <button className="btn btn-success primary" onClick={() => this.handleIncreament({ id: 1 })}>hello world</button>
        }<p style={{ fontSize: 30 }} className="badge badge-primary m-2">{this.state.name}</p>
        <p style={{ fontSize: 30 }} className="badge badge-primary m-2">{this.formatCounter()}</p>
        <input type="text" onChange={this.changeName} value={this.state.name} />
        <br /> <br />
        <div style={{ fontSize: 30 }} className="badge badge-primary m-2">{this.state.name}</div>
        <div>
        <input type="radio" />
        </div>


      </div>
    );
  }
}
//<button onClick={this.changeState.bind(this, 'ceaser')} >change state of name using the bind method</button>
// {this.renderMethod()}
export default MyComponent;

//parameter are passing using 2 methods:bind method and anonymous function
//anonymous or fat arrow function: () => this.handleIncreament({id:1})
//bind : this.handler.bind(this,name) this will send the name for changing setName ;



// import React, { Component } from 'react';

// class MyComponent extends Component {
//  state = {
//    count: 0,
//    imageUrl: "https://picsum.photos/540/500"
//  };
//   render() {
//     return( <div>
//     <img src={this.state.imageUrl} />
//     <p>this is most beautiful place</p>
//     </div>
//     );
//     }

// export default MyComponent;
