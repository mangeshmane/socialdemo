import React, { Component } from 'react';//It is a Main component and others are childs component
import MyComponent from './myComponent';
import Child from '../Child/Child';

class MyComponents extends Component {
   constructor(props){
    super(props);
   
    this.state={
    name:'roman'
   }
    this.handleIncreament=this.handleIncreament.bind(this);
   }

   handleIncreament(){
       console.log("Yes,It's working", this.state.name);
   }
    render() {
        return (
            <div>
                <Child name='hank'  hello={this.state.name} greet={this.handleIncreament}/>
                <MyComponent />
            </div>
        );
    }
}

export default MyComponents;