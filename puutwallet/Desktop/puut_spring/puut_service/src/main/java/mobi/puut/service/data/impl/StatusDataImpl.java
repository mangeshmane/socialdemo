package mobi.puut.service.data.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.spongycastle.crypto.tls.SecurityParameters;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import mobi.puut.service.data.StatusData;
import mobi.puut.service.entities.users.Cryptostatus;
import mobi.puut.service.entities.users.TransactionTypes;


@EnableTransactionManagement
@Repository("statusData")
public class StatusDataImpl implements StatusData{

	 @Resource(name = "sessionFactory")
	    public SessionFactory sessionFactory;
	 
	 	@PostConstruct
		public void init() {
			SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<Cryptostatus> getByWalletId(long id) {
		// TODO Auto-generated method stub
		this.init();
		List<Cryptostatus> statuslist=sessionFactory.getCurrentSession()
				.createQuery("from Cryptostatus as cryptostatus where cryptostatus.walletid=:id")
				.setParameter("id", id).getResultList();
		
		return  statuslist;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public TransactionTypes getTransactionTypeById(long id) {
		// TODO Auto-generated method stub
		this.init();
				 TransactionTypes transactionTypes=(mobi.puut.service.entities.users.TransactionTypes) sessionFactory.getCurrentSession()
				.createQuery("from TransactionTypes as transactiontypes where transactiontypes.id=:id")
				.setParameter("id", id)
				.uniqueResult();
		
		return transactionTypes;
		
		
		
		
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Cryptostatus saveStatus(Cryptostatus status) {
		// TODO Auto-generated method stub
		this.init();
		sessionFactory.getCurrentSession().save(status);
		
		return status;				
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Cryptostatus getByTransaction(String transactionMessage, Long walletid) {
		// TODO Auto-generated method stub
		this.init();
		return sessionFactory.getCurrentSession()
				.createQuery("from Cryptostatus where cryptotransactions=:transactionMessage and walletid:walletid",Cryptostatus.class)
				.setParameter("transactionMessage", transactionMessage)
				.setParameter("walletid", walletid)
				.uniqueResult();
	}	
	
}
