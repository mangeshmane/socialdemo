package mobi.puut.service.entities.users;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import mobi.puut.service.entities.documents.Property;

@Entity
//@XmlRootElement
@Table(name="typeproperties")
public class TypeProperty implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="type")
    private mobi.puut.service.entities.documents.Type type;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="property")
    private Property property;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public mobi.puut.service.entities.documents.Type getType() {
        return type;
    }

    public void setType( mobi.puut.service.entities.documents.Type type) {
        this.type = type;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }
}