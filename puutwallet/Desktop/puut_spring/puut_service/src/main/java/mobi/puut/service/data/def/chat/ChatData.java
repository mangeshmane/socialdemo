package mobi.puut.service.data.def.chat;

import java.util.List;

import mobi.puut.service.entities.chat.Chats;

public interface ChatData {

	 public void save(Chats m);
	 public void update(Chats m);
	 public Chats find(long id);
	 public List<Chats> findAll();
	 public List<Chats> findByConversationId(long conversationId);
	 public List<Chats> findMostRecent(String currentUser);
	 public List<Chats> findUnreadMessages(long userId);
	 
}
