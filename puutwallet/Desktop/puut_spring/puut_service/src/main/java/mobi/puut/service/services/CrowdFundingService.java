package mobi.puut.service.services;

import java.util.List;

import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.crowdfunding.Area;
import mobi.puut.service.entities.crowdfunding.CrowdList;
import mobi.puut.service.entities.crowdfunding.Phase;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.Transaction;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.User;


public interface CrowdFundingService {


	public Result getSocialFinance();

	List getCurrentCampaign();

	List viewProject(Long id);

	CrowdList invest(Long id);

	Result investPost(Transaction transaction);

	Result newcampaign(CrowdList crowdList);

	List previouscampaign();

	public List<Currency> getCurrency();

	public void add(CrowdList crowdList);

	public List getCrowdFounding(User user);

	public List getPreviousCrowd(User user);

	public List getProjecttitle(Long id);

	public boolean checkTitle(String projecttitle);

	public boolean checkphone(String phone);

	public AccountSetting getAccountSettings(User user);

	public List<Phase> getPhase();

	public List<Area> getArea();

	

	public CrowdList getDifferentDate(Long id);
}
