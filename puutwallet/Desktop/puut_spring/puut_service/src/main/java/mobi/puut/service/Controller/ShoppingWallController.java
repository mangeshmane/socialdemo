
 
package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import javax.ws.rs.core.MediaType;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.ShoppingGroup;
import mobi.puut.service.entities.shoppingwall.ShoppingGroupUser;
import mobi.puut.service.entities.shoppingwall.ShoppingList;
import mobi.puut.service.services.ShoppingWallService;
import mobi.puut.service.services.crypto.impl.WalletServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Development
 */
@RestController
@RequestMapping("/shoppingwall")
public class ShoppingWallController {

    @Autowired
    private ShoppingWallService shoppingWallService;

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinggroups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<ShoppingGroup> getShoppingGroups() {
        try {
            return shoppingWallService.getShoppingGroups();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ShoppingWallController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shopping/users/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<ShoppingGroupUser> getShoppingGroupUser(@PathVariable("id") Long id) {
        try {
            return shoppingWallService.getShoppingGroupUser(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ShoppingWallController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shopping/groups/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<ShoppingGroup> getShoppingGroupbyName(@PathVariable("name") String name) {
        try {
            return shoppingWallService.getShoppingGroupbyName(name);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ShoppingWallController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinggroups/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result createShoppingGroup(@RequestBody ShoppingGroup shoppinggroup) {
        try {
            return shoppingWallService.createShoppingGroup(shoppinggroup);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ShoppingWallController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinggroups/edit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result editShoppingGroup(@RequestBody ShoppingGroup shoppinggroup) {
        try {
            return shoppingWallService.editShoppingGroup(shoppinggroup);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ShoppingWallController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinggroups/join/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
    public Result joinShoppingGroup(@PathVariable("id") Long id) {
        try {
            return shoppingWallService.joinShoppingGroup(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ShoppingWallController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinggroups/approve/{userid}/{groupid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public ShoppingGroupUser approveUserToGroup(@PathVariable("userid") Long userid, @PathVariable("groupid") Long groupid) {
        try {
            return shoppingWallService.approveUserToGroup(userid, groupid);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ShoppingWallController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinggroups/setmoderator/{userid}/{groupid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result assignModerator(@PathVariable("userid") Long userid, @PathVariable("groupid") Long groupid) {
        try {
            return shoppingWallService.assignModerator(userid, groupid);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ShoppingWallController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinggroups/leave/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
    public Result leaveShoppingGroup(@PathVariable("id") Long id) {
        try {
            return shoppingWallService.leaveShoppingGroup(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ShoppingWallController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinglists", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<ShoppingList> getShoppingLists() {
        try {
            return shoppingWallService.getShoppingLists();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ShoppingWallController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinglists/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result createShoppingList(@RequestBody ShoppingList shoppinglist) {
        try {
            return shoppingWallService.createShoppingList(shoppinglist);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletServiceImpl.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinglists/edit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result editShoppingList(@RequestBody ShoppingList shoppinglist) {
        try {
            return shoppingWallService.editShoppingList(shoppinglist);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletServiceImpl.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinglists/join/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
    public Result joinShoppingList(@PathVariable("id") Long id) {
        try {
            return shoppingWallService.joinShoppingList(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletServiceImpl.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/shoppinglists/leave/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
    public Result leaveShoppingList(@PathVariable("id") Long id) {
        try {
            return shoppingWallService.leaveShoppingList(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletServiceImpl.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/offerlists", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<ShoppingList> getOfferLists() {
        try {
            return shoppingWallService.getOfferLists();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletServiceImpl.class).error(exception);
            throw new Error(exception);
        }
    }

}
