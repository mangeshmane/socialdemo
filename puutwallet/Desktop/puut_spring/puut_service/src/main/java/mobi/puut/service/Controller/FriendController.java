package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.documents.Type;
import mobi.puut.service.entities.users.Friend;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.FriendService;

@RestController
@RequestMapping("/friends")
public class FriendController {
	
	@Autowired
	private FriendService friendService;

	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
	public List<Friend>  getFriends(){
		try {
			return friendService.getFriends();
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}

	
	
	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/invitations",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
	public List<Friend>  getInvitations(){
		try {
			return friendService.getInvitations();
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	

	
	

	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/accept/{id}",method=RequestMethod.PUT,produces=MediaType.APPLICATION_JSON)
	public Result  acceptInvitations(@PathVariable("id") Long id){
		try {
			return friendService.acceptInvitations(id);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}	
	
	
	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
	public User  getFriendProfile(@PathVariable("id") Long id){
		try {
			return friendService.getFriendProfile(id);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	

	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/search",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON,consumes=MediaType.APPLICATION_JSON)
	public List<User> searchFriend(@RequestBody User user){
		try {
			return friendService.searchFriend(user);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	
	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/delete",method=RequestMethod.DELETE,produces=MediaType.APPLICATION_JSON,consumes=MediaType.APPLICATION_JSON)
	public Result deleteFriend(@RequestBody List<User> users){
		try {
			return friendService.deleteFriend(users);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}	
	
}

