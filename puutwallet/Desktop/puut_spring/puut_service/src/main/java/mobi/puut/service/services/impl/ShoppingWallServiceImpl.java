package mobi.puut.service.services.impl;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import javax.annotation.PostConstruct;

import mobi.puut.service.ServicesUtil;
import mobi.puut.service.data.ShoppingWallData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.ShoppingGroup;
import mobi.puut.service.entities.shoppingwall.ShoppingGroupUser;
import mobi.puut.service.entities.shoppingwall.ShoppingList;
import mobi.puut.service.entities.shoppingwall.ShoppingListUser;
import mobi.puut.service.entities.users.Personal;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.ShoppingWallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;


@Service("shoppingWallService")
public class ShoppingWallServiceImpl implements ShoppingWallService {

    @Autowired
    ShoppingWallData shoppingWallData;

    @Autowired
    UserData usersData;

    @Autowired
    ServicesUtil servicesUtil;

    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    public List<ShoppingGroup> getShoppingGroups() {
        this.init();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = usersData.getByUsername(login);
        List<ShoppingGroup> groups = shoppingWallData.getGroups();
        List<ShoppingGroupUser> users = shoppingWallData.getUsers(user);

        for (ShoppingGroup group : groups) {
            group.setHasuser(false);
            for (ShoppingGroupUser u : users) {
                if (group.getId() == u.getShoppinggroup().getId()) {
                    group.setHasuser(true);
                    break;
                }
            }
        }

        return groups;
    }

    @Override
    public Result createShoppingGroup(ShoppingGroup shoppinggroup) {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        int randomNumber = ThreadLocalRandom.current().nextInt();
        String fileName = ShoppingGroup.class.getSimpleName() + randomNumber;
        shoppinggroup.setImgname(fileName);
        return shoppingWallData.createGroup(shoppinggroup,login);
    }

    @Override
    public Result joinShoppingGroup(Long id) {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        return shoppingWallData.joinGroup(id, login);
    }

    @Override
    public Result leaveShoppingGroup(Long id) {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        return shoppingWallData.leaveGroup(id, login);
    }

    
    @Override
    public List<ShoppingList> getShoppingLists() {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = usersData.getByUsername(login);
        List<ShoppingList> lists = shoppingWallData.getLists(user, 1L);
        List<ShoppingListUser> users = shoppingWallData.getListUsers(user);

        for (ShoppingList list : lists) {
            list.setHasuser(false);
            for (ShoppingListUser u : users) {
                if (list.getId() == u.getShoppinglist().getId()) {
                    list.setHasuser(true);
                    break;
                }
            }
        }

        return lists;
    }

    
    @Override
    public Result createShoppingList(ShoppingList shoppinglist) {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        int randomNumber = ThreadLocalRandom.current().nextInt();
        String fileName = ShoppingList.class.getSimpleName() + randomNumber;
        shoppinglist.setImgname(fileName);
        return shoppingWallData.createList(shoppinglist, login);
    }

    
    @Override
    public Result joinShoppingList(Long id) {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        return shoppingWallData.joinList(id, login);
    }

    @Override
    public Result leaveShoppingList(Long id) {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        return shoppingWallData.leaveList(id, login);
    }

    @Override
    public List<ShoppingList> getOfferLists() {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = usersData.getByUsername(login);
        return shoppingWallData.getLists(user, 2L);
    }

    @Override
    public List<ShoppingGroupUser> getShoppingGroupUser(Long id) {
        this.init();
        ShoppingGroup group = shoppingWallData.getShoppingGroupById(id);

        List<ShoppingGroupUser> groupUser = shoppingWallData.getShoppingGroupUsers(group);

        for (int i = 0; i < groupUser.size(); i++) {
            Personal personal = usersData.getPersonal(groupUser.get(i).getUser());
            if (personal != null) {
                groupUser.get(i).getUser().setPimage(personal.getImage());
            }
        }
        return groupUser;

    }

    @Override
    public List<ShoppingGroup> getShoppingGroupbyName(String name) {
        this.init();
        return shoppingWallData.getShoppingGroupByName(name);
    }

    @Override
    public ShoppingGroupUser approveUserToGroup(Long userid, Long groupid) {
        this.init();
        ShoppingGroup groupUser = shoppingWallData.getShoppingGroupById(groupid);
        User user = usersData.get(userid);
        ShoppingGroupUser shoppingGroupUser = shoppingWallData.getGroupUserByGroupAndUser(user, groupUser, false);
        shoppingGroupUser.setApproved(Boolean.TRUE);
        ShoppingGroupUser newShoppingGroupUser = shoppingWallData.createShoppingGroupUser(shoppingGroupUser);
        return newShoppingGroupUser;
    }

    @Override
    public Result assignModerator(Long userid, Long groupid) {
        this.init();
        Result result = new Result();
        ShoppingGroup shoppinggroup = shoppingWallData.getShoppingGroupById(groupid);
        User user = usersData.get(userid);
        ShoppingGroupUser groupUser = shoppingWallData.getNonModeratorGroupUser(user, shoppinggroup);

        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User loginUser = usersData.getByUsername(login);
        ShoppingGroupUser loginGroupUser = shoppingWallData.getGroupUserByGroupAndUser(loginUser, shoppinggroup, true);

        if (loginGroupUser.isModerator()) {
            groupUser.setModerator(true);
            shoppingWallData.createShoppingGroupUser(groupUser);
            result.setSuccess(true);
        } else {
            result.setSuccess(false);
        }
        return result;
    }

    @Override
    public Result editShoppingGroup(ShoppingGroup shoppinggroup) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        Result result = new Result();

        ShoppingGroupUser shoppingGroupUser = shoppingWallData.getGroupUserByGroupAndUser(user, shoppinggroup, Boolean.TRUE);
        if (shoppingGroupUser != null) {
            if (shoppingWallData.checkModerator(user, shoppinggroup)) {
                int randomNumber = ThreadLocalRandom.current().nextInt();
                String fileName = ShoppingGroup.class.getSimpleName() + randomNumber;
                shoppinggroup.setImgname(fileName);
                return shoppingWallData.editGroup(shoppinggroup);
            } else {
                result.setSuccess(false);
                result.setError("CURRENT USER IS NOT MODERATOR OF GROUP");
                return result;
            }
        } else {
            result.setSuccess(false);
            result.setError("USER IS NOT MEMBER OF GROUP");
            return result;

        }

    }

    @Override
    public Result editShoppingList(ShoppingList shoppinglist) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        Result result = new Result();
        if (shoppingWallData.isShoppingListUser(user, shoppinglist)) {
            int randomNumber = ThreadLocalRandom.current().nextInt();
            String fileName = ShoppingList.class.getSimpleName() + randomNumber;
            shoppinglist.setImgname(fileName);
            return shoppingWallData.editShoppingList(shoppinglist);
        } else {
            result.setSuccess(false);
            result.setError("USER IS NOT MEMBER OF SHOPPINGLIST");
            return result;
        }
    }

}
