package mobi.puut.service.entities.shoppingwall;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import mobi.puut.service.entities.users.IPlocations;


@Entity
//@XmlRootElement
@Table(name="shoppinggroups")
public class ShoppingGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
    @Column(name="name")
    private String name;
    
    @Column(name="description")
    private String description;
    
    @Column(name="preferences")
    private String preferences;
    
    @Column(name="image")
    private byte[] image;
    @ManyToOne(cascade = {CascadeType.REFRESH})
    
    @JoinColumn(name="city")
    private IPlocations city;
    
    @Column(name="imgname")
    private String imgname;

   
    
    @Transient
    private Boolean hasuser;

    public IPlocations getCity() {
        return city;
    }

    public void setCity(IPlocations city) {
        this.city = city;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreferences() {
        return preferences;
    }

    public void setPreferences(String preferences) {
        this.preferences = preferences;
    }

    @XmlElement(name="hasuser")
    public Boolean getHasuser() {
        return hasuser;
    }

    public void setHasuser(Boolean hasuser) {
        this.hasuser = hasuser;
    }
    
    
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    
     public String getImgname() {
        return imgname;
    }

    public void setImgname(String imgname) {
        this.imgname = imgname;
    }
}