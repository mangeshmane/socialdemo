/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.entities.users;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author admin1
 */

@Entity
@Table(name="user_role")
public class UserRole {
    
    @Id
    @Column(name="id")
    @GeneratedValue
    private int id;
    
    
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name="user")
    private User user;
    
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name="role")
    private Role role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
    
 
}
