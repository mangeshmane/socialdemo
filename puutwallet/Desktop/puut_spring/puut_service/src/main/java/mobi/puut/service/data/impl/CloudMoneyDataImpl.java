package mobi.puut.service.data.impl;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import mobi.puut.service.data.CloudMoneyData;
import mobi.puut.service.data.DocumentData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.cloudmoney.CloudMoney;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.documents.Status;
import mobi.puut.service.entities.shoppingwall.Transaction;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.Document;
import mobi.puut.service.entities.users.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Repository("cloudMoneyData")
public class CloudMoneyDataImpl implements CloudMoneyData {
    
    @Autowired
    SessionFactory sessionFactory;
    
    @Autowired
    DocumentData documentsData;
    
    @Autowired
    UserData usersData;
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }


    @Transactional(rollbackFor=Exception.class)
    public List<Currency> getCurrencies() {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<Currency> currencies = session.createQuery("from Currency",Currency.class).getResultList();
        
        return currencies;
    }

    @Transactional(rollbackFor=Exception.class)
    public List<CloudMoney> getBalance(String login) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<CloudMoney> list = session.createQuery("from CloudMoney",CloudMoney.class).getResultList();
        
        return list.stream().filter(c->c.getUser().getUsername().equals(login)).collect(Collectors.toList());
    }

    @Transactional(rollbackFor=Exception.class)
    public List<Transaction> getTransactions(String login) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();

        List<Transaction> transactions = (List<Transaction>) session.createQuery("from Transaction",Transaction.class).getResultList();

        return transactions.stream().filter(t->t.getUserfrom().getUsername().equals(login) && t.getUserto().getUsername().equals(login))
                .collect(Collectors.toList());
    }

    private CloudMoney getCloudMoneyByLogin(String login, Currency currency) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();        
        List<CloudMoney> list = (List<CloudMoney>) session.createQuery("from CloudMoney",CloudMoney.class).getResultList();
        CloudMoney cm = null;
        for (CloudMoney cloudMoney : list) {
            if(cloudMoney.getUser() != null && cloudMoney.getCurrency() != null){
                if(cloudMoney.getUser().getUsername().equals(login) && cloudMoney.getCurrency().getId().equals(currency.getId())){
                    cm = cloudMoney;
                }
            }
        }
        
//        CloudMoney cloudMoney = list.stream()
//                .filter(c->c.getUser().getLogin().equals(login) && c.getCurrency().getId()==currency.getId())
//                .findFirst()
//                .orElse(null);
        
        return cm;
    }

    private CloudMoney getCloudMoneyByPuutNumber(String puut_number, Currency currency) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        CloudMoney cm = null;
        List<CloudMoney> list = (List<CloudMoney>) session.createQuery("from CloudMoney",CloudMoney.class).getResultList();
        for (CloudMoney cloudMoney : list) {
            if(cloudMoney.getUser() != null && cloudMoney.getCurrency() != null){
                if(cloudMoney.getUser().getPuutnumber().equals(puut_number) && cloudMoney.getCurrency().getId().equals(currency.getId())){
                    cm = cloudMoney;
                }
            }
        }
//        CloudMoney cloudMoney = list.stream()
//                .filter(c->c.getUser().getPuutnumber().equals(puut_number) && c.getCurrency().getId()==currency.getId())
//                .findFirst()
//                .orElse(null);
        
        return cm;
    }

    private void add(CloudMoney cloudMoney) {
        this.init();
        
        sessionFactory.getCurrentSession()
                .save(cloudMoney);
    }
    
    private void edit(CloudMoney cloudMoney) {
        this.init();
        
        sessionFactory.getCurrentSession()
                .saveOrUpdate(cloudMoney);
    }
    
    private void add(Transaction transaction) {
        this.init();
        
        sessionFactory.getCurrentSession()
                .save(transaction);
    }

    @Transactional(rollbackFor=Exception.class)
    public Result sendMoney(Transaction transaction, Result result, String login) {
        this.init();
        
        User userto;

        if (transaction.getBillid() != null) {
            Document invoice = documentsData.get(Long.parseLong(transaction.getBillid()),
                    login, 10L, 2L);

            if (invoice == null) {
                result.setSuccess(false);
                result.setError("NOSUCHUNPAIDBILL");
                return result;
            }

            if (invoice.getCurrency() == null || invoice.getMerchant() == null || invoice.getAmount() == null) {
                result.setSuccess(false);
                result.setError("BILLDATAMISSING");
                return result;
            }

            transaction.setCurrency(invoice.getCurrency());
            transaction.setPuutnumber(invoice.getMerchant().getPuutnumber());
            transaction.setAmount(invoice.getAmount());

            userto = invoice.getMerchant();
            Status status = new Status();
            status.setId(1L);
            invoice.setStatus(status);
            documentsData.edit(invoice);
        } else {
            Session session = sessionFactory.getCurrentSession();

            List<User> list = (List<User>) session.createQuery("from User",User.class).getResultList();

            userto = list.stream()
                    .filter(u->u.getPuutnumber().equals(transaction.getPuutnumber()))
                    .findFirst()
                    .orElse(null);
        }

        if (userto != null) {
            Object moneyfrom;
            boolean credit_card = false;
            if (transaction.getCreditcardid() == null) {
                moneyfrom = getCloudMoneyByLogin(login, transaction.getCurrency());
            } else {
                moneyfrom = documentsData.get(Long.parseLong(transaction.getCreditcardid()),
                        login, 1L, new Date());
                credit_card = true;
            }

            CloudMoney moneyto = getCloudMoneyByPuutNumber(transaction.getPuutnumber(),
                    transaction.getCurrency());

            if (moneyfrom != null) {
                String camount;
                if (credit_card) {
                    camount = ((Document)moneyfrom).getAmount();
                }
                else {
                    camount = ((CloudMoney)moneyfrom).getAmount();
                }
                
                if(camount.isEmpty() || camount == null){
                    result.setSuccess(Boolean.FALSE);
                    result.setError("No Amount is available");
                }
                
                Double moneyfrom_amount = Double.parseDouble(camount.replace(',', '.'));
                Double trans_amount = Double.parseDouble(transaction.getAmount().replace(',', '.'));
                Double moneyfrom_new_amount = moneyfrom_amount - trans_amount;

                if (moneyfrom_new_amount >= 0) {
                    DecimalFormat df = new DecimalFormat("###.##");
                    if (credit_card) {
                        ((Document)moneyfrom).setAmount(df.format(moneyfrom_new_amount).replace(',', '.'));
                        documentsData.edit((Document)moneyfrom);
                    } else {
                        ((CloudMoney)moneyfrom).setAmount(df.format(moneyfrom_new_amount).replace(',', '.'));
                        edit((CloudMoney)moneyfrom);
                    }

                    if (moneyto == null) {
                        moneyto = new CloudMoney();
                        moneyto.setUser(userto);
                        moneyto.setCurrency(transaction.getCurrency());
                        moneyto.setAmount(df.format(trans_amount).replace(',', '.'));
                        add(moneyto);
                    } else {
                        Double moneyto_amount = Double.parseDouble(moneyto.getAmount().replace(',', '.'));
                        Double moneyto_new_amount = moneyto_amount + trans_amount;
                        moneyto.setAmount(df.format(moneyto_new_amount).replace(',', '.'));
                        edit(moneyto);
                    }

                    if (credit_card) {
                        transaction.setUserfrom(((Document)moneyfrom).getUser());
                    }
                    else {
                        transaction.setUserfrom(((CloudMoney)moneyfrom).getUser());
                    }
                    transaction.setUserto(userto);
                    transaction.setTransdate(new Date());
                    add(transaction);
                    result.setSuccess(true);
                } else {
                    result.setSuccess(false);
                    result.setError("NOTENOUGHMONEY");
                }
            } else {
                result.setSuccess(false);
                result.setError("WRONGACCOUNT");
            }
        } else {
            result.setSuccess(false);
            result.setError("WRONGRECIPIENT");
        }
        
        return result;
    }

    @Transactional
    public Currency getCurrencyByCode(String symbol) {
        return sessionFactory.getCurrentSession().createQuery("from Currency where symbol=:symbol",Currency.class).setParameter("symbol", symbol)
                .uniqueResult();
    }
}