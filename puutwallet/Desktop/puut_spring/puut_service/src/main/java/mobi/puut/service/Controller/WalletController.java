/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.List;
import javax.ws.rs.core.MediaType;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.crypto.btc.SendMoney;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.Cryptoinfo;
import mobi.puut.service.entities.users.Cryptostatus;
import mobi.puut.service.entities.users.GenerateWallet;
import mobi.puut.service.services.crypto.def.WalletService;
import mobi.puut.service.services.crypto.impl.WalletServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Development
 */
@RestController
@RequestMapping("/crypto")
public class WalletController {

    @Autowired
    WalletService walletService;

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/wallets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Cryptoinfo> getAllWallets() {
        try {
            return walletService.getAllWallets();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "getWalletInfo/{walletId:[\\d]+}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public Cryptoinfo getWalletInfo(@PathVariable("walletId") Long walletId) {
        try {
            return walletService.getWalletInfo(walletId);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/generateAddress", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    public Result generateAddress(@RequestBody GenerateWallet generateWallet) {
        try {
            return walletService.generateAddress(generateWallet);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "delete/{walletId:[\\d]+}", method = RequestMethod.DELETE)
    public Result deleteWalletInfoById(@PathVariable("walletId") Long id) {
        try {
            return walletService.deleteWalletInfoById(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "sendMoney/{walletId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result sendMoney(@PathVariable("walletId") final Long walletId, @RequestBody final SendMoney sendMoney) {
        try {
            return walletService.sendMoney(walletId, sendMoney);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "balance/{walletId:[\\d]+}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public String getWalletBalanceById(@PathVariable("walletId") final long id) {
        try {
            return walletService.getWalletBalanceById(id);
        } catch (Exception e) {
//            StringWriter sw = new StringWriter();
//            e.printStackTrace(new PrintWriter(sw));
//            String exception = sw.toString();
//            Logger.getLogger(WalletController.class).error(exception);
//            throw new Error(exception);
              Logger.getLogger(WalletController.class).error("Got Exception: "+e);
        }
        return "";
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public String getWalletsCount() {
        try {
            return walletService.getWalletsCount();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "addressAndCurrency/{currency}/{address}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public Cryptoinfo getWalletInfoByCurrencyAndAddress(@PathVariable("currency") String currencyName, @PathVariable("address") String address) {
        try {
            return walletService.getWalletInfoByCurrencyAndAddress(currencyName, address);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "defaultAddress/{walletId:[\\d]+}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public Cryptoinfo updateDefaultWallet(@PathVariable("walletId") final Long id) {
        try {
            return walletService.updateDefaultWallet(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "transaction/{walletId:[\\d]+}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List getWalleTransactionsById(@PathVariable("walletId") final long id) {
        try {
            return walletService.getWalleTransactionsById(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "currencies", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Currency> currencys() {
        try {
            return walletService.currencys();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "transactions/date/{walletId}/{date}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Cryptostatus> getTransactionsByMonth(@PathVariable("walletId") long id, @PathVariable("date") String date) throws ParseException {
        try {
            return walletService.getTransactionsByMonth(id, date);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "transactions/date/{walletId}/{startDate}/{endDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Cryptostatus> getTransactionsBetweenDates(@PathVariable("walletId") long id, @PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate) throws ParseException {
        try {
            return walletService.getTransactionsBetweenDates(id, startDate, endDate);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

   /* @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "requestbtc", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result requestbtc(@RequestBody final RequestMoney requestMoney) {
        try {
            return walletService.requestbtc(requestMoney);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "recivedrequestbtc", method = RequestMethod.GET)
    List<Cryptorequest> getRecievedBtcRequest() {
        try {
            return walletService.getRecievedBtcRequest();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }
    
     @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "sentrequestbtc", method = RequestMethod.GET)
    List<Cryptorequest> getSentBtcRequest() {
        try {
            return walletService.getSentBtcRequest();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }


    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "getrequestbtc/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public Cryptorequest getBTCRequestByid(@PathVariable("id") long id) {
        try {
            return walletService.getBTCRequestByid(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "updaterequestbtc/{id}/{amount}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    Result updateRequestBTC(@PathVariable("id") long id, @PathVariable("amount") String amount) {
        try {
            return walletService.updateRequestBTC(id, amount);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletController.class).error(exception);
            throw new Error(exception);
        }
    }
*/
}
