package mobi.puut.service.services.impl;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mobi.puut.service.crypto.util.RestService;
import mobi.puut.service.entities.users.User;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import mobi.puut.service.entities.users.Preference;


import mobi.puut.service.data.def.chat.BlockIntervalData;
import mobi.puut.service.data.def.chat.ChatData;
import mobi.puut.service.data.def.chat.ConversationData;
import mobi.puut.service.data.def.chat.ParticipantData;
import mobi.puut.service.entities.chat.BlockInterval;
import mobi.puut.service.entities.chat.Chats;
import mobi.puut.service.entities.chat.Conversation;
import mobi.puut.service.entities.chat.Participant;
import mobi.puut.service.data.FriendData;
import mobi.puut.service.data.UserData;

import mobi.puut.service.data.def.chat.UserToUserData;
import mobi.puut.service.entities.chat.Autocompletion;
import org.apache.commons.codec.binary.Base64;
import mobi.puut.service.utils.chat.DateScanner;
import mobi.puut.service.entities.chat.UserToUser;

import mobi.puut.service.entities.users.Personal;
import org.json.JSONObject;

import mobi.puut.service.services.chat.chatService;

/**
 *
 * @author Java Consultant Jay
 */
@Service("chatService")
@RestService
public class ChatServiceImpl implements chatService {

    @Autowired
    ConversationData conversationData;

    @Autowired
    ParticipantData participantData;

    @Autowired
    ChatData messageData;

    @Autowired
    UserData userData;

    @Autowired
    FriendData friendData;

    @Autowired
    BlockIntervalData blockIntervalData;

    @Autowired
    UserToUserData userToUserData;

    @Autowired
    DateScanner dateScanner;

    @Override
    public Boolean delete(String convId) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userData.getByUsername(login);
        try {
            Participant p = participantData.findWithConvAndUser(Long.valueOf(convId), user.getId()).get(0);
            if (p != null) {
                p.setVisible(Boolean.FALSE);
                participantData.update(p);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to disable chat");
        }

    }

    /*public Boolean delete(String convId) {
       String login = SecurityContextHolder.getContext().getAuthentication().getName();       
       return disableConversation(convId, login);
    }*/
    
    @Override
    public List<User> chatCreation() {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userData.getByUsername(login);
        Preference preference = friendData.getPreference(user);
        List<Preference> friends = friendData.getAccept(preference);
        List<Preference> friends2 = friendData.getFind(preference);
        friends.addAll(friends2);

        List<User> friendsList = new ArrayList<>();

        for (int i = 0; i < friends.size(); i++) {
            Personal personal = userData.getPersonal(friends.get(i).getUser());
            friends.get(i).getUser().setPimage(personal.getImage());
            friendsList.add(friends.get(i).getUser());
        }
        friendsList.sort(Comparator.comparing(User::getId));
        return friendsList;
    }

    
    @Override
    public Map submitUsers(String name, String[] friendIds) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userData.getByUsername(login);
        //String friendname = "";   
        String fullname = "";
        List<User> users = new ArrayList<User>();
        Map<String, Object> result = new HashMap<String, Object>();

        if (name.equals("6r4s5G\\@pXF9NsYZ")) {
            throw new IllegalArgumentException("You cannot use this conversation name. Try another one.");
        } else if (!verifyNameAndOwner(name, login)) {
            throw new IllegalArgumentException("You have already used this conversation name. Try another one");
        } else {

            List<String> listUsernames = new ArrayList<>();
            for (int i = 0; i < friendIds.length; i++) {
                //listUsernames.add(friendData.getUser(Long.valueOf(friendIds[i])).getUsername());
                if (friendData.isFriend(user, userData.getUser(Long.valueOf(friendIds[i])))) {
                    listUsernames.add(userData.getUser(Long.valueOf(friendIds[i])).getUsername());
                    
                 
                }
            }
            listUsernames.add(login);
            listUsernames.sort(String::compareToIgnoreCase);

            for (int i = 0; i < listUsernames.size(); i++) {
                Long friendId;
                User tempUser = new User();
                if (i < listUsernames.size() - 1) {
                    friendId = userData.getByUsername(listUsernames.get(i)).getId();
                    
                    
                } else {
                    friendId = userData.getByUsername(listUsernames.get(i)).getId();
                }
                tempUser.setId(friendId);
                tempUser.setUsername(listUsernames.get(i));
                users.add(tempUser);
            }

            String owner;
            if (listUsernames.size() == 2) {
                name = "6r4s5G\\@pXF9NsYZ";
                owner = "nobody";
                for (int i = 0; i < listUsernames.size(); i++) {
                    if (!listUsernames.get(i).equals(login)) {
                        fullname = userData.getByUsername(listUsernames.get(i)).getFirstname() + " " + userData.getByUsername(listUsernames.get(i)).getLastname();
                      
                        
                        users.get(i).setFirstname(userData.getByUsername(listUsernames.get(i)).getFirstname());
                        users.get(i).setLastname(userData.getByUsername(listUsernames.get(i)).getLastname());
                        users.get(i).setFullname(fullname);
                    } else {
                        users.get(i).setFirstname(userData.getByUsername(listUsernames.get(i)).getFirstname());
                        users.get(i).setLastname(userData.getByUsername(listUsernames.get(i)).getLastname());
                        users.get(i).setFullname(user.getFirstname() + " " + user.getLastname());
                    }
                }
                result.put("owner", owner);
            } else {
                //owner = ""+userData.get(login).getId();
            
                owner = "" + userData.getByUsername(login).getId();
                result.put("owner", owner);
            }

            //String fullnamesList = "";
            for (int i = 0; i < listUsernames.size(); i++) {
                String fullnamesList = "";
                if (!listUsernames.get(i).equals(login)) {

                    if (i < listUsernames.size() - 1) {
                        if (i == listUsernames.size() - 2 && login.equals(listUsernames.get(listUsernames.size() - 1))) {
                            users.get(i).setFirstname(userData.getByUsername(listUsernames.get(i)).getFirstname());
                            users.get(i).setLastname(userData.getByUsername(listUsernames.get(i)).getLastname());
                            fullnamesList = userData.getByUsername(listUsernames.get(i)).getFirstname() + "0" + userData.getByUsername(listUsernames.get(i)).getLastname();
                        } else {
                            users.get(i).setFirstname(userData.getByUsername(listUsernames.get(i)).getFirstname());
                            users.get(i).setLastname(userData.getByUsername(listUsernames.get(i)).getLastname());
                            fullnamesList = userData.getByUsername(listUsernames.get(i)).getFirstname() + "_" + userData.getByUsername(listUsernames.get(i)).getLastname();
                        }
                    } else {
                        users.get(i).setFirstname(userData.getByUsername(listUsernames.get(i)).getFirstname());
                        users.get(i).setLastname(userData.getByUsername(listUsernames.get(i)).getLastname());
                        fullnamesList += userData.getByUsername(listUsernames.get(i)).getFirstname() + "0" + userData.getByUsername(listUsernames.get(i)).getLastname();
                    }

                } else {
                    users.get(i).setFirstname(user.getFirstname());
                    users.get(i).setLastname(user.getLastname());
                    fullnamesList += userData.getByUsername(user.getFirstname() + " " + user.getLastname());
                }
                users.get(i).setFullname(fullnamesList);
            }

        }
        result.put("name", name);
        result.put("Users", users);
        return result;
    }

    @Override
    public List<Conversation> display() {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userData.getByUsername(login);
        List<Participant> paricipants = participantData.findByUser(user);
        List<Conversation> conversations = new ArrayList<Conversation>();

        for (Participant paricipant : paricipants) {
            conversations.add(conversationData.find(paricipant.getConversation().getId()));
        }

        // List<Conversation> conversations = conversationData.findConversationByUser(user);
        for (Conversation conversation : conversations) {
            List<Participant> participants = participantData.findByConv(conversation.getId());
            List<Chats> messages = messageData.findByConversationId(conversation.getId());
            if (!participants.isEmpty()) {
                conversation.setParticipants(participants);
            }
            if (!messages.isEmpty()) {
                conversation.setMessages(messages);
            }

        }
        return conversations;
    }

    @Override
    public List<Conversation> getConvList(String query) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userData.getByUsername(login);

        List<Conversation> conversations = conversationData.findConvByTerm(query);
        if (conversations.isEmpty()) {
            return conversations;
        }
        for (Conversation conversation : conversations) {
            List<Participant> participants = participantData.findByConv(conversation.getId());
            List<Chats> messages = messageData.findByConversationId(conversation.getId());
            if (!participants.isEmpty()) {
                conversation.setParticipants(participants);
            }
            if (!messages.isEmpty()) {
                conversation.setMessages(messages);
            }

        }

        return conversations;
    }

    @Override
    public Conversation updateConverSation(Conversation conv) {
        Conversation conversation = conversationData.find(conv.getId());
        if (conversation == null) {
            throw new IllegalArgumentException("Conversation not found with gievn id");
        }

        if (conv.getName() != null || !conv.getName().isEmpty()) {
            conversation.setName(conv.getName());
        }

        if (conv.getConvPicture() != null) {
            conversation.setConvPicture(conv.getConvPicture());
        }

        try {
            conversationData.update(conv);
            return conversation;
        } catch (Exception e) {
            throw new Error("Failed to update conversation");
        }
    }

    /*@Override
    public List getUserQuery(String query, String conv) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        List<List> tagList = getUserList(query, login); 
   
         List<User> results = new ArrayList<User>();
        
      
        Conversation conversation = conversationData.find(Long.valueOf(conv));
        List<Participant> participants = participantData.getParticipantByConversation(conversation);
        
        int participantsFound = 0;
        int i = 0;
        while (i < tagList.size()){
            boolean participantFound = false;
            for (int j = 0; j < participants.size(); j++){
                if (tagList.get(i).get(1).equals(userData.get(participants.get(j).getUser().getId()).getLogin())){
                    participantFound = true;
                }
            }
            if (!participantFound){  
                User user = new User();
                user.setId(Long.valueOf(tagList.get(i).get(2).toString()));
                user.setFirstname(tagList.get(i).get(3).toString());
                user.setLastname(tagList.get(i).get(4).toString());
                user.setFullName((String)tagList.get(i).get(0));
                results.add(user);
//                results.add((String)tagList.get(i).get(0));
//                results.add((String)tagList.get(i).get(1));
                participantsFound++;
            }
            i++;
        }

        return results;
    }*/

    @Override
    public List getUserQuery(String query, String conv) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        List<List> tagList = getUserList(query, login);

        List<Autocompletion> results = new ArrayList<>();

        String[] convParams = conv.split("-");
        String[] participants = convParams[1].split("_");

        int participantsFound = 0;
        int i = 0;
        while (i < tagList.size() && participantsFound < 5) {
            boolean participantFound = false;
            for (int j = 0; j < participants.length; j++) {
                if (tagList.get(i).get(1).equals(userData.get(Long.valueOf(participants[j])).getUsername())) {
                    participantFound = true;
                }
            }
            if (!participantFound) {
                Autocompletion a = new Autocompletion();
                a.setFullnames((String) tagList.get(i).get(0));
                a.setIdentifier((String) tagList.get(i).get(1));
                results.add(a);
                participantsFound++;
            }
            i++;
        }

        return results;
    }

    /**
     * To save a new conversation
     *
     * @param c The specific conversation that we want to store
     * @return A boolean which says if the save is OK
     */
    public Boolean addConversation(Conversation c) {
        conversationData.save(c);
        return true;
    }

    /**
     * To update a conversation
     *
     * @param c The specific conversation that we want to update
     * @return A boolean which says if the update is OK
     */
    public Boolean updateConversation(Conversation c) {
        if (conversationData.find(c.getId()) != null) {
            conversationData.update(c);
            return true;
        }
        return false;
    }

    /**
     * To save a new participant
     *
     * @param p The specific participant that we want to store
     * @return A boolean which says if the save is OK
     */
    public Boolean addParticipant(Participant p) {
        participantData.save(p);
        return true;
    }

    /**
     * To update a participant
     *
     * @param p The specific participant that we want to update
     * @return A boolean which says if the update is OK
     */
    public Boolean updateParticipant(Participant p) {
        if (participantData.find(p.getId()) != null) {
            participantData.update(p);
            return true;
        }
        return false;
    }

    /**
     * To save a new message
     *
     * @param m The specific message that we want to store
     * @return A boolean which says if the save is OK
     */
    public Boolean addMessage(Chats m) {
        messageData.save(m);
        return true;
    }

    /**
     * To update a message
     *
     * @param m The specific message that we want to update
     * @return A boolean which says if the update is OK
     */
    public Boolean updateMessage(Chats m) {
        if (messageData.find(m.getId()) != null) {
            messageData.update(m);
            return true;
        }
        return false;
    }

    /**
     * Helps to load the messages for a specific conversation
     *
     * @param conv The name of the conversation
     * @param userId The current user ID
     * @return A list of lists. In those list, we have the messages contents,
     * but also message types and messages senders
     */
    public List<List> loadMessages(String conv, Long userId) {

        Long convId = null;
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        if (nameAndOwner[1].equals("nobody")) {
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                convId = conversationData.findByName(convParams[1]).get(0).getId();
            }
        } else {
            if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
                convId = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0).getId();
            }
        }

        if (convId != null) {
            List<Chats> messages = new ArrayList<Chats>();
            messages = messageData.findByConversationId(convId);
            List<String> messagesString = new ArrayList<>();
            List<Integer> messagesTypes = new ArrayList<>();
            List<String> messagesSenders = new ArrayList<>();
            List<List> result = new ArrayList<>();
            Chats previousMessage = null;
            Calendar previousCalendar = null;

            Participant participant = participantData.findWithConvAndUser(convId, userId).get(0);
            Date timeJoined = participant.getTimeJoined();

            if (participantData.countParticipants(convId) == 2) {

                String[] participants = convParams[1].split("_");
                String friend = "";
                for (String p : participants) {
                    if (!userId.equals(Long.valueOf(p))) {
                        friend = p;
                    }
                }

                List<BlockInterval> blockIntervalList = blockIntervalData.findAllByUsersIds(userId, Long.valueOf(friend));

                for (Chats m : messages) {

                    boolean blocked = false;
                    for (BlockInterval b : blockIntervalList) {
                        if (b.getTimeUnblocked() != null) {
                            if (m.getDate().after(b.getTimeBlocked()) && m.getDate().before(b.getTimeUnblocked())) {
                                blocked = true;
                            }
                        } else {
                            if (m.getDate().after(b.getTimeBlocked())) {
                                blocked = true;
                            }
                        }
                    }

                    if (!blocked) {
                        if (timeJoined.before(m.getDate()) || timeJoined.equals(m.getDate())) {
                            Calendar calendar = GregorianCalendar.getInstance();
                            calendar.setTime(m.getDate());
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                            dateFormat.setCalendar(calendar);
                            if (previousMessage != null) {
                                if (previousCalendar != null) {
                                    if (dateScanner.compareDates(m.getDate().getTime(), previousMessage.getDate().getTime()) != null) {
                                        messagesString.add(dateScanner.compareDates(m.getDate().getTime(), previousMessage.getDate().getTime()));
                                        messagesTypes.add(0);
                                        messagesSenders.add("");
                                    }
                                }
                            } else {
                                messagesString.add(dateScanner.compareDatesFirstMessage(new Date().getTime(), m.getDate().getTime()));
                                messagesTypes.add(0);
                                messagesSenders.add("");
                            }

                            // messagesString.add(m.getContent());
                            if (m.getContent() != null) {
                                messagesString.add(m.getContent());
                            } else {
                                if (m.getChatPicture() != null) {
                                    String base64Img = Base64.encodeBase64String(m.getChatPicture());
                                    messagesString.add("dsta:image/png;base64," + base64Img);
                                }
                            }

                            if (m.getUser().getId().equals(userId)) {
                                messagesTypes.add(1);
                                messagesSenders.add("");
                            } else {
                                messagesTypes.add(2);
                                if (previousMessage != null) {
                                    if (previousMessage.getUser().getUsername().equals(m.getUser().getUsername())) {
                                        messagesSenders.add("");
                                    } else {
                                        messagesSenders.add(m.getUser().getFirstname() + " " + m.getUser().getLastname());
                                    }
                                } else {
                                    messagesSenders.add(m.getUser().getFirstname() + " " + m.getUser().getLastname());
                                }
                            }
                            previousMessage = m;
                            previousCalendar = GregorianCalendar.getInstance();
                            previousCalendar.setTime(m.getDate());

                        }
                    }
                }
            } else {
                for (Chats m : messages) {
                    if (timeJoined.before(m.getDate()) || timeJoined.equals(m.getDate())) {
                        Calendar calendar = GregorianCalendar.getInstance();
                        calendar.setTime(m.getDate());
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        dateFormat.setCalendar(calendar);
                        if (previousMessage != null) {
                            if (previousCalendar != null) {
                                if (dateScanner.compareDates(m.getDate().getTime(), previousMessage.getDate().getTime()) != null) {
                                    messagesString.add(dateScanner.compareDates(m.getDate().getTime(), previousMessage.getDate().getTime()));
                                    messagesTypes.add(0);
                                    messagesSenders.add("");
                                }
                            }
                        } else {
                            messagesString.add(dateScanner.compareDatesFirstMessage(new Date().getTime(), m.getDate().getTime()));
                            messagesTypes.add(0);
                            messagesSenders.add("");
                        }

                        //messagesString.add(m.getContent());
                        if (m.getContent() != null) {
                            messagesString.add(m.getContent());
                        } else {
                            if (m.getChatPicture() != null) {
                                String base64Img = Base64.encodeBase64String(m.getChatPicture());
                                messagesString.add("dsta:image/png;base64," + base64Img);
                            }
                        }
                        if (m.getUser().getId().equals(userId)) {
                            messagesTypes.add(1);
                            messagesSenders.add("");
                        } else {
                            messagesTypes.add(2);
                            if (previousMessage != null) {
                                if (previousMessage.getUser().getUsername().equals(m.getUser().getUsername())) {
                                    messagesSenders.add("");
                                } else {
                                    messagesSenders.add(m.getUser().getFirstname() + " " + m.getUser().getLastname());
                                }
                            } else {
                                messagesSenders.add(m.getUser().getFirstname() + " " + m.getUser().getLastname());
                            }
                        }
                        previousMessage = m;
                        previousCalendar = GregorianCalendar.getInstance();
                        previousCalendar.setTime(m.getDate());

                    }
                }
            }

            result.add(messagesString);
            result.add(messagesTypes);
            result.add(messagesSenders);
            return result;
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * To know if a conversation is existing
     *
     * @param conv The name of the conversation
     * @return A boolean which says if the conversation exists or not
     */
    public Boolean isExistingConversation(String conv) {
        Conversation c = null;
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");
        if (nameAndOwner[1].equals("nobody")){
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                c = conversationData.findByName(convParams[1]).get(0);
            }
        } else {
            if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
                c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
            }
        }

        if (c != null) {
            return true;
        }
        return false;
    }

    /**
     * To create a new conversation
     *
     * @param conv The conversation parameters
     * @param timeJoined The date of the conversation creation
     * @return A boolean which says if the creation is OK
     */
    public Boolean createConversationWithUsers(String conv, Date timeJoined) {
        List<User> users = new ArrayList<>();
        String[] convParams = conv.split("-");
        String[] usersIds = convParams[1].split("_");

        for (String p : usersIds) {
            if (userData.get(Long.valueOf(p)) != null) {
                users.add(userData.get(Long.valueOf(p)));
            }
        }

        if (users.size() == usersIds.length) {

            Conversation c = null;
            String[] nameAndOwner = convParams[0].split("_");

            if (nameAndOwner[1].equals("nobody")) {
                if (!conversationData.findByName(convParams[1]).isEmpty()) {
                    c = conversationData.findByName(convParams[1]).get(0);
                }
            } else {
                if (conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).size() > 0) {
                    c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
                }
            }

            if (c == null) {
                c = new Conversation();
                if (nameAndOwner[1].equals("nobody")) {
                    c.setOwner(null);
                    c.setName(convParams[1]);
                } else {
                    c.setOwner(userData.get(Long.valueOf(nameAndOwner[1])));
                    c.setName(nameAndOwner[0]);
                }

                addConversation(c);
                for (User u : users) {
                    Participant p = new Participant();
                    p.setUser(u);
                    p.setConversation(c);
                    p.setTimeJoined(timeJoined);
                    p.setLastReadMessage(new Date());
                    p.setVisible(Boolean.TRUE);
                    p.setActive(Boolean.TRUE);
                    addParticipant(p);
                }
                return true;
            }
        }
        return false;
    }

    public Boolean checkIfImageData(String message) {
        if (message.indexOf("data:image") > -1) {
            return true;
        }
        return false;
    }

    /**
     * To add a message in a conversation
     *
     * @param username The sender username
     * @param conv The name of the conversation with some parameters
     * @param message The message content
     * @param d The date of the message
     * @return A boolean which says if the message has been added correctly
     */
    public Boolean addMessageWithUsers(String username, String conv, String message, Date d) {

        Conversation c = null;
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");
        boolean isImage = this.checkIfImageData(message);
        if (isImage) {
            String type = message.substring(message.indexOf("/") + 1, message.indexOf(";"));
            if (type != "") {
                message = message.replace("data:image/" + type + ";base64,", "");
            }
        }

        if (nameAndOwner[1].equals("nobody")) {
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                c = conversationData.findByName(convParams[1]).get(0);
            }
        } else {
            if (conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).size() > 0) {
                c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
            }
        }

        if (c != null) {
            List<Participant> participants = participantData.findByConv(c.getId());
            for (Participant p : participants) {
                if (!p.getVisible()) {
                    p.setVisible(Boolean.TRUE);
                    p.setTimeJoined(d);
                    participantData.update(p);
                }
            }
            Chats newMessage = new Chats();
            if (isImage) {
                newMessage.setChatPicture(Base64.decodeBase64(message));
            } else {
                newMessage.setContent(message);
            }
            newMessage.setDate(d);
            newMessage.setConversation(c);
            newMessage.setDeleted(Boolean.FALSE);
            newMessage.setUser(userData.getByUsername(username));
            addMessage(newMessage);
            return true;
        }
        return false;
    }

    /**
     * To get the ten most recent conversations names for a given user
     *
     * @param currentUser The corresponding user
     * @return The list of conversations names
     */
    public List<String> getConversationsNames(String currentUser) {
        List<String> result = new ArrayList<>();
        List<Chats> recentMessages = findMostRecent(currentUser);
        //long userId = userData.findByLogin(currentUser).get(0).getId();
        long userId = userData.getByUsername(currentUser).getId();
        for (Chats m : recentMessages) {
            long convId = m.getConversation().getId();
            if (participantData.findWithConvAndUser(convId, userId).get(0).getVisible() && participantData.findWithConvAndUser(convId, userId).get(0).getActive()) {
                result.add(m.getConversation().getName());
            }
        }
        return result;
    }

    /**
     * To get buttons identifiers for the list of conversations
     *
     * @param currentUser The corresponding user
     * @return The list of buttons identifiers
     */
    public List<String> getButtonsIdentifiers(String currentUser) {
        List<String> result = new ArrayList<>();
        List<Chats> recentMessages = findMostRecent(currentUser);
        //long userId = userData.findByLogin(currentUser).get(0).getId();
        long userId = userData.getByUsername(currentUser).getId();
        for (Chats m : recentMessages) {
            long convId = m.getConversation().getId();
            if (participantData.findWithConvAndUser(convId, userId).get(0).getVisible() && participantData.findWithConvAndUser(convId, userId).get(0).getActive()) {
                if (participantData.countParticipants(convId) <= 2) {
                    result.add("button_6r4s5G\\@pXF9NsYZ_nobody-" + m.getConversation().getName());
                } else {
                    List<Participant> participants = participantData.findByConv(convId);
                    List<Long> participantsLongList = new ArrayList<>();
                    for (int i = 0; i < participants.size(); i++) {
                        participantsLongList.add(participants.get(i).getUser().getId());
                    }

                    String participantString = "";
                    for (int j = 0; j < participantsLongList.size(); j++) {
                        if (j < participantsLongList.size() - 1) {
                            participantString += participantsLongList.get(j) + "_";
                        } else {
                            participantString += participantsLongList.get(j);
                        }
                    }

                    result.add("button_" + m.getConversation().getName() + "_" + m.getConversation().getOwner().getId() + "-" + participantString);
                }
            }
        }
        return result;
    }

    /**
     *
     * @param currentUser
     * @return
     */
    public List<String> getConversationsIdentifiers(String currentUser) {
        List<String> result = new ArrayList<>();
        List<Chats> recentMessages = findMostRecent(currentUser);
        //long userId = userData.findByLogin(currentUser).get(0).getId();
        long userId = userData.getByUsername(currentUser).getId();
        for (Chats m : recentMessages) {
            long convId = m.getConversation().getId();
            if (participantData.findWithConvAndUser(convId, userId).get(0).getVisible() && participantData.findWithConvAndUser(convId, userId).get(0).getActive()) {
                if (participantData.countParticipants(convId) <= 2) {
                    result.add("6r4s5G\\@pXF9NsYZ_nobody-" + m.getConversation().getName());
                } else {
                    List<Participant> participants = participantData.findByConv(convId);
                    List<Long> participantsLongList = new ArrayList<>();
                    for (int i = 0; i < participants.size(); i++) {
                        participantsLongList.add(participants.get(i).getUser().getId());
                    }

                    String participantString = "";
                    for (int j = 0; j < participantsLongList.size(); j++) {
                        if (j < participantsLongList.size() - 1) {
                            participantString += participantsLongList.get(j) + "_";
                        } else {
                            participantString += participantsLongList.get(j);
                        }
                    }

                    result.add(m.getConversation().getName() + "_" + m.getConversation().getOwner().getId() + "-" + participantString);
                }
            }
        }
        return result;
    }

    /**
     * To find the most recent conversations participants
     *
     * @param currentUser The current user username
     * @return The participants as a String chain for each conversation
     */
    public List<String> getRecentConv(String currentUser) {
        List<String> result = new ArrayList<>();
        List<Chats> recentMessages = findMostRecent(currentUser);
        //long userId = userData.findByLogin(currentUser).get(0).getId();
        long userId = userData.getByUsername(currentUser).getId();
        for (Chats m : recentMessages) {
            long convId = m.getConversation().getId();
            List<Participant> participants = participantData.findByConv(convId);
            if (participantData.findWithConvAndUser(convId, userId).get(0).getVisible() && participantData.findWithConvAndUser(convId, userId).get(0).getActive()) {
                if (participants.size() <= 2) {
                    for (int i = 0; i < participants.size(); i++) {
                        if (!participants.get(i).getUser().getUsername().equals(currentUser)) {
                            result.add(participants.get(i).getUser().getFirstname() + " " + participants.get(i).getUser().getLastname());
                        }
                    }
                } else {
                    result.add(conversationData.find(convId).getName());
                }
            }
        }
        return result;
    }

    /**
     * To find the ten most recent conversations last message
     *
     * @param currentUser The current user username
     * @return The ten most recent conversations last message with the date
     */
    public List<String> getRecentMessages(String currentUser) {
        List<String> result = new ArrayList<>();
        List<Chats> recentMessages = findMostRecent(currentUser);
        //long userId = userData.findByLogin(currentUser).get(0).getId();
        long userId = userData.getByUsername(currentUser).getId();
        for (Chats m : recentMessages) {
            long convId = m.getConversation().getId();
            if (participantData.findWithConvAndUser(convId, userId).get(0).getVisible() && participantData.findWithConvAndUser(convId, userId).get(0).getActive()) {
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.setTime(m.getDate());
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
                dateFormat.setCalendar(calendar);
                result.add(dateFormat.format(calendar.getTime()) + ", " + m.getUser().getFirstname() + " " + m.getUser().getLastname() + " : " + m.getContent());
            }
        }
        return result;
    }

    /**
     * To find the user list for a term parameter
     *
     * @param term The term that we want to find
     * @param currentUser The connected user
     * @return A list that contains lists, those lists contain full names and
     * usernames for each user
     */
    public List<List> getUserList(String term, String currentUser) {
        List<List> resultList = new ArrayList<>();
        List<User> userList = findUserByTerm(term + "%", currentUser);
        User user = userData.getByUsername(currentUser);

        for (User u : userList) {
            if (u.equals(user)) {
                userList.remove(user);
            }
        }

        for (User u : userList) {
            ArrayList<String> tempList = new ArrayList<>();
            tempList.add(u.getFirstname() + " " + u.getLastname());
            tempList.add(u.getUsername());
            resultList.add(tempList);
        }

        return resultList;
    }

    /**
     * To update the last read message for a user in a conversation
     *
     * @param d The last read message date
     * @param conv The conversation identifier
     * @param username The current user username
     * @return A boolean which indicates if the update is OK
     */
    public Boolean updateLastReadMessage(Date d, String conv, String username) {

        Conversation c = null;
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        if (nameAndOwner[1].equals("nobody")) {
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                c = conversationData.findByName(convParams[1]).get(0);
            }
        } else {
            if (conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).size() > 0) {
                c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
            }
        }

        if (c != null) {
            List<Participant> participants = participantData.findByConv(c.getId());
            for (Participant p : participants) {
                if (p.getUser().getUsername().equals(username)) {
                    p.setLastReadMessage(d);
                    participantData.update(p);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * To know how many messages are unread for the most recent conversations
     *
     * @param currentUser The current user username
     * @return A list of integers which indicates the number of unread messages
     */
    public List<Integer> getUnreadMessages(String currentUser) {
        List<Integer> result = new ArrayList<>();
        User u = userData.getByUsername(currentUser);
        List<Chats> unreadMessages = messageData.findUnreadMessages(u.getId());
        List<Chats> recentMessages = findMostRecent(currentUser);

        int cpt;
        for (Chats m : recentMessages) {
            long convId = m.getConversation().getId();
            if (participantData.findWithConvAndUser(convId, u.getId()).get(0).getVisible() && participantData.findWithConvAndUser(convId, u.getId()).get(0).getActive()) {
                cpt = 0;
                for (int i = 0; i < unreadMessages.size(); i++) {
                    if (m.getConversation().getId().longValue() == unreadMessages.get(i).getConversation().getId().longValue()) {
                        boolean blocked = false;
                        if (participantData.findByConv(convId).size() == 2) {
                            long friendId = 0L;
                            List<Participant> participantList = participantData.findByConv(convId);
                            for (Participant p : participantList) {
                                if (p.getUser().getId().compareTo(u.getId()) != 0) {
                                    friendId = p.getUser().getId();
                                }
                            }
                            List<BlockInterval> blockIntervalList = blockIntervalData.findAllByUsersIds(u.getId(), friendId);
                            for (BlockInterval b : blockIntervalList) {
                                if (b.getTimeUnblocked() == null) {
                                    if (unreadMessages.get(i).getDate().after(b.getTimeBlocked())) {
                                        blocked = true;
                                    }
                                } else {
                                    if (unreadMessages.get(i).getDate().after(b.getTimeBlocked()) && unreadMessages.get(i).getDate().before(b.getTimeUnblocked())) {
                                        blocked = true;
                                    }
                                }
                            }
                        }
                        if (!blocked) {
                            cpt++;
                        }
                    }
                }
                result.add(cpt);
            }
        }

        return result;

    }

    /**
     * To make a conversation invisible for an user if he decides to delete it
     *
     * @param conv The conversation ID
     * @param currentUser The current user username
     * @return A boolean which says if the modification is OK
     */
    public Boolean disableConversation(String conv, String currentUser) {

        Conversation c = null;
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");
        String[] participants = convParams[1].split("_");

        List<User> users = new ArrayList<>();
        for (int i = 0; i < participants.length; i++) {
            if (userData.get(Long.valueOf(participants[i])) != null) {
                users.add(userData.get(Long.valueOf(participants[i])));
            }
        }

        if (nameAndOwner[1].equals("nobody")) {
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                c = conversationData.findByName(convParams[1]).get(0);
            }
        } else {
            if (conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).size() > 0) {
                c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
            }
        }

        if (users.size() == participants.length && c != null) {
            User myUser = userData.getByUsername(currentUser);
            Participant p = participantData.findWithConvAndUser(c.getId(), myUser.getId()).get(0);
            if (p != null) {
                p.setVisible(Boolean.FALSE);
                participantData.update(p);
                return true;
            }
        }
        return false;
    }

    /**
     * Sets the visibility to true for the corresponding user participant in a
     * specific conversation
     *
     * @param conv The name of the conversation with some parameters
     * @param currentUser The current user username
     * @return A boolean which indicates if the modification is OK
     */
    public Boolean checkVisibility(String conv, String currentUser) {

        User myUser = userData.getByUsername(currentUser);
        Conversation c = null;
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        if (nameAndOwner[1].equals("nobody")) {
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                c = conversationData.findByName(convParams[1]).get(0);
            }
        } else {
            if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
                c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
            }
        }

        if (c != null) {
            if (!participantData.findWithConvAndUser(c.getId(), myUser.getId()).get(0).getVisible()) {
                Participant p = participantData.findWithConvAndUser(c.getId(), myUser.getId()).get(0);
                changeTimeJoined(conv, currentUser);
                p.setVisible(Boolean.TRUE);
                participantData.update(p);
                return true;
            }
        }
        return false;
    }

    /**
     * To check if a participant is active in a conversation
     *
     * @param conv The conversation that we want to check
     * @param currentUser The user that we want to check
     * @return A boolean which indicates if the user is active in this
     * conversation
     */
    public Boolean checkActive(String conv, String currentUser) {

        User myUser = userData.getByUsername(currentUser);
        Conversation c = null;
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        if (nameAndOwner[1].equals("nobody")) {
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                c = conversationData.findByName(convParams[1]).get(0);
            }
        } else {
            if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
                c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
            }
        }

        if (c != null) {
            return participantData.findWithConvAndUser(c.getId(), myUser.getId()).get(0).getActive();
        }
        return false;
    }

    /**
     * Changes the time when a user joined a conversation. That can be the case
     * if the user deleted the conversation and receive a new message
     *
     * @param conv The name of the conversation
     * @param currentUser The current user username
     * @return A boolean which says if the modification is OK
     */
    public Boolean changeTimeJoined(String conv, String currentUser) {

        User myUser = userData.getByUsername(currentUser);
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");
        Long convId = null;

        if (nameAndOwner[1].equals("nobody")) {
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                convId = conversationData.findByName(convParams[1]).get(0).getId();
            }
        } else {
            if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
                convId = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0).getId();
            }
        }

        if (convId != null) {
            Participant p = participantData.findWithConvAndUser(convId, myUser.getId()).get(0);
            if (!p.getVisible()) {
                p.setTimeJoined(new Date());
            }
            participantData.update(p);
            return true;
        }
        return false;
    }

    /**
     * Return the last message date
     *
     * @param conv The conversation name with some parameters
     * @param currentUser The current user username
     * @return The last message date
     */

    
    
    public Date getLastMessageDate(String conv, String currentUser) {

        Conversation c = null;
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        if (nameAndOwner[1].equals("nobody")) {
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                c = conversationData.findByName(convParams[1]).get(0);
            }
        } else {
            if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
                c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
            }
        }

        if (c != null) {
            List<Chats> recentMessages = findMostRecent(currentUser);
            for (int i = 0; i < recentMessages.size(); i++) {
                if (Long.compare(recentMessages.get(i).getConversation().getId(), c.getId()) == 0) {
                    return recentMessages.get(i).getDate();
                }
            }
        }
        return null;
    }

    /**
     * To find the most recent messages for each of the ten most recent
     * conversations
     *
     * @param username The current user username
     * @return A list of Chats
     */
    private List<Chats> findMostRecent(String username) {
        List<Chats> messages = messageData.findMostRecent(username);
        List<Chats> result = new ArrayList<>();
        List<Long> convIds = new ArrayList<>();
        int cptsize = 0;
        int cptConv = 0;

        while (cptsize < messages.size()) {
            Chats m = messages.get(cptsize);
            List<Participant> list = participantData.findByConv(m.getConversation().getId());

            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).getUser().getUsername().equals(username) && cptConv < 10) {
                    boolean blocked = false;
                    if (list.size() == 2) {
                        List<BlockInterval> blockIntervalList;
                        if (j == 0) {
                            blockIntervalList = blockIntervalData.findAllByUsersIds(list.get(j).getUser().getId(), list.get(1).getUser().getId());
                        } else {
                            blockIntervalList = blockIntervalData.findAllByUsersIds(list.get(j).getUser().getId(), list.get(0).getUser().getId());
                        }
                        for (BlockInterval b : blockIntervalList) {
                            if (b.getTimeUnblocked() == null) {
                                if (m.getDate().after(b.getTimeBlocked())) {
                                    blocked = true;
                                }
                            } else {
                                if (m.getDate().after(b.getTimeBlocked()) && m.getDate().before(b.getTimeUnblocked())) {
                                    blocked = true;
                                }
                            }
                        }
                    }

                    if (!blocked && !convIds.contains(m.getConversation().getId())) {
                        result.add(m);
                        convIds.add(m.getConversation().getId());
                        cptConv++;
                    }
                }
            }
            cptsize++;
        }

        return result;
    }

    /**
     * To find the lists of User which could correspond for a given term
     *
     * @param term The term that we want to find
     * @param currentUser The current user username
     * @return Each conversation found correspond to a list of User
     * (participants)
     */
    public List<List> findByTerm(String term, String currentUser) {

        String lookingFor = term + '%';

        List<User> users = userData.getByTerm(lookingFor);
        //List<Friend> friendList = friendData.findByUser(currentUser);
        Preference preference = friendData.getPreference(userData.getByUsername(currentUser));
        List<Preference> friendList = friendData.getAccept(preference);
        List<Preference> friendList2 = friendData.getFind(preference);
        friendList.addAll(friendList2);

        List<User> toRemove = new ArrayList<>();

        for (int i = 0; i < users.size(); i++) {
            boolean foundFriend = false;
            for (int j = 0; j < friendList.size(); j++) {
                if (users.get(i).getUsername().equals(friendList.get(j).getUser().getUsername())) {
                    foundFriend = true;
                }
            }
            if (!foundFriend) {
                toRemove.add(users.get(i));
            }
        }

        for (int k = 0; k < toRemove.size(); k++) {
            users.remove(toRemove.get(k));
        }

        String[] lookingForConv = lookingFor.split(" ");

        List<Conversation> intersectionConversations = new ArrayList<>();

        for (int i = 0; i < lookingForConv.length; i++) {

            List<User> usersBis = userData.getByTermBis(lookingForConv[i]);
            List<User> toRemoveBis = new ArrayList<>();

            for (int j = 0; j < usersBis.size(); j++) {
                boolean foundFriend = false;
                for (int k = 0; k < friendList.size(); k++) {
                    if (usersBis.get(j).getUsername().equals(friendList.get(k).getUser().getUsername())) {
                        foundFriend = true;
                    }
                }
                if (!foundFriend) {
                    toRemoveBis.add(usersBis.get(j));
                }
            }

            for (int j = 0; j < toRemoveBis.size(); j++) {
                usersBis.remove(toRemoveBis.get(j));
            }

            List<Conversation> queryConversations = conversationData.findConvByTerm(lookingForConv[i]);

            String friendChain = lookingForConv[i];
            if (i == lookingForConv.length - 1) {
                friendChain = lookingForConv[i].substring(0, lookingForConv[i].length() - 1);
            }

            boolean foundFriend = false;
            for (int j = 0; j < usersBis.size(); j++) {
                if (usersBis.get(j).getFirstname().toLowerCase().startsWith(friendChain.toLowerCase()) || usersBis.get(j).getLastname().toLowerCase().startsWith(friendChain.toLowerCase())) {
                    foundFriend = true;
                }
            }

            List<Conversation> queryConversations2 = new ArrayList<>();
            if (foundFriend) {
                queryConversations2 = conversationData.findParticipantsByTerm(lookingForConv[i]);
            }

            if (i == 0) {
                intersectionConversations = new ArrayList<>(queryConversations);
                intersectionConversations.addAll(queryConversations2);
            } else {
                queryConversations.addAll(queryConversations2);
                intersectionConversations.retainAll(queryConversations);
            }
        }

        List<Conversation> conversations = new ArrayList<>();

        for (int i = 0; i < intersectionConversations.size(); i++) {
            if (!conversations.contains(intersectionConversations.get(i))) {
                conversations.add(intersectionConversations.get(i));
            }
        }

        List<String> finalFullname = new ArrayList<>();
        List<String> finalUsername = new ArrayList<>();

        for (int i = 0; i < users.size(); i++) {
            if (!users.get(i).getUsername().equals(currentUser)) {
                finalFullname.add(users.get(i).getFirstname() + " " + users.get(i).getLastname());
                finalUsername.add("" + users.get(i).getId());
            }
        }

        List<User> userList = new ArrayList<>();
        for (int i = 0; i < finalFullname.size(); i++) {
            User u = new User();
            u.setFirstname(finalFullname.get(i));
            u.setUsername(finalUsername.get(i));
            userList.add(u);
        }

        userList.sort((o1, o2) -> o1.getFirstname().toLowerCase().compareTo(o2.getFirstname().toLowerCase()));

        List<String> tmpFullname = new ArrayList<>();
        List<String> tmpUsername = new ArrayList<>();

        for (int j = 0; j < conversations.size(); j++) {
            if (participantData.countParticipants(conversations.get(j).getId()) > 2) {

                List<Participant> participants = participantData.findByConv(conversations.get(j).getId());
                String nton = conversations.get(j).getName() + "_" + conversations.get(j).getOwner().getId() + "-";
                String fullnames = "";

                for (int k = 0; k < participants.size(); k++) {
                    if (k < participants.size() - 1) {
                        nton += participants.get(k).getUser().getId() + "_";
                        fullnames += participants.get(k).getUser().getFirstname() + " " + participants.get(k).getUser().getLastname() + ", ";
                    } else {
                        nton += participants.get(k).getUser().getId();
                        fullnames += participants.get(k).getUser().getFirstname() + " " + participants.get(k).getUser().getLastname();
                    }
                }
                tmpFullname.add(fullnames);
                tmpUsername.add(nton);
            }
        }

        List<User> userListBis = new ArrayList<>();
        for (int i = 0; i < tmpFullname.size(); i++) {
            User u = new User();
            u.setFirstname(tmpFullname.get(i));
            u.setUsername(tmpUsername.get(i));
            userListBis.add(u);
        }

        userListBis.sort((o1, o2) -> o1.getFirstname().toLowerCase().compareTo(o2.getFirstname().toLowerCase()));
        userList.addAll(userListBis);

        List<List> finalResult = new ArrayList<>();

        for (int j = 0; j < userList.size(); j++) {
            List<String> simpleResult = new ArrayList<>();
            simpleResult.add(userList.get(j).getFirstname());
            simpleResult.add(userList.get(j).getUsername());
            finalResult.add(simpleResult);
        }

        if (finalResult.isEmpty()) {
            return new ArrayList<>();
        } else if (finalResult.size() < 5) {
            return finalResult.subList(0, finalResult.size());
        } else {
            return finalResult.subList(0, 5);
        }
    }

    /**
     * To find a list of users corresponding to a given term
     *
     * @param term The term that we want to find
     * @param currentUser The connected user
     * @return A list of Users
     */
    private List<User> findUserByTerm(String term, String currentUser) {

        List<User> resultLastElement = userData.getByTerm(term);
        Preference preference = friendData.getPreference(userData.getByUsername(currentUser));
        List<Preference> friendList = friendData.getAccept(preference);
        List<Preference> friendList2 = friendData.getFind(preference);
        friendList.addAll(friendList2);

        List<User> toRemove = new ArrayList<>();

        for (int i = 0; i < resultLastElement.size(); i++) {
            boolean foundFriend = false;
            for (int j = 0; j < friendList.size(); j++) {
                if (resultLastElement.get(i).getUsername().equals(friendList.get(j).getUser().getUsername())) {
                    foundFriend = true;
                }
            }
            if (!foundFriend) {
                toRemove.add(resultLastElement.get(i));
            }
        }

        for (int k = 0; k < toRemove.size(); k++) {
            resultLastElement.remove(toRemove.get(k));
        }

        resultLastElement.sort((o1, o2) -> o1.getFirstname().toLowerCase().compareTo(o2.getFirstname().toLowerCase()));

        if (resultLastElement.contains(userData.getByUsername(currentUser))) {
            resultLastElement.remove(userData.getByUsername(currentUser));
        }
        if (!resultLastElement.isEmpty()) {
            return resultLastElement;
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * To find the last message username
     *
     * @param conv The name of the conversation and some parameters
     * @param currentUser The current user username
     * @return The last message username
     */
    public String getLastMessageUsername(String conv, String currentUser) {

        Conversation c = null;
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        if (nameAndOwner[1].equals("nobody")) {
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                c = conversationData.findByName(convParams[1]).get(0);
            }
        } else {
            if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
                c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
            }
        }

        if (c != null) {
            List<Chats> recentMessages = findMostRecent(currentUser);
            for (int i = 0; i < recentMessages.size(); i++) {
                if (Long.compare(recentMessages.get(i).getConversation().getId(), c.getId()) == 0) {
                    return recentMessages.get(i).getUser().getUsername();
                }
            }
        }
        return null;
    }

    /**
     * Checks if an user is in a conversation or not
     *
     * @param conversation The name of the conversation
     * @param term The name of the user that we want to find
     * @return A boolean which indicates if the participant is in the
     * conversation
     */
    public Boolean verifyParticipantInConv(String conversation, String term) {

        String[] convParams = conversation.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        Conversation c = null;
        if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
            c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
        }

        if (c != null) {
            List<Participant> participants = participantData.findByConv(c.getId());
            List<String> participantsNames = new ArrayList<>();

            for (int i = 0; i < participants.size(); i++) {
                participantsNames.add(participants.get(i).getUser().getUsername());
            }

            if (!participantsNames.contains(term)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * To add a new participant in a conversation
     *
     * @param conversation The corresponding conversation
     * @param term The user that we want to add to the conversation
     * @param currentUser The connected user
     * @return A boolean which indicates if the adding was OK or not
     */
    public Boolean addParticipantInConv(String conversation, String term, String currentUser) {

        String[] convParams = conversation.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        if (userData.getByUsername(term) != null) {
            Conversation c = null;
            if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
                c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
            }

            if (c != null) {
                Participant p = new Participant();
                p.setActive(Boolean.TRUE);
                p.setLastReadMessage(new Date());
                p.setTimeJoined(new Date());
                p.setVisible(Boolean.TRUE);
                p.setConversation(c);
                p.setUser(userData.getByUsername(term));
                participantData.save(p);
                return true;
            }
        }

        return false;
    }

    /**
     * Permits the connected user to leave a conversation
     *
     * @param conversation The corresponding conversation
     * @param currentUser The connected user
     * @return A boolean which indicates if the connected user was able to leave
     * the conversation or not
     */
    public Boolean leaveConversation(String conversation, String currentUser) {

        String[] convParams = conversation.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        Conversation c = null;

        if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
            c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
        }

        if (c != null) {
            if (userData.getByUsername(currentUser) != null) {
                List<Participant> result = participantData.findWithConvAndUser(c.getId(), userData.getByUsername(currentUser).getId());
                if (!result.isEmpty()) {
                    Participant p = result.get(0);
                    p.setLastReadMessage(new Date());
                    p.setActive(Boolean.FALSE);
                    participantData.update(p);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Indicates the number of participants in a conversation
     *
     * @param conv The conversation that we want to check
     * @return The number of participants
     */
    public Integer countParticipants(String conv) {
        Conversation c = null;
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        if (nameAndOwner[1].equals("nobody")) {
            if (!conversationData.findByName(convParams[1]).isEmpty()) {
                c = conversationData.findByName(convParams[1]).get(0);
            }
        } else {
            if (!conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).isEmpty()) {
                c = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1])).get(0);
            }
        }

        if (c != null) {
            return participantData.countParticipants(c.getId());
        } else {
            return 0;
        }
    }

    /**
     * To get the full name of a friend participating in a 1to1 conversation
     *
     * @param conv The corresponding conversation
     * @param currentUser The connected user
     * @return The friend full name as a String chain
     */
    public String getFriendFullname(String conv, String currentUser) {
        String[] convParams = conv.split("-");
        String[] ids = convParams[1].split("_");

        User u = userData.getByUsername(currentUser);

        if (u != null) {
            for (int i = 0; i < ids.length; i++) {
                if (!u.getId().equals(Long.valueOf(ids[i]))) {
                    User friend = userData.get(Long.valueOf(ids[i]));
                    if (friend != null) {
                        return friend.getFirstname() + " " + friend.getLastname();
                    }
                }
            }
        }

        return "";
    }

    /**
     * To get the full name of an user with its username
     *
     * @param login The login that we need to inspect
     * @return The full name as a String
     */
    public String getFullnameByLogin(String login) {
        User u = userData.getByUsername(login);
        if (u != null) {
            return u.getFirstname() + " " + u.getLastname();
        } else {
            return "";
        }
    }

    /**
     * Return the participants full names for a NtoN conversation
     *
     * @param conv The corresponding conversation
     * @param currentUser The connected user
     * @return The participants full names as a String
     */
    public String getParticipantsFullnames(String conv, String currentUser) {

        String result = "";
        String[] convParams = conv.split("-");
        String[] ids = convParams[1].split("_");

        for (int i = 0; i < ids.length; i++) {
            User friend = userData.get(Long.valueOf(ids[i]));
            if (!currentUser.equals(friend.getUsername())) {
                if (i < ids.length - 1) {
                    if (i == ids.length - 2 && currentUser.equals(userData.get(Long.valueOf(ids[ids.length - 1])))) {
                        result += friend.getFirstname() + "0" + friend.getLastname();
                    } else {
                        result += friend.getFirstname() + "0" + friend.getLastname() + "_";
                    }
                } else {
                    result += friend.getFirstname() + "0" + friend.getLastname();
                }
            }
        }
        return result;
    }

    /**
     * To re-open an existing conversation with a new participant
     *
     * @param conv The old conversation identifier
     * @param newParticipant The new participant username
     * @return The new conversation identifier
     */
    public String openExistingConv(String conv, String newParticipant) {

        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");
        String[] participants = convParams[1].split("_");

        List<User> concernedUsers = new ArrayList<>();
        for (int i = 0; i < participants.length; i++) {
            if (!userData.get(Long.valueOf(participants[i])).getUsername().equals("")) {
                User u = new User();
                u.setId(Long.valueOf(participants[i]));
                u.setUsername(userData.get(Long.valueOf(participants[i])).getUsername());
                concernedUsers.add(u);
            }
        }

        User u = userData.getByUsername(newParticipant);

        if (u != null) {
            concernedUsers.add(u);

            if (concernedUsers.size() == participants.length + 1) {
                List<Conversation> convList = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1]));
                if (!convList.isEmpty()) {

                    List<String> fullnames = new ArrayList<>();
                    for (int i = 0; i < concernedUsers.size(); i++) {
                        fullnames.add(concernedUsers.get(i).getUsername());
                    }

                    fullnames.sort(String::compareToIgnoreCase);
                    String newConv = convParams[0] + "-";

                    for (int j = 0; j < fullnames.size(); j++) {
                        if (j < fullnames.size() - 1) {
                            newConv += userData.getByUsername(fullnames.get(j)).getId() + "_";
                        } else if (j < fullnames.size()) {
                            newConv += userData.getByUsername(fullnames.get(j)).getId();
                        }
                    }
                    return newConv;
                }
            }
        }

        return "";
    }

    /**
     * To modify the name of a conversation
     *
     * @param previousName The conversation previous name
     * @param newName The conversation new name
     * @param owner The owner ID
     * @return A boolean which indicates if the modification was OK or not
     */
    public Boolean modifyConvName(String previousName, String newName, String owner) {
        List<Conversation> convList = conversationData.findByNameAndOwner(previousName, Long.valueOf(owner));

        if (!convList.isEmpty()) {
            if (convList.get(0) != null) {
                Conversation c = convList.get(0);
                c.setName(newName);
                conversationData.update(c);
                return true;
            }
        }
        return false;
    }

    /**
     * To store a new picture as a NtoN conversation picture
     *
     * @param image The image that we need to store with a base64 encryption
     * @param conv The concerned conversation
     * @return A boolean which indicates if the image was stored correctly
     */
    public Boolean storeNewConvPicture(String image, String conv) {
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        List<Conversation> convList = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1]));

        if (!convList.isEmpty()) {
            if (convList.get(0) != null) {
                Conversation c = convList.get(0);
                image = image.replace("data:image/png;base64,", "");
                c.setConvPicture(Base64.decodeBase64(image));

                conversationData.update(c);
                return true;
            }
        }
        return false;
    }

    /**
     * To get all the conversation pictures for the most recent conversations
     *
     * @param currentUser The connected user
     * @return A list of picture identifiers
     */
    public List<String> getConversationsPictures(String currentUser) {

        List<String> picturesIdentifiers = new ArrayList<>();
        List<Chats> recentMessages = findMostRecent(currentUser);
        long userId = userData.getByUsername(currentUser).getId();

        for (Chats m : recentMessages) {
            long convId = m.getConversation().getId();
            if (participantData.findWithConvAndUser(convId, userId).get(0).getVisible() && participantData.findWithConvAndUser(convId, userId).get(0).getActive()) {
                if (participantData.countParticipants(convId) <= 2) {
                    picturesIdentifiers.add("");
                } else {
                    List<Participant> participants = participantData.findByConv(convId);
                    List<Long> participantsLongList = new ArrayList<>();
                    for (int i = 0; i < participants.size(); i++) {
                        participantsLongList.add(participants.get(i).getUser().getId());
                    }

                    String participantString = "";
                    for (int j = 0; j < participantsLongList.size(); j++) {
                        if (j < participantsLongList.size() - 1) {
                            participantString += participantsLongList.get(j) + "_";
                        } else {
                            participantString += participantsLongList.get(j);
                        }
                    }

                    if (conversationData.find(convId).getConvPicture() != null) {
                        byte[] convPictureBytes = conversationData.find(convId).getConvPicture();
                        picturesIdentifiers.add(m.getConversation().getName() + "_" + m.getConversation().getOwner().getId() + "-" + participantString + " " + "data:image/png;base64," + Base64.encodeBase64String(convPictureBytes));
                    } else {
                        picturesIdentifiers.add("");
                    }
                }
            }
        }
        return picturesIdentifiers;
    }

    public Date getTimeJoined(String conv, String currentUser) {
        String[] convParams = conv.split("-");
        String[] nameAndOwner = convParams[0].split("_");

        List<Conversation> convList;
        if (nameAndOwner[1].equals("nobody")) {
            convList = conversationData.findByName(convParams[1]);
        } else {
            convList = conversationData.findByNameAndOwner(nameAndOwner[0], Long.valueOf(nameAndOwner[1]));
        }

        if (!convList.isEmpty()) {
            if (convList.get(0) != null) {

                Conversation c = convList.get(0);
                long convId = c.getId();

                Participant p = participantData.findWithConvAndUser(convId, userData.getByUsername(currentUser).getId()).get(0);
                return p.getTimeJoined();
            }
        }

        return null;
    }

    public Boolean verifyNameAndOwner(String convName, String username) {

        List<Conversation> conversations = conversationData.findByNameAndOwner(convName, userData.getByUsername(username).getId());
        if (conversations.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * To know if a blocking relation is active between two users
     *
     * @param friendname The friend username
     * @param currentUser The connected username
     * @return A boolean which indicates the blocking relation is active or not
     */
    public Boolean loadBlockingRelation(String friendname, String currentUser) {
        if (userData.getByUsername(currentUser) != null && userData.getByUsername(friendname) != null) {
            List<BlockInterval> blockIntervalList = blockIntervalData.findCurrentByUsersIds(userData.getByUsername(currentUser).getId(), userData.getByUsername(friendname).getId());
            return (blockIntervalList.size() > 0);
        }
        return false;
    }

    /**
     * Permits to block another user
     *
     * @param friendname The friend username
     * @param currentUser The connected user
     * @return A boolean which indicates if the friend was blocked properly or
     * not
     */
    public Boolean blockUser(String friendname, String currentUser) {

        if (userData.getByUsername(currentUser) != null && userData.getByUsername(friendname) != null) {
            UserToUser u;
            User user1 = userData.getByUsername(currentUser);
            User user2 = userData.getByUsername(friendname);
            if (userToUserData.findByUsersIds(user1.getId(), user2.getId()).isEmpty()) {
                u = new UserToUser();
                u.setUser1Id(user1);
                u.setUser2Id(user2);
                u.setBlockIntervals(new ArrayList<>());
                u.setBlocked(Boolean.TRUE);
                userToUserData.save(u);

                BlockInterval b = new BlockInterval();
                b.setTimeBlocked(new Date());
                b.setTimeUnblocked(null);
                b.setUsertouserId(u);
                blockIntervalData.save(b);
            } else {
                u = userToUserData.findByUsersIds(user1.getId(), user2.getId()).get(0);
                u.setBlocked(Boolean.TRUE);
                userToUserData.update(u);

                BlockInterval b = blockIntervalData.findByRelation(u.getId());
                b.setTimeBlocked(new Date());
                b.setTimeUnblocked(null);
                b.setUsertouserId(u);
                blockIntervalData.save(b);
            }

            return true;
        }
        return false;
    }

    /**
     * Permits to unblock a friend
     *
     * @param friendname The friend username
     * @param currentUser The connected user username
     * @return A boolean which indicates if the friend was unblocked properly or
     * not
     */
    public Boolean unblockUser(String friendname, String currentUser) {
        if (userData.getByUsername(currentUser) != null && userData.getByUsername(friendname) != null) {
            List<UserToUser> userToUserList = userToUserData.findByUsersIds(userData.getByUsername(currentUser).getId(), userData.getByUsername(friendname).getId());
            List<BlockInterval> blockIntervalList = blockIntervalData.findCurrentByUsersIds(userData.getByUsername(currentUser).getId(), userData.getByUsername(friendname).getId());

            if (userToUserList.size() > 0 && blockIntervalList.size() > 0) {
                UserToUser u = userToUserList.get(0);
                u.setBlocked(Boolean.FALSE);
                userToUserData.update(u);

                BlockInterval b = blockIntervalList.get(0);
                b.setTimeUnblocked(new Date());
                blockIntervalData.update(b);
                return true;
            }
        }
        return false;
    }


	@Override
    public List getMembersInConversation(JSONObject object) {
        String userString = object.getString("member");
        List<String> user = new ArrayList<String>();
        String[] userIdArray = userString.split("\\-");

        for (int i = 0; i < userIdArray.length; i++) {
            String userName = userData.get(Long.valueOf(userIdArray[i])).getUsername();
            user.add(userName);
        }
        return user;
    }

}