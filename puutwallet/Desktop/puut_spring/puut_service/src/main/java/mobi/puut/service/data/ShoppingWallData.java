package mobi.puut.service.data;

import java.util.List;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.ShoppingGroup;
import mobi.puut.service.entities.shoppingwall.ShoppingGroupUser;
import mobi.puut.service.entities.shoppingwall.ShoppingList;
import mobi.puut.service.entities.shoppingwall.ShoppingListUser;
import mobi.puut.service.entities.users.User;

public interface ShoppingWallData {

	
	public List<ShoppingGroup> getGroups();
	public List<ShoppingGroupUser> getUsers(User user);
	public Result createGroup(ShoppingGroup group, String login);
	public Result joinGroup(Long id, String login);
	public Result leaveGroup(Long id, String login);
	public List<ShoppingList> getLists(User user, Long list_type);
	public List<ShoppingListUser> getListUsers(User user);
	public Result createList(ShoppingList list, String login);
	public Result joinList(Long id, String login);
	public Result leaveList(Long id, String login);
	public ShoppingGroup getShoppingGroupById(Long id);
	public List<ShoppingGroupUser> getShoppingGroupUsers(ShoppingGroup shoppinggroup);

	 public ShoppingGroupUser getGroupUserByGroupAndUser(User user, ShoppingGroup shoppinggroup,Boolean approved);
	 public ShoppingGroupUser getNonModeratorGroupUser(User user, ShoppingGroup shoppinggroup);
	 public ShoppingGroupUser createShoppingGroupUser(ShoppingGroupUser shoppingGroupUser) ;
	 public Boolean checkModerator(User user, ShoppingGroup shoppinggroup) ;
	 public Result editGroup(ShoppingGroup group);
	 public Boolean isShoppingListUser(User user, ShoppingList shoppinglist);
	 public Result editShoppingList(ShoppingList shoppinglist);
	 public List<User> getGroupUsers(ShoppingGroup shoppinggroup) ;
	 public List<ShoppingGroup> getShoppingGroupByName(String name); 
	 
}
