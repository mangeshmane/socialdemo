package mobi.puut.service;

import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.stereotype.Service;

@Service
public class MailUtil {

    public void sendMail(String subject, String content, String recipient) throws MessagingException {
        String host = "mail.puutwallet.com";
        String username = "accounts-noreply@puutwallet.com";
        String password = "Puu2Sup4Sec1";
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        javax.mail.Session mailsession = javax.mail.Session.getDefaultInstance(props, null);

        MimeMessage msg = new MimeMessage(mailsession);
        msg.setSubject(subject);
        msg.setContent(content, "text/plain");
        msg.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(recipient));
        msg.setFrom(new InternetAddress("accounts-noreply@puutwallet.com"));

        Transport t = mailsession.getTransport("smtps");
        t.connect(host, username, password);
        t.sendMessage(msg, msg.getAllRecipients());
        t.close();
    }
}
