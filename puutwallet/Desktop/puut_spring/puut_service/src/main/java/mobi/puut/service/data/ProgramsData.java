package mobi.puut.service.data;

import mobi.puut.service.entities.documents.Program;
import mobi.puut.service.entities.users.User;

public interface ProgramsData {

	void save(Program program);

	Program findByName(String name);

	Program findByUser(User user);

}
