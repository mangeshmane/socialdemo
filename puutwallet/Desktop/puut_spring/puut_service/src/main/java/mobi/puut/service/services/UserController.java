package mobi.puut.service.services;

import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.apache.cxf.service.invoker.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.crypto.btc.SendMoney;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.Coverimage;
import mobi.puut.service.entities.users.Cryptoinfo;
import mobi.puut.service.entities.users.Cryptostatus;
import mobi.puut.service.entities.users.GenerateWallet;
import mobi.puut.service.entities.users.Personal;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.crypto.def.WalletService;

@RestController
@RequestMapping("/data")
public class UserController {

   
	@Autowired
	private UserService userService;
	
	@Autowired
	private WalletService walletService; 
	
	@Autowired
    private ServletContext servletContext;

	
	@Autowired
	HttpServletRequest request;
	
	@Autowired 
	UserData userdata;

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}


	@Autowired UserData userData;
	
	

    //@GetMapping(path ="/getbyid",produces = MediaType.APPLICATION_JSON)
	@RequestMapping(method = RequestMethod.GET)
    public User getUser() throws IOException{    
		String userObj=SecurityContextHolder.getContext().getAuthentication().getName();
		User user1=userdata.getByUsername(userObj);
		return user1;
    }
	
	@RequestMapping(value ="/generate",method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    public Result getUser(@RequestBody GenerateWallet wallet) {    	
    	return walletService.generateAddress(wallet);
    }
	
      
	@RequestMapping(value ="/upload",method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.MULTIPART_FORM_DATA)
	public Personal updateImage(@RequestParam("file") MultipartFile file) {
		return userService.updateImage(file);		
	}
	
	
	@RequestMapping(value ="/formdata",method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	public String postData(@FormParam(value = "name") String name) {
		return "ok";
	}
	
	
	@RequestMapping(value="/cover",method=RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA)
	 Coverimage updateCoverImage(@RequestParam("file")  MultipartFile file) {
		 return userService.updateCoverImage(file);
	 }
	
	
	@RequestMapping(value="/getWalletTransactionById/{id}",method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public List getWalletTransactionById(@PathVariable long id)
	{
		return walletService.getWalletTransactionById(id);
	}
	
	
//	@POST
//	@Path("sendMoney/{walletId}")	
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	
//	
//	
//	@RequestMapping(value="sendMoney/{walletId}",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON,consumes=MediaType.APPLICATION_JSON)
//	Cryptostatus sendMoney(@PathParam("walletId") final long walletId,final SendMoney sendMoney) {
//
//		return walletService.sendMoney(walletId, sendMoney);
//	}
//	

}
