package mobi.puut.service.services.impl;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mobi.puut.service.data.DocumentData;
import mobi.puut.service.entities.documents.Availability;
import mobi.puut.service.entities.documents.Category;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.documents.Status;
import mobi.puut.service.entities.documents.Subtype;
import mobi.puut.service.entities.documents.Type;
import mobi.puut.service.entities.users.Document;
import mobi.puut.service.services.DocumentService;

@Repository("documentService")
@Transactional
public class DocumentServiceImpl implements DocumentService{
	

	@Autowired
	DocumentData documentData;

	@Override
	public List<Type> getTypes(){
		// TODO Auto-generated method stub
		return documentData.getTypes();
	}

	@Override
	public List<Subtype> getSubTypes(Long type_id) {
		// TODO Auto-generated method stub
		return documentData.getSubTypes(type_id);
	}

	@Override
	public List<Category> getCategories(Long type_id) {
		// TODO Auto-generated method stub
		return documentData.getCategories(type_id);
	}

	@Override
	public List<Status> getStatuses() {
		// TODO Auto-generated method stub
		return documentData.getStatuses();
	}

	@Override
	public List<Availability> getAvailabilities() {
		// TODO Auto-generated method stub
		return documentData.getAvailabilities();
	}

	@Override
	public List<Document> getDocument(Long type) {
		// TODO Auto-generated method stub
		return documentData.getDocument(type);
	}

	@Override
	public List<Document> getFriendBusinessCard(Long id) {
		// TODO Auto-generated method stub
		return documentData.getFriendBusinessCard(id) ;
	}

	@Override
	public Result shareDocument(Long id, Long status) {
		// TODO Auto-generated method stub
		
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		return documentData.shareDocument(id,login,status);
	}

	@Override
	public List<Document> getDocumentsByAvailability(Long type, Long availability) {
		// TODO Auto-generated method stub
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		return documentData.getDocumentsByAvailability(type,login,availability);
	}

	@Override
	public List<Document> getDocumentsByStatus(Long type, Long status) {
		// TODO Auto-generated method stub
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		return documentData.getDocumentsByStatus(type,login,status);
	}

	@Override
	public List<Document> getValidDocuments(Long type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Document> getExpiredDocuments(Long type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response getLinearCode(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response getQRCode(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document getGraphicImage(Long document_id, int width, int height) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response uploadFile(String name, String cslogan, String designation, String cname, String address,
			String phone, String mobile, String fax, String email, String town, String web, String post, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response createDocument(Document document) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response createDocument(Long id, Document document) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response addCart(Long id, String name, String number) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	
}
