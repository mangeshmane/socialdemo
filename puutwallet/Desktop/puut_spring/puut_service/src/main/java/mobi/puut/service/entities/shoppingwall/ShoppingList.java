package mobi.puut.service.entities.shoppingwall;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.users.User;

@Entity
//@XmlRootElement
@Table(name="shoppinglists")
public class ShoppingList implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="shoppinggroup")
    private ShoppingGroup shoppinggroup;
    
    @Column(name="productname")
    private String productname;
    
    @Column(name="productdescription")
    private String productdescription;
    
    @Column(name="productnumber")
    private String productnumber;
    
    @Column(name="manufacturer")
    private String manufacturer;
    
    @Column(name="shoppers")
    private Integer shoppers;
    
    @Column(name="offer")
    private BigDecimal offer;
    
    @Column(name="price")
    private BigDecimal price;
    
    @Column(name="expiration")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expiration;
    
    @Column(name="discount")
    private Integer discount;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="merchant")
    private User merchant;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="shoppingtype")
    private ShoppingType shoppingtype;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="currency")
    private Currency currency;
    
    @Column(name="image")
    private byte[] image;
      
    @Column(name="imgname")
    private String imgname;

    @Transient
    private Boolean hasuser;

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    @XmlTransient
    public Date getExpiration() {
        return new Date(expiration.getTime());
    }
    
    @XmlElement(name="expiration")
    public String getExpirationFormatted() {
        if (expiration == null) {
            return null;
        }
        else {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            return df.format(expiration);
        }
    }

    public void setExpiration(Date expiration) {
        this.expiration = new Date(expiration.getTime());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public User getMerchant() {
        return merchant;
    }

    public void setMerchant(User merchant) {
        this.merchant = merchant;
    }

    public BigDecimal getOffer() {
        return offer;
    }

    public void setOffer(BigDecimal offer) {
        this.offer = offer;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductdescription() {
        return productdescription;
    }

    public void setProductdescription(String productdescription) {
        this.productdescription = productdescription;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductnumber() {
        return productnumber;
    }

    public void setProductnumber(String productnumber) {
        this.productnumber = productnumber;
    }

    public Integer getShoppers() {
        return shoppers;
    }

    public void setShoppers(Integer shoppers) {
        this.shoppers = shoppers;
    }

    public ShoppingGroup getShoppinggroup() {
        return shoppinggroup;
    }

    public void setShoppinggroup(ShoppingGroup shoppinggroup) {
        this.shoppinggroup = shoppinggroup;
    }

    public ShoppingType getShoppingtype() {
        return shoppingtype;
    }

    public void setShoppingtype(ShoppingType shoppingtype) {
        this.shoppingtype = shoppingtype;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
        
    public String getImgname() {
        return imgname;
    }

    public void setImgname(String imgname) {
        this.imgname = imgname;
    }

    @XmlElement(name="hasuser")
    public Boolean getHasuser() {
        return hasuser;
    }

    public void setHasuser(Boolean hasuser) {
        this.hasuser = hasuser;
    }
}