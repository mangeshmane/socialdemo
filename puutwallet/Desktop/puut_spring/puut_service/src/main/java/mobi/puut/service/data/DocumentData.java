package mobi.puut.service.data;

import java.util.Date;
import java.util.List;

import mobi.puut.service.entities.cloudmoney.CloudMoney;
import mobi.puut.service.entities.documents.Availability;
import mobi.puut.service.entities.documents.Category;
import mobi.puut.service.entities.documents.Program;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.documents.Status;
import mobi.puut.service.entities.documents.Subtype;
import mobi.puut.service.entities.documents.Type;
import mobi.puut.service.entities.users.Document;
import mobi.puut.service.entities.users.User;

public interface DocumentData {
	public List<Type> getTypes();
	public List<Subtype> getSubTypes(Long type_id); 
	public List<Category>  getCategories(Long type_id);
	public List<Status> getStatuses();
	public List<Availability> getAvailabilities();
	public List<Document> getDocument(Long type);
	public List<Document> getFriendBusinessCard(Long id); 
	public Result shareDocument(Long id, String login, Long status);
	public List<Document> getDocumentsByAvailability(Long type, String login,Long availability);
	public List<Document> getDocumentsByStatus(Long type,String login, Long status);
	byte[] getPublictimg(Document document);
	Boolean checkMobile(String mobile);
	public Boolean checkTicket(String number);
	public void addLoyaltyCrad(Document document, User user);
	public CloudMoney getSales();
	public CloudMoney getVoucher(User user);
	public void updateMoney(User user, CloudMoney cm, CloudMoney sales);
	public void updateMoney(User user, CloudMoney sales);
	public Document getById(Long id);
	public boolean checkProgramAndType(Type type, User partner, Program program);
	void edit(Document document);
	Document get(Long document_id, String login, Long type_id, Date date);
	List<Document> getDocuments(String login, Long type_id, Date cdate);
	List<Document> getDocuments(String login, Long type_id, Long status_id);
	public Document get(long parseLong, String login, long type_id, long l);
}
