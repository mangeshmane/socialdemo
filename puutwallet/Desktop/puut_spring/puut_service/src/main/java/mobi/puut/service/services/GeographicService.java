package mobi.puut.service.services;

import java.util.List;
import mobi.puut.service.entities.users.Country;
import mobi.puut.service.entities.geographic.IPCheck;
import mobi.puut.service.entities.users.IPlocations;

public interface GeographicService {

    public List<Country> getCountries();

    public List<IPlocations> getCities();

    public List<IPlocations> getCitiesByCountry(char country_iso_code);

    public IPCheck getIP(String ipnumber);

    public Country getCountryByIso(String iso);
}
