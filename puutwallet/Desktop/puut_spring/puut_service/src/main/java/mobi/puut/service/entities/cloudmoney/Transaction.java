/*package mobi.puut.service.entities.cloudmoney;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import mobi.puut.service.entities.users.User;

@Entity
//@XmlRootElement
@Table(name="transactions")
public class Transaction implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="userfrom")
    private User userfrom;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="userto")
    private User userto;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="currency")
    private Currency currency;
    
    @Column(name="amount")
    private String amount;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="purpose")
    private Purpose purpose;
    
    @Column(name="remark")
    private String remark;
    
    @Column(name="transdate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date transdate;
    
    @Transient
    private String puutnumber;
    
    @Transient
    private String creditcardid;
    
    @Transient
    private String billid;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Purpose getPurpose() {
        return purpose;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @XmlTransient
    public Date getTransdate() {
        return new Date(transdate.getTime());
    }
    
    @XmlElement
    public String getTransDateFormatted() {
        if (transdate == null) {
            return null;
        }
        else {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            return df.format(transdate);
        }
    }

    public void setTransdate(Date transdate) {
        this.transdate = new Date(transdate.getTime());
    }

    @XmlTransient
    public User getUserfrom() {
        return userfrom;
    }

    public void setUserfrom(User userfrom) {
        this.userfrom = userfrom;
    }

    @XmlTransient
    public User getUserto() {
        return userto;
    }

    public void setUserto(User userto) {
        this.userto = userto;
    }
    
    @XmlElement
    public String getPuutnumber() {
        return puutnumber;
    }
    
    public void setPuutnumber(String puutnumber) {
        this.puutnumber = puutnumber;
    }

    public String getBillid() {
        return billid;
    }

    public void setBillid(String billid) {
        this.billid = billid;
    }

    public String getCreditcardid() {
        return creditcardid;
    }

    public void setCreditcardid(String creditcardid) {
        this.creditcardid = creditcardid;
    }
}*/