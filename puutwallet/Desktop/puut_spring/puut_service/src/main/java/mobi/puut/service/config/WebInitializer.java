package mobi.puut.service.config;


import javax.servlet.FilterRegistration;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRegistration.Dynamic;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

public class WebInitializer  implements WebApplicationInitializer{

	
	/*@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();  
		servletContext.addListener(new ContextLoaderListener(createWebAppContext(ctx)));
        servletContext.addListener(new RequestContextListener());
        //ctx.register(AppConfig.class);
        ctx.setServletContext(servletContext);    
        Dynamic dynamic =  servletContext.addServlet("dispatcher", new DispatcherServlet(ctx)); 
        
        dynamic.addMapping("/");  
        dynamic.setLoadOnStartup(1);
		
      
        FilterRegistration.Dynamic corsFilter = servletContext.addFilter("corsFilter", CORSFilter.class);
        corsFilter.addMappingForUrlPatterns(null, false, "/*"); 
        
	}*/
	
	

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();  
		servletContext.addListener(new ContextLoaderListener(createWebAppContext(ctx)));
        servletContext.addListener(new RequestContextListener());
        //ctx.register(AppConfig.class);
        ctx.setServletContext(servletContext);    
        DispatcherServlet dp =  new DispatcherServlet(ctx);
        dp.setThrowExceptionIfNoHandlerFound(true);
        Dynamic dynamic =  servletContext.addServlet("dispatcher", dp); 
        dynamic.addMapping("/");  
        dynamic.setLoadOnStartup(1);
		
      
        FilterRegistration.Dynamic corsFilter = servletContext.addFilter("corsFilter", CORSFilter.class);
        corsFilter.addMappingForUrlPatterns(null, false, "/*"); 
        
	}
	
	
	private void addApacheCxfServlet(ServletContext servletContext) {
		CXFServlet cxfServlet=new CXFServlet();
		ServletRegistration.Dynamic appServlet =servletContext.addServlet("CXFServlet", cxfServlet);
		appServlet.setLoadOnStartup(1);
		appServlet.addMapping("/*");
		//Set<String> mappingConflicts=appServlet.addMapping(ServiceConfig.class);	
	}
	
private WebApplicationContext createWebAppContext(AnnotationConfigWebApplicationContext appContext) {
	appContext.register(AppConfig.class,WebSecurityConfig.class);
	return appContext;
}
}

