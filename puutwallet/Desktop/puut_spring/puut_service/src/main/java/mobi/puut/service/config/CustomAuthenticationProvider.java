/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.config;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.HashSet;
import java.util.Set;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;

import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.users.Role;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.entities.users.UserRole;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;


/**
 *
 * @author Jay
 */

public class CustomAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private UserData userdata;

    @Autowired
    private HttpServletRequest request;
    
    String exceptionMessege;

    @Override
    protected void additionalAuthenticationChecks(UserDetails ud, UsernamePasswordAuthenticationToken upat) throws AuthenticationException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected UserDetails retrieveUser(String string, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

        String username = null;
        String password = null;
        String phoneNo = null;
        String pin = null;
        String phone = null;
        UserDetails uDetails=null;
        mobi.puut.service.entities.users.User user = null;

        
        try {
        	
        	/*String userObj=SecurityContextHolder.getContext().getAuthentication().getName();
        	User user1=userdata.getByUsername(userObj);*/
        	
        	
        	
        	
   
        	
        	
            if (authentication.getPrincipal() != null && !authentication.getPrincipal().toString().isEmpty()) {
                username = authentication.getPrincipal().toString();
                password = authentication.getCredentials().toString();
                user = userdata.getByUsername(username);

                if (user == null) {
                    exceptionMessege = "no user found for given user name";
                    throw new LoginException(exceptionMessege);

                } else {
                    uDetails=verifyUsingUsernamePassword(username, password, user);
                }

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return uDetails;
    }
    
    private UserDetails verifyUsingUsernamePassword(String username,String password,User user)
    {
        UserDetails ud=null;
        if(user.getName().equals(username) && user.getPassword().equals(getMD5(password)));
        {
            ud=buildUserFromUserEntity(user);
        }
        return  ud;
    }
    
    
    
    public String getMD5(String input) {
        byte[] source;
        try {
            //Get byte according by specified coding.
            source = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            source = input.getBytes();
        }
        String result = null;
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source);
            //The result should be one 128 integer
            byte temp[] = md.digest();
            char str[] = new char[16 * 2];
            int k = 0;
            for (int i = 0; i < 16; i++) {
                byte byte0 = temp[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            result = new String(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
private UserDetails buildUserFromUserEntity(mobi.puut.service.entities.users.User userEntity)
  {
      Set<GrantedAuthority> authorites=new HashSet<GrantedAuthority>();
      UserRole role=userdata.getRole(userEntity);
      GrantedAuthority grantedAuthority=new SimpleGrantedAuthority(role.getRole().getName());
      authorites.add(grantedAuthority);
      
      String username=userEntity.getName();
      String password=userEntity.getPassword();
      org.springframework.security.core.userdetails.User springUser=new org.springframework.security.core.userdetails.User(username, password, authorites);
      
      return  springUser;
  }
    

}
