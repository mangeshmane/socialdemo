package mobi.puut.service.services;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.web.multipart.MultipartFile;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.photo.Photo;
import mobi.puut.service.entities.photo.PhotoAlbum;

public interface PhotosService {
	public Photo  getPhotoInformation(Long id);
	public List<PhotoAlbum>  getAlbums();
	public List<PhotoAlbum>  getFriendAlbums(Long friend_id);
	public List<Photo>  getPhotos( Long album_id,int width,int height);
	public Result  sharePhoto(Long id,Long status);
	public Result  voteForPhoto(Long id,Long status);
	public Response  uploadFile(MultipartFile uploadInputStream,Long id);
	public Result  createAlbum(PhotoAlbum photoAlbum);
	
}
