/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.config;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *
 * @author admin1
 */
public class CORSFilter extends OncePerRequestFilter{

  
	  static final String ORIGIN = "Origin";
	     
	    @Override
	    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
	      /*
	        response.addHeader("Access-Control-Allow-Origin", "*");
	        response.addHeader("Access-Control-Allow-Credentials", "true");
	        // CORS "pre-flight" request
	            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
	           // response.addHeader("Access-Control-Allow-Headers", "Authorization");
	           // response.addHeader("Access-Control-Allow-Headers", "Content-Type");
	            response.addHeader("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, X-Requested-With, Authorization");
	            response.addHeader("Access-Control-Max-Age", "1s");
	        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {

	            

	            if (request.getMethod().equals("OPTIONS")) {
	                response.setStatus(HttpServletResponse.SC_ACCEPTED);
	                //return;

	            }
	        }else{
	            filterChain.doFilter(request, response);
	        }*/
	     String origin=request.getHeader(ORIGIN);
	      
	        if(request.getHeader(ORIGIN) == null || request.getHeader(ORIGIN).equals("null")){
	                   
	               response.setHeader("Access-Control-Allow-Origin","*");
	               response.setHeader("Access-Control-Allow-Credentials", "true");
	               response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
	        }
	        
	        if((request.getRequestURI().substring(request.getContextPath().length()).equalsIgnoreCase("/login") && request.getMethod().equals("POST")) || request.getMethod().equals("GET")||request.getMethod().equals("DELETE")||request.getMethod().equals("PUT")||request.getMethod().equals("POST")){
	            response.setHeader("Access-Control-Allow-Origin","*");
	            response.setHeader("Access-Control-Allow-Credentials", "true");
	            response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
	        }
	        
	        if(request.getMethod().equals("OPTIONS")){
	               
	               try{ 
	                     response.setHeader("Access-Control-Allow-Origin","*");
	                     response.setHeader("Access-Control-Allow-Credentials", "true");
	                     response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
	                     //response.setHeader("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, X-Requested-With, Authorization");
	                     response.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
	                     response.getWriter().print("OK");
	                     response.getWriter().flush();
	               }catch(IOException e){
	                     e.printStackTrace();
	               } 
	        }else{
	            filterChain.doFilter(request, response);
	        }       
	    
	     }
    
    
   /* @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain fc) throws ServletException, IOException {
        
        response.addHeader("Access-Control-Allow-Origin","*");
        response.addHeader("Access-Control-Allow-Credentials","true");
        
        if(request.getHeader("Access-Control-Request-Method")!=null && "OPTIONS".equals(request.getMethod()))
        {
           response.addHeader("Access-Control-Allow-Methods","GET,POST,PUT,DELETE,OPTIONS");
           response.addHeader("Access-Control-Allow-Headers","Origin,Content-Type,Accept,X-Requested-With,Authorization");
           response.addHeader("Access-Control-Max-Age","1");
           
           if(request.getMethod().equals("OPTIONS"))
           {
               response.setStatus(HttpServletResponse.SC_ACCEPTED);
               return;
           }
        }

        
        fc.doFilter(request, response);
        
    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
    
}
