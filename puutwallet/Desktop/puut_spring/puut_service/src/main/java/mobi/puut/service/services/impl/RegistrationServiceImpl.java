/*package mobi.puut.service.services.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import mobi.puut.service.EncodingUtil;
import mobi.puut.service.MailUtil;
import mobi.puut.service.PuutUtil;
import mobi.puut.service.ServicesUtil;
import mobi.puut.service.data.GeographicData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.cloudmoney.CloudMoney;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.users.AccountSetting;

import mobi.puut.service.entities.users.Accounttype;

import mobi.puut.service.entities.users.Coverimage;
import mobi.puut.service.entities.users.Dlicenses;
import mobi.puut.service.entities.users.IPlocations;
import mobi.puut.service.entities.users.Language;
import mobi.puut.service.entities.users.Notification;
import mobi.puut.service.entities.users.Passports;
import mobi.puut.service.entities.users.Personal;
import mobi.puut.service.entities.users.Preference;
import mobi.puut.service.entities.users.Result;
import mobi.puut.service.entities.users.Role;
import mobi.puut.service.entities.users.Ubill;
import mobi.puut.service.entities.users.Uploadids;
import mobi.puut.service.entities.users.User;

import mobi.puut.service.entities.users.UserRole;
import mobi.puut.service.services.RegistrationService;
import mobi.puut.service.services.crypto.impl.WalletServiceImpl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    UserData usersData;

    @Autowired
    ServicesUtil profileUtil;

    @Autowired
    EncodingUtil encodingUtil;

    @Autowired
    PuutUtil puutUtil;

    @Autowired
    MailUtil mailUtil;

    @Autowired
    GeographicData geographicData;

    //@Autowired
    //private HttpServletRequest request;
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    public mobi.puut.service.entities.users.Result register(User user)
            throws UnsupportedEncodingException, NoSuchAlgorithmException, MessagingException {
    
        Result result=new Result();
		try {
            this.init();
            IPlocations location1 = null;
//        if(request.getHeader("X-Forwarded-For") != null){
//            System.out.println("equest.getHeader(\"X-Forwarded-For\") condition");
//            String ip = IPUtil.getClientIpAddr(request);
//            //String ip = "16.77.72.18";
//            String IP= ip.replace(".", "");
//            final String countryCode = geographicData.getIP(IP).getIplocations().getCountry_iso_code();
//            location1 = geographicData.getIP(IP).getIplocations();
//            Country country1 = geographicData.getCountries().stream().filter(c->c.getIso().equals(countryCode)).findFirst().orElse(null);
//            user.setCountry(country1);
//            user.setIpaddress(ip);
//        }

            result.setResult(profileUtil.getRegistrationErrors(user));

            if (result.getSuccess()) {
                String hash_password = encodingUtil.hash(user.getPassword());
                if (!hash_password.isEmpty()) {
                    user.setPassword(hash_password);

                    String puutnumber = puutUtil.generatePuutNumber();

                    if (puutnumber.isEmpty()) {
                        System.out.println("Puut number is empty");
                        result.setSuccess(false);
                        result.setError("PUUTNUMBERNOTGENERATEDPROPERLY");
                    } else {
                        user.setPuutnumber(puutnumber);

                        User checkLogin = usersData.getByUsername(user.getUsername());

                        if (checkLogin == null) {
                            User checkPhone = null;
                            if (user.getPhoneno() == null || user.getPhoneno().isEmpty()) {
                                result.setSuccess(false);
                                System.out.println("PHONENUMBERREQUIRED");
                                result.setError("PHONENUMBERREQUIRED");
                                return result;
                            } else {
                                checkPhone = usersData.getUserByPhone(user.getPhoneno());
                            }

                            //TODO: Get IP based country from user.
                            //String ip = IPUtil.getClientIpAddr(request);
                            if (checkPhone == null) {
                                user.setEnabled(Boolean.FALSE);
                                user.setDisabled(Boolean.TRUE);
                                Language language = new Language();
                                language = usersData.getLanguageById(1L);
                                user.setLanguage(language);
                                user.setCountry(geographicData.getCountryByIso(user.getCountry_iso()));
                                // user.setCountry(country1);
                                //user.setIpaddress(ip);
                                if (location1 != null) {
                                    user.setCity(location1);
                                }
                                Accounttype accounttype = new Accounttype();
                                accounttype.setId(1L);
                                accounttype.setName("I am a Phone User");
                                user.setAccounttype(accounttype);
                                user.setStatus("0L");
                                usersData.add(user);
                                System.out.println("User added");
                                Role role = new Role();
                                role.setId(8);

                                UserRole userrole = new UserRole();
                                userrole.setUser(user);
                                userrole.setRole(role);
                                usersData.add(userrole);
                                System.out.println("Role added");
                                AccountSetting account_setting = new AccountSetting();
                                account_setting.setUser(user);
                                account_setting.setFirstname(Boolean.TRUE);
                                account_setting.setMiddlename(Boolean.FALSE);
                                account_setting.setLastname(Boolean.TRUE);
                                account_setting.setPhone(Boolean.TRUE);
                                account_setting.setBirthdate(Boolean.FALSE);
                                account_setting.setGender(Boolean.FALSE);
                                account_setting.setCountry(Boolean.FALSE);
                                account_setting.setPuutnumber(Boolean.TRUE);
                                Notification notification = new Notification();
                                notification.setId(2L);
                                account_setting.setNotification(notification);

                                account_setting.setLanguage(user.getLanguage());
                                usersData.add(account_setting);
                                System.out.println("account_setting added");
                                //New lines for preferences  16.04.2014
                                Preference preference = new Preference();
                                preference.setUser(user);
                                preference.setIsbusiness(Boolean.TRUE);
                                //preference.setIsCulture(Boolean.TRUE);
                                preference.setEducation(Boolean.TRUE);
                                preference.setEmail(Boolean.TRUE);
                                preference.setPuut(Boolean.TRUE);
                                preference.setIshealth(Boolean.TRUE);
                                preference.setHome(Boolean.TRUE);
                                preference.setSport(Boolean.TRUE);
                                preference.setNews(Boolean.TRUE);
                                preference.setSms(Boolean.TRUE);
                                preference.setShopping(Boolean.TRUE);
                                preference.setTravel(Boolean.TRUE);
                                preference.setLang(Boolean.TRUE);
                                preference.setLanguage(user.getLanguage());
                                usersData.addPreference(preference);
                                System.out.println("preference added");
                                // New lines for preferences 16.04.2014
                                // New lines for preferences 10.05.2015 underneath

                                AccountsPermissions ap = new AccountsPermissions();
                                ap.setUser(user);
                                ap.setShoppinggroups(Boolean.TRUE);
                                ap.setShoppinglist(Boolean.TRUE);
                                ap.setOfferlist(Boolean.TRUE);
                                ap.setAccoutbalance(Boolean.TRUE);
                                ap.setSendmoney(Boolean.TRUE);
                                ap.setWithdrawmoney(Boolean.TRUE);
                                ap.setPaybills(Boolean.TRUE);
                                ap.setAccounthistory(Boolean.TRUE);
                                ap.setRefillcloudmoney(Boolean.TRUE);
                                ap.setCreditcards(Boolean.TRUE);
                                ap.setLoyaltycards(Boolean.TRUE);
                                ap.setTickets(Boolean.TRUE);
                                ap.setEvent(Boolean.TRUE);
                                ap.setBoardingpass(Boolean.TRUE);
                                ap.setPhotos(Boolean.TRUE);
                                ap.setBills(Boolean.TRUE);
                                ap.setReceipts(Boolean.TRUE);
                                ap.setBusinesscards(Boolean.TRUE);
                                ap.setDocuments(Boolean.TRUE);
                                ap.setIdcards(Boolean.TRUE);
                                ap.setAccesscards(Boolean.TRUE);
                                ap.setCrypto(Boolean.TRUE);
                                ap.setOpenbanking(Boolean.TRUE);
                                Account account = new Account();
                                account.setId(2L);
                                ap.setAccount(account);
                                usersData.addAccountsPermissions(ap);
                                System.out.println("account-permission added");

                                AllowPermissions userPermission = new AllowPermissions();
                                userPermission.setUser(user);
                                List<Allow> allow = usersData.getAllow();
                                Iterator it = allow.iterator();
                                while (it.hasNext()) {
                                    userPermission.setTickets(Boolean.TRUE);
                                    userPermission.setEvent(Boolean.TRUE);
                                    userPermission.setBoardingpass(Boolean.TRUE);
                                    userPermission.setIdcards(Boolean.TRUE);
                                    userPermission.setAccesscards(Boolean.TRUE);
                                    userPermission.setCreditcards(Boolean.TRUE);
                                    userPermission.setBusinesscards(Boolean.TRUE);
                                    userPermission.setLoyaltycards(Boolean.TRUE);
                                    userPermission.setPhotos(Boolean.TRUE);
                                    userPermission.setBills(Boolean.TRUE);
                                    userPermission.setReceipts(Boolean.TRUE);
                                    userPermission.setBusinesscards(Boolean.TRUE);
                                    userPermission.setDocuments(Boolean.TRUE);
                                    userPermission.setShoppinggroups(Boolean.TRUE);
                                    userPermission.setShoppinglist(Boolean.TRUE);
                                    userPermission.setOfferlist(Boolean.TRUE);
                                    userPermission.setAccoutbalance(Boolean.TRUE);
                                    userPermission.setAccounthistory(Boolean.TRUE);
                                    userPermission.setSendmoney(Boolean.TRUE);
                                    userPermission.setWithdrawmoney(Boolean.TRUE);
                                    userPermission.setPaybills(Boolean.TRUE);
                                    userPermission.setRefillcloudmoney(Boolean.TRUE);
                                    userPermission.setCrypto(Boolean.TRUE);
                                    userPermission.setOpenbanking(Boolean.TRUE);
                                    userPermission.setAllow((Allow) it.next());
                                    usersData.addAllowPermissions(userPermission);
                                    System.out.println("allow-permission added");
                                }
                                // New lines for cloudmoney 28.04.2014                            
                                CloudMoney cloudMoney = new CloudMoney();
                                String amount = new String("0.00");
                                cloudMoney.setUser(user);
                                Currency currency = usersData.getCurrencyById(1l);                                
                                cloudMoney.setCurrency(currency);
                                cloudMoney.setAmount(amount);
                                cloudMoney.setMessage(0);
                                cloudMoney.setSent(0);
                                cloudMoney.setDocmessage(0);
                                cloudMoney.setDocsent(0);
                                cloudMoney.setReloadamount(false);
                                usersData.addCloudMoney(cloudMoney);
                                System.out.println("cloudMoney added");

                                UpdateMoney updateMoney = new UpdateMoney();
                                updateMoney.setUser(user);
                                updateMoney.setUpgrade(null);
                                usersData.addUpdateMoney(updateMoney);
                                System.out.println("updateMoney added");

                                Personal personal = new Personal();
                                personal.setUser(user);
                                personal.setChoose(Boolean.FALSE);
                                usersData.addPersonal(personal);
                                System.out.println("personal added");

                                Coverimage coverimage = new Coverimage();
                                coverimage.
                                setUser(user);
                                coverimage.setChoose(false);
                                usersData.addCoverimage(coverimage);
                                System.out.println("coverimage added");

                                Dlicenses dlicenses = new Dlicenses();
                                dlicenses.setUser(user);
                                dlicenses.setChoose(false);
                                usersData.addDlicenses(dlicenses);
                                System.out.println("dlicenses added");

                                Passports passports = new Passports();
                                int randomNumber = ThreadLocalRandom.current().nextInt();
                                String fileName = Passports.class.getSimpleName() + randomNumber;
                                passports.setUser(user);
                                passports.setChoose(false);
                                passports.setName(fileName);
                                usersData.addPassports(passports);
                                System.out.println("passports added");

                                Ubill ubill = new Ubill();
                                ubill.setUser(user);
                                ubill.setChoose(false);
                                usersData.addUbill(ubill);
                                System.out.println("ubill added");

                                Uploadids uploadids = new Uploadids();
                                uploadids.setUser(user);
                                uploadids.setChoose(false);
                                usersData.addUploadids(uploadids);
                                System.out.println("uploadids added");

                                UserInterest userInterest = new UserInterest();
                                AccountSetting test = usersData.getSettings(user);
                                List<Interest> interests = usersData.getInterests();
                                for (Interest interest : interests) {
                                    userInterest.setAccountsetting(test);
                                    userInterest.setInterest(interest);
                                    userInterest.setChecked(Boolean.FALSE);
                                    usersData.addUserInterest(userInterest);
                                }
                                System.out.println("userInterest added");
                                StringBuilder content = new StringBuilder();
                                content.append("Dear ");
                                content.append(user.getFirstname());
                                content.append("\n\n");
                                content.append("Welcome to Puut Wallet.\n\n");
                                content.append("To activate your account and verify your email ");
                                content.append("address, please click the following link:\n\n");
                                content.append("https://puutwallet.com/confirm/");
                                content.append(puutnumber);
                                content.append("/");
                                content.append(user.getId());
                                content.append("\n\n");
                                content.append("***NOTE*** You'll need your verification link if ");
                                content.append("you lose access to your account\n");
                                content.append("(for example, if you forget your username and ");
                                content.append("password)\n\n");
                                content.append("If you've received this mail in error, it's ");
                                content.append("likely that another user entered\n");
                                content.append("your email address while trying to create an ");
                                content.append("account for a different email address.\n");
                                content.append("If you don't click the verification link, the ");
                                content.append("account won't be activated.\n\n");
                                content.append("If you didn't request this email, but you decide ");
                                content.append("to use this account, or delete it,\n");
                                content.append("you'll first need to reset the account password ");
                                content.append("by entering your email address at\n\n");
                                content.append("https://www.puutwallet.com/\n\n");
                                content.append("If clicking the link above does not work, copy ");
                                content.append("and paste the URL\n");
                                content.append("in a new browser window instead.\n\n");
                                content.append("Sincerely,\n");
                                content.append("The PuutWallet Accounts Team\n\n");
                                content.append("Note: This email address cannot accept replies. ");
                                content.append("To fix an issue or\n");
                                content.append("learn more about your account, visit our help center:\n\n");
                                content.append("https://www.puutwallet.com/support/\n\n");
                                content.append("Thank you,\n");
                                content.append("The PuutWallet Accounts Team");
                                System.out.println("Before email send");
                                mailUtil.sendMail("PuutWallet Account Activation", content.toString(), user.getLogin());
                                System.out.println("After email send");
                            } else {
                                result.setSuccess(false);
                                result.setError("PHONEALREADYREGISTERED");
                            }
                        } else {
                            result.setSuccess(false);
                            result.setError("LOGINALREADYREGISTERED");
                        }
                    }
                } else {
                    result.setSuccess(false);
                    result.setError("ENCODINGERROR");
                }
            }

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletServiceImpl.class).error(exception);
            result.setSuccess(Boolean.FALSE);
            result.setError(exception);
            //throw new AccessDeniedException(exception);

        }
        return result;
    }

    public Result confirm(String puutnumber, String userid) {
        this.init();
        Result result = new Result();
        result.setSuccess(Boolean.FALSE);

        User user = usersData.getUserByPuutNumber(puutnumber);

        if (user != null) {
            user.setEnabled(Boolean.TRUE);
            user.setDisabled(Boolean.FALSE);
            usersData.edit(user);
            result.setSuccess(Boolean.TRUE);
        } else {
            result.setError("There is no such a user");
        }

        return result;
    }

}
*/