package mobi.puut.service.services.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import mobi.puut.service.data.FriendData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.Friend;
import mobi.puut.service.entities.users.Personal;
import mobi.puut.service.entities.users.Preference;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.FriendService;



@Repository("friendService")
public class FriendServiceImpl implements FriendService{

	
	@Autowired 
	FriendData friendData;
	
	@Autowired
	UserData userData;
	
	 @PostConstruct
		public void init() {
			SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		}

	 @Transactional
	@Override
	public List<Friend> getFriends() {
		// TODO Auto-generated method stub
		
		this.init();
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		User user=userData.getByUsername(login);
		List<Friend> friends=friendData.getFriends(user);	
		
		for(Friend friend : friends) {
			Personal personal=userData.getPersonal(friend.getFriend());
			friend.getFriend().setImage(personal.getImage());
		}
		return friends;
	}

	
	 
	@Override
	@Transactional
	public List<Friend> getInvitations() {
		// TODO Auto-generated method stub

		this.init();
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		User user=userData.getByUsername(login);
		List<Friend> friends=friendData.getInvitations(user);	
		
		for(Friend friend : friends) {
			Personal personal=userData.getPersonal(friend.getFriend());
			friend.getFriend().setImage(personal.getImage());
		}		
		return friends;
	}

	
	
	@Override
	public Result acceptInvitations(Long id) {
		// TODO Auto-generated method stub
		this.init();
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		User user=userData.getByUsername(login);
		
		Result result=new Result();
		result.setSuccess(true);
		
		Friend friend=friendData.get(id,user);
		
		if(friend==null) {
			result.setSuccess(false);
			result.setError("No friend found");
			
		}else {
			friend.setAccepted(true);
			friendData.edit(friend);
			
		}
		return result;
		
	}

	@Override
	public User getFriendProfile(Long id) {
		// TODO Auto-generated method stub
		this.init();
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		
		Friend friend=friendData.get(id,login,1);
		
		if(friend==null) {
			AccountSetting settings=userData.getSettings(friend.getUser());
			
			
			if(settings==null){
				settings=new AccountSetting();
				settings.setUser(friend.getFriend());
				settings.setFirstname(Boolean.TRUE);
				settings.setMiddlename(Boolean.TRUE);
				settings.setLastname(Boolean.TRUE);
				settings.setPhone(Boolean.TRUE);
				settings.setBirthdate(Boolean.TRUE);
				settings.setGender(Boolean.TRUE);
				settings.setCountry(Boolean.TRUE);
				settings.setPuutnumber(Boolean.TRUE);
				
				
				userData.add(settings);
				
			}
			
			friend.getFriend().setUsername("hidden");
			if(!settings.getFirstname()) {
				friend.getFriend().setFirstname("hidden");
			}
			if(!settings.getMiddlename()) {
				friend.getFriend().setMiddlename("hidden");
			}
			if(!settings.getLastname()) {
				friend.getFriend().setLastname("hidden");
			}
			if(!settings.getPhone()) {
				friend.getFriend().setPhoneno("hidden");
			}
			if(!settings.getBirthdate()) {
				Calendar birthdate=Calendar.getInstance();
				birthdate.set(1800,0,1);
				friend.getFriend().setBirthdate("hidden");
			}
			if(!settings.getGender()) {
				friend.getFriend().getGender().setName("hidden");
			}
			if(!settings.getCountry()) {
				friend.getFriend().getCountry().setName("hidden");
			}
			
			if(!settings.getPuutnumber()) {
				friend.getFriend().setPuutnumber("hidden");
			}
			
		}
		if(friend==null) {
			return new User();
		}else
		{
			return friend.getFriend();
		}
		
	}

	@Transactional
	@Override
	public List<User> searchFriend(User user) {
		// TODO Auto-generated method stub
		this.init();
		
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		User loginUser=userData.getByUsername(login);
		List<User> list=friendData.searchFriend(user,login);
		
		List<User> resultList=new ArrayList<User>();
		
		
		for(User u: list) {
			User result_profile=new User();
			Personal personal=userData.getPersonal(u);
			
			if(u.getId()!=null) {
				result_profile.setId(u.getId());
			}
			
			if(u.getLastname()!=null) {
				result_profile.setLastname(u.getLastname());
			}
			
			
			if(u.getFirstname()!=null) {
				result_profile.setFirstname(u.getFirstname());
			}
			
			
			if(u.getMiddlename()!=null) {
				result_profile.setMiddlename(u.getMiddlename());
			}
			
			if(u.getCity()!=null) {
				result_profile.setCity(u.getCity());
			}
			
			if(personal!=null) {
				if(personal.getImage()!=null) {
					result_profile.setPimage(personal.getImage());
				}
			}
			
			if(loginUser.getId()!=u.getId()) {
				resultList.add(result_profile);
			}
			
		}
		return resultList;
	}

	
	public Result addFriend(User user) {
		this.init();
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		
		Result result=new Result();
		result.setSuccess(true);
		
		User user_profile=userData.getByUsername(login);
		User friend_profile=userData.get(user.getId());
		Preference preuser=friendData.getPreference(user_profile);
		Preference prefriend= friendData.getPreference(friend_profile);
		
		
		Friend friend=new Friend();
		friend.setUser(user_profile);
		friend.setFriend(friend_profile);
		friend.setAccepted(false);
		friend.setPreuser(preuser);
		friend.setPrefriend(prefriend);
		friendData.add(friend);
		
		
		return result;
	}
	
	
	@Override
	public Result deleteFriend(List<User> users) {
		// TODO Auto-generated method stub
		this.init();
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		return friendData.deleteFriend(users,login);
	}
	
	

}
