/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.data.def.chat;

import java.util.List;
import mobi.puut.service.entities.chat.BlockInterval;

/**
 * The block interval DAO
 * @author Nathan Audin
 */
public interface BlockIntervalData {
    public void save(BlockInterval b);
    public void update(BlockInterval b);
    public BlockInterval find(long id);
    public List<BlockInterval> findAll();
    public List<BlockInterval> findCurrentByUsersIds(long userId1, long userId2);
    public List<BlockInterval> findAllByUsersIds(long userId1, long userId2);
    public BlockInterval findByRelation(long usertouserId);
}
