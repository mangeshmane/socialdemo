
package mobi.puut.service.entities.chat;

import mobi.puut.service.entities.users.User;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import org.codehaus.jackson.annotate.JsonIgnore;


@Entity
//@XmlRootElement
@Table(name="participant")
public class Participant implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id; 
   
    @ManyToOne
    @JoinColumn(name = "conversation")
    private Conversation conversationIdParticipants;
    
    @ManyToOne
    @JoinColumn(name = "user")
    private User userId;
    
    @Column(name = "lastreadmessage")
    private Date lastReadMessage;
    
    @Column(name = "timejoined")
    private Date timeJoined;
    
    @Column(name = "visible", columnDefinition="TINYINT(1)")
    private Boolean visible;
    
    @Column(name = "active", columnDefinition="TINYINT(1)")
    private Boolean active;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Conversation getConversation() {
        return conversationIdParticipants;
    }

    public void setConversation(Conversation conversationId) {
        this.conversationIdParticipants = conversationId;
    }
    
    public User getUser() {
        return userId;
    }

    public void setUser(User userId) {
        this.userId = userId;
    }
    
    public Date getLastReadMessage() {
        return lastReadMessage;
    }

    public void setLastReadMessage(Date lastReadMessage) {
        this.lastReadMessage = lastReadMessage;
    }
    
    public Date getTimeJoined() {
        return timeJoined;
    }

    public void setTimeJoined(Date timeJoined) {
        this.timeJoined = timeJoined;
    }
    
    public Boolean getVisible(){
        return visible;
    }
    
    public void setVisible(Boolean visible){
        this.visible = visible;
    }
    
    public Boolean getActive(){
        return active;  
    }
    
    public void setActive(Boolean active){
        this.active = active;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Participant)) {
            return false;
        }
        Participant other = (Participant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "Participant{" +
                    "id='" + id + '\'' +
                    ", conversationIdParticipants='" + conversationIdParticipants.toString() + '\'' + 
                    ", userId='" + userId.toString() + '\'' + 
                    ", lastReadMessage='" + lastReadMessage + '\'' +
                    ", timeJoined='" + timeJoined + '\'' +    
                    ", visible='" + visible + '\'' +
                    ", active='" + active + '\'' +
                "}";
    }
}
