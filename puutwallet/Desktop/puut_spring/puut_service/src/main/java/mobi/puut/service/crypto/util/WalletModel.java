package mobi.puut.service.crypto.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.utils.MonetaryFormat;
import org.bitcoinj.wallet.Wallet;
import org.bitcoinj.wallet.listeners.WalletChangeEventListener;
import org.bitcoinj.wallet.listeners.WalletCoinsReceivedEventListener;
import org.bitcoinj.wallet.listeners.WalletCoinsSentEventListener;
import org.springframework.beans.factory.annotation.Autowired;

import mobi.puut.service.data.UserData;

public class WalletModel {

	private int UserId;
	private Address address;
	private String transaction;
	private Coin balance = Coin.ZERO;
	private double syncProgress=-1.0;
	private static double SYNCHRONISATION_FINISHED=1.0;
	private ProgressBarUpdater syncProgressUpdater =new ProgressBarUpdater();
	private List<Transaction> transactions=Collections.synchronizedList(new ArrayList<>());
	//private WalletInfoData walletInfoData=new WalletInfoDataImpl();
	//private StatusData statusData=new StatusDataImpl();
	
	@Autowired
	private UserData userData;
	
	public WalletModel(){
		
	}

	public int getUserId() {
		return UserId;
	}

	public void setUserId(int userId) {
		this.UserId = userId;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	
	
	
	public WalletModel(Wallet wallet){
		setWallet(wallet);
	}
	
	public boolean isSyncFinished()	{
		return syncProgress==SYNCHRONISATION_FINISHED;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public Coin getBalance() {
		return balance;
	}
	
	public float getBalanceFloatFormat() {
		float bal=(float)balance.getValue();
		float fac=(float)Math.pow(10,8);
		float result=bal/fac;
		return result; 
	}
	
	public double getSyncProgress(){
		return syncProgress;
	}
	
	public List<Transaction> geTransactions(){
		return transactions;
	}
	
	public ProgressBarUpdater getSyncProgressUpdater() {
		return syncProgressUpdater;
	}
	
	
	private void update(Wallet wallet)	{
		this.balance=wallet.getBalance();
		this.address=wallet.currentReceiveAddress();
		transactions=Collections.synchronizedList(new ArrayList<>());
		transactions.addAll(wallet.getRecentTransactions(100, false));
		
		this.transaction=Objects.isNull(transactions)||
				transactions.isEmpty() ?"No Transaction":String.valueOf(transactions.get(0));
		
	}
	
	
	public boolean setWallet(Wallet wallet)
	{
		try {
			wallet.addChangeEventListener(new WalletChangeEventListener(){ 
				
				
				@Override
				public void onWalletChanged(Wallet wallet) {
					// TODO Auto-generated method stub
					update(wallet);
				}
			});
			
			wallet.addCoinsReceivedEventListener(new WalletCoinsReceivedEventListener() {
				
				@Override
				public void onCoinsReceived(Wallet wallet, Transaction tx, Coin prevBalance, Coin newBalance) {
					// TODO Auto-generated method stub
					
				}
			});
			
			wallet.addCoinsSentEventListener(new WalletCoinsSentEventListener() {
				
				@Override
				public void onCoinsSent(Wallet wallet, Transaction t, Coin coin, Coin coin1) {
					// TODO Auto-generated method stub
					
				}
			});
			update(wallet);
			return true;
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
		
	}
	
	private class ProgressBarUpdater extends DownloadProgressTracker{

		@Override
		protected void doneDownload() {
			// TODO Auto-generated method stub
			super.doneDownload();
			syncProgress=SYNCHRONISATION_FINISHED;
		}

		@Override
		protected void progress(double percentage, int blocksSoFar, Date date) {
			// TODO Auto-generated method stub
			super.progress(percentage, blocksSoFar, date);
			syncProgress=percentage/100.0;
		}
		
	}
	
	public String addTransactionHistory(Transaction transaction,WalletManager walletManager) {
		if(Objects.isNull(transaction)) {
			return "No Transaction";
		}
		
		Coin value=transaction.getValue(walletManager.bitcoin.wallet());
		if(value.isPositive()) {
			String message="Incoming payment of "+MonetaryFormat.BTC.format(value);
			return message;
		}else if(value.isNegative()) {
			Address address=transaction.getOutput(0).getAddressFromP2PKHScript(new WalletManager().networkParameters);
			String message ="Outbound payment to"+address+"worth of"+(MonetaryFormat.BTC.format(value).toString().replaceAll("-", ""));
			return message;
		}
		String message ="Payment with id"+transaction.getHash();
		return message;
	}

	public List<Transaction> getTransactions() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
}
