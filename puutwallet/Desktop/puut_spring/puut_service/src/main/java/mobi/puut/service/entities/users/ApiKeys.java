package mobi.puut.service.entities.users;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="apiKeys")
public class ApiKeys implements Serializable{

private static final long serialVersionUID=1L;



@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id")
private Long id;


@Column(name="apikey")
private String apikey;

@Column(name="scretkey")
private String scretkey;



@ManyToOne(cascade= {CascadeType.ALL})
@JoinColumn(name="user")
private User user;



public Long getId() {
	return id;
}



public void setId(Long id) {
	this.id = id;
}



public String getApikey() {
	return apikey;
}



public void setApikey(String apikey) {
	this.apikey = apikey;
}



public String getScretkey() {
	return scretkey;
}



public void setScretkey(String scretkey) {
	this.scretkey = scretkey;
}



public User getUser() {
	return user;
}



public void setUser(User user) {
	this.user = user;
}



public static long getSerialversionuid() {
	return serialVersionUID;
}








}
