package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mobi.puut.service.entities.documents.Availability;
import mobi.puut.service.entities.documents.Category;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.documents.Status;
import mobi.puut.service.entities.documents.Subtype;
import mobi.puut.service.entities.documents.Type;
import mobi.puut.service.entities.users.Document;
import mobi.puut.service.services.DocumentService;


@RestController
@RequestMapping("/documents")
public class DocumentsController {

	
@Autowired
private DocumentService documentService;
		
@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
@RequestMapping(value="/types",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Type>  getTypes(){
	try {
		return documentService.getTypes();
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}




@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/subtypes/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Subtype>  getSubTypes(@PathVariable("id") Long type_id){
	try {
		return documentService.getSubTypes(type_id);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}



@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/catagories/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Category>  getCategories(@PathVariable("id") Long type_id){
	try {
		return documentService.getCategories(type_id);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}


@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/statuses",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Status>  getStatuses(){
	try {
		return documentService.getStatuses();
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}



@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/availabilities",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Availability> getAvailabilities(){
	try {
		return documentService.getAvailabilities();
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}



@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/{type}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Document> getDocument(@PathVariable("type") Long type){
	try {
		return documentService.getDocument(type);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}


@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/friend/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Document> getFriendBusinessCard(@PathVariable("id") Long id){
	try {
		return documentService.getFriendBusinessCard(id);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}

@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/share/{id}/{status}",method=RequestMethod.PUT,produces=MediaType.APPLICATION_JSON)
public Result shareDocument(@PathVariable("id") Long id,@PathVariable("status") Long status){
	try {
		return documentService.shareDocument(id,status);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}



@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/avalibilities/{type}/{availability}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Document> getDocumentsByAvailability(@PathVariable("type") Long type,@PathVariable("availability") Long availability){
	try {
		return documentService.getDocumentsByAvailability(type,availability);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}


@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/statuses/{type}/{status}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Document> getDocumentsByStatus(@PathVariable("type") Long type,@PathVariable("status") Long status){
	try {
		return documentService.getDocumentsByStatus(type,status);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}

@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/valid/{type}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Document> getValidDocuments(@PathVariable("type") Long type){
	try {
		return documentService.getValidDocuments(type);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}


@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/expired/{type}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public List<Document> getExpiredDocuments(@PathVariable("type") Long type){
	try {
		return documentService.getExpiredDocuments(type);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}


@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/linear/{id}",method=RequestMethod.GET,produces=org.springframework.http.MediaType.IMAGE_PNG_VALUE)
public Response getLinearCode(@PathVariable("id") Long id){
	try {
		return documentService.getLinearCode(id);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}


@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/qr/{id}",method=RequestMethod.GET,produces=org.springframework.http.MediaType.IMAGE_PNG_VALUE)
public Response getQRCode(@PathVariable("id") Long id){
	try {
		return documentService.getQRCode(id);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}

@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/{id}/{width}/{height}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
public Document getGraphicImage(@PathVariable("id")  Long document_id,@PathVariable("width")  int width,@PathVariable("height") int height) {
	try {
		return documentService.getGraphicImage(document_id,width,height);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}


@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')or hasAuthority('puutmerchant') or hasAuthority('puutpartner')")
@RequestMapping(value="/bs/upload",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON,consumes=MediaType.MULTIPART_FORM_DATA)
public Response uploadFile(
@FormParam(value="name") String name,
@FormParam(value="cslogan") String cslogan,
@FormParam(value="designation") String designation,
@FormParam(value="cname") String cname,
@FormParam(value="address") String address,
@FormParam(value="phone") String phone,
@FormParam(value="mobile") String mobile,
@FormParam(value="fax") String fax,
@FormParam(value="email") String email,
@FormParam(value="town") String town,
@FormParam(value="web") String web,
@FormParam(value="post") String post,
@FormParam(value="login") String login
		) {
	try {
		return documentService.	uploadFile(name,cslogan,designation,cname,address,phone,mobile,fax,email,
				 town,
				 web,
				 post,
				 login
				);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}

@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/create",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON,consumes=MediaType.APPLICATION_JSON)
public Response createDocument(@RequestBody Document document){
	try {
		return documentService.createDocument(document);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}


@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/create/{id}",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON,consumes=MediaType.APPLICATION_JSON)
public Response createDocument(@PathVariable("id") Long id,@RequestBody Document document){
	try {
		return documentService.createDocument(id,document);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}


@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclent')")
@RequestMapping(value="/add/{id}/{name}/{number}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON,consumes=MediaType.APPLICATION_JSON)
public Response addCart(@PathVariable("id") Long id,@PathVariable("name") String name,@PathVariable("number") String number){
	try {
		return documentService.addCart(id,name,number);
	}catch(Exception e) {
	StringWriter sw=new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	String exception=sw.toString();
	Logger.getLogger(DocumentsController.class).error(exception);
	throw new Error(exception);
	}
}




}
