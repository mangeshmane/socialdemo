package mobi.puut.service.data.def.chat;

import java.util.List;

import mobi.puut.service.entities.chat.UserToUser;

public interface UserToUserData {

	public void save(UserToUser userToUser);
	public void update(UserToUser userToUser);
	public UserToUser find(long id);
	public List<UserToUser> findAll();
	public List<UserToUser> findByUsersIds(long userId1, long userId2);
   	
}
