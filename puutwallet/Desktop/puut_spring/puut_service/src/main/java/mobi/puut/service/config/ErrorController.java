package mobi.puut.service.config;

import javax.ws.rs.core.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import mobi.puut.service.entities.users.Result;

@RestController
@RequestMapping("/error")
public class ErrorController {

		
	    @RequestMapping(value = "/404", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	    public Result display() {
			Result result=new Result();
			result.setError("Not found");
			result.setSuccess(Boolean.FALSE);
	    	return result;
			
	    }
	    
	    @RequestMapping(value = "/404", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	    public Result displayE() {
			Result result=new Result();
			result.setError("Not found");
			result.setSuccess(Boolean.FALSE);
	    	return result;
			
	    }
	    
	    
	    @RequestMapping(value = "/404", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	    public Result displayEr() {
			Result result=new Result();
			result.setError("Not found");
			result.setSuccess(Boolean.FALSE);
	    	return result;
			
	    }
	
}
