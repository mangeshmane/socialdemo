package mobi.puut.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;

import mobi.puut.service.data.ShoppingWallData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.shoppingwall.BankWithdrawal;
import mobi.puut.service.entities.shoppingwall.Message;
import mobi.puut.service.entities.shoppingwall.ShoppingGroup;
import mobi.puut.service.entities.shoppingwall.ShoppingGroupUser;
import mobi.puut.service.entities.shoppingwall.ShoppingList;
import mobi.puut.service.entities.shoppingwall.Transaction;
import mobi.puut.service.entities.users.Gender;
import mobi.puut.service.entities.users.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Service("servicesUtil")
public class ServicesUtil {
    
    @Autowired
    PatternUtil patternUtil;
    
    @Autowired
    private UserData usersData;
    
    @Autowired
    @Lazy
    private ShoppingWallData shoppingWallData;
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
    
    public List<String> getEditProfileErrors(User user) {
        this.init();
        
        List<String> errors = new ArrayList<String>();

        if (user.getCity() == null) {
            errors.add("CITYMISSING");
        } else
        if (user.getUsername() != null) {
            if (!user.getUsername().isEmpty()) {
                if (patternUtil.validate(user.getUsername(), "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                    errors.add("LOGINNOTEMAILFORMAT");
                }
            }
        } else if (user.getPhoneno() == null) {
            errors.add("PHONEMISSING");
        } else if (user.getPhoneno().isEmpty()) {
            errors.add("PHONEMISSING");
        }

        return errors;
    }
    
    public List<String> getRegistrationErrors(User user) {

        PatternUtil pattern = new PatternUtil();
        List<String> errors = new ArrayList<String>();
        
        if (user.getUsername() == null) {
            errors.add("LOGINMISSING");
        } else {
            if (user.getUsername().isEmpty()) {
                errors.add("LOGINMISSING");
            } else {
                if (pattern.validate(user.getUsername(), "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                    errors.add("LOGINNOTEMAILFORMAT");
                }
            }
        }
        
        if (user.getPassword() == null) {
            errors.add("PASSWORDMISSING");
        } else {
            if (user.getPassword().isEmpty()) {
                errors.add("PASSWORDMISSING");
            } else {
                if (user.getPassword().length() < 8) {
                    errors.add("PASSWORDLESSTHAN8");
                }
            }
        }
        
//        if (user.getGender() == null) {
//            Gender gender = new Gender();
//            gender.setId(1L);
//            user.setGender(gender);
//        }
        
        if (user.getFirstname() == null) {
            errors.add("FIRSTNAMEMISSING");
        } else {
            if (user.getFirstname().isEmpty()) {
                errors.add("FIRSTNAMEMISSING");
            }
        }
        
        if (user.getMiddlename() == null) {
            user.setMiddlename("");
        }
        
        if (user.getLastname() == null) {
            errors.add("LASTNAMEMISSING");
        } else {
            if (user.getLastname().isEmpty()) {
                errors.add("LASTNAMEMISSING");
            }
        }
        
//        if (user.getBirthdate() == null) {
//            user.setBirthdate("");
//        }
        
        return errors;
    }
    
    public List<String> getMessagesErrors(Message message) {
        
        List<String> errors = new ArrayList<String>();

        if (message.getTitle() == null) {
            errors.add("TITLEMISSING");
        } else {
            if (message.getTitle().isEmpty()) {
                errors.add("TITLEMISSING");
            }
        }
        
        if (message.getBody() == null) {
            errors.add("BODYMISSING");
        } else {
            if (message.getBody().isEmpty()) {
                errors.add("BODYMISSING");
            } else {
                if (message.getBody().length() > 500) {
                    errors.add("MESSAGETOOLONG");
                }
            }
        }

        return errors;
    }
    
    public List<String> getSendMoneyErrors(Transaction transaction) {

        List<String> errors = new ArrayList<String>();
        
        if (transaction.getPuutnumber() == null) {
            errors.add("RECEIVERMISSING");
        } else {
            if (transaction.getPuutnumber().isEmpty()) {
                errors.add("RECEIVERMISSING");
            }
        }
        
        if (transaction.getCurrency() == null) {
            errors.add("CURRENCYMISSING");
        }
        
        if (transaction.getAmount() == null) {
            errors.add("AMOUNTMISSING");
        } else {
            try {
                String amount = transaction.getAmount().replace(',', '.');
                Double amount_double = Double.parseDouble(amount);
                if (amount_double == 0) {
                    errors.add("AMOUNTCANTBENIL");
                } else {
                    DecimalFormat df = new DecimalFormat("###.##");
                    transaction.setAmount(df.format(amount_double).replace(',', '.'));
                }
            } catch (Exception e) {
                errors.add("WRONGAMOUNTFORMAT");
                //Logger.getLogger(CloudMoneyService.class).error(e.getMessage());
            }
        }
        
        if (transaction.getPurpose() == null) {
            errors.add("PURPOSEMISSING");
        }
        
        if (transaction.getRemark() == null) {
            transaction.setRemark("");
        }

        return errors;
    }
    
    public List<String> getWithdrawalErrors(BankWithdrawal bankWithdrawal) {
        List<String> errors = new ArrayList<String>();
        
        if (bankWithdrawal.getSwift() == null) {
            errors.add("SWIFTMISSING");
        } else {
            if (bankWithdrawal.getSwift().isEmpty()) {
                errors.add("SWIFTMISSING");
            }
        }
        
        if (bankWithdrawal.getBeneficiary() == null) {
            errors.add("BENEFICIARYMISSING");
        } else {
            if (bankWithdrawal.getBeneficiary().isEmpty()) {
                errors.add("BENEFICIARYMISSING");
            }
        }
        
        if (bankWithdrawal.getBeneficiary_address() == null) {
            errors.add("BENEFICIARYADDRESSMISSING");
        } else {
            if (bankWithdrawal.getBeneficiary_address().isEmpty()) {
                errors.add("BENEFICIARYADDRESSMISSING");
            }
        }
        
        if (bankWithdrawal.getIban_number() == null) {
            errors.add("IBANMISSING");
        } else {
            if (bankWithdrawal.getIban_number().isEmpty()) {
                errors.add("IBANMISSING");
            }
        }
        
        if (bankWithdrawal.getBic_code() == null) {
            errors.add("BICMISSING");
        } else {
            if (bankWithdrawal.getBic_code().isEmpty()) {
                errors.add("BICMISSING");
            }
        }
        
        if (bankWithdrawal.getAccount() == null) {
            errors.add("ACCOUNTMISSING");
        } else {
            if (bankWithdrawal.getAccount().isEmpty()) {
                errors.add("ACCOUNTMISSING");
            }
        }
        
        if (bankWithdrawal.getTransfer_type() == null) {
            errors.add("TRANSFERTYPEMISSING");
        } else {
            if (bankWithdrawal.getTransfer_type().isEmpty()) {
                errors.add("TRANSFERTYPEMISSING");
            }
        }
        
        if (bankWithdrawal.getAmount() == null) {
            errors.add("AMOUNTMISSING");
        } else {
            if (bankWithdrawal.getAmount().compareTo(BigDecimal.ZERO) == 0) {
                errors.add("AMOUNTMISSING");
            }
        }
        
        if (bankWithdrawal.getCurrency() == null) {
            errors.add("CURRENCYMISSING");
        }
        
        if (bankWithdrawal.getPurpose() == null) {
            errors.add("PURPOSEMISSING");
        }
        
        return errors;
    }
    
    public List<String> getShoppingGroupErrors(ShoppingGroup shoppinggroup) {
        List<String> errors = new ArrayList<String>();
        
        if (shoppinggroup.getName() == null) {
            errors.add("NAMEMISSING");
        } else {
            if (shoppinggroup.getName().isEmpty()) {
                errors.add("NAMEMISSING");
            }
        }
        
        if (shoppinggroup.getDescription() == null) {
            errors.add("DESCRIPTIONEMPTY");
        } else {
            if (shoppinggroup.getName().isEmpty()) {
                errors.add("DESCRIPTIONEMPTY");
            }
        }
        
        if (shoppinggroup.getPreferences() == null) {
            errors.add("PREFERENCESMISSING");
        } else {
            if (shoppinggroup.getPreferences().isEmpty()) {
                errors.add("PREFERENCESMISSING");
            }
        }
        
    //    if (shoppinggroup.getCity() == null) {
    //        errors.add("CITYMISSING");
    //    }

        return errors;
    }
    
    public List<String> getShoppingListErrors(ShoppingList shoppinglist) {

        List<String> errors = new ArrayList<String>();
        
        if (shoppinglist.getShoppinggroup() == null) {
            errors.add("SHOPPINGGROUPMISSING");
        }
        
        if (shoppinglist.getProductname() == null) {
            errors.add("PRODUCTNAMEMISSING");
        } else {
            if (shoppinglist.getProductname().isEmpty()) {
                errors.add("PRODUCTNAMEMISSING");
            }
        }
        
        if (shoppinglist.getProductdescription() == null) {
            errors.add("PRODUCTDESCRIPTIONMISSING");
        } else {
            if (shoppinglist.getProductdescription().isEmpty()) {
                errors.add("PRODUCTDESCRIPTIONMISSING");
            }
        }
        
        if (shoppinglist.getProductnumber() == null) {
            errors.add("PRODUCTNUMBERMISSING");
        } else {
            if (shoppinglist.getProductnumber().isEmpty()) {
                errors.add("PRODUCTNUMBERMISSING");
            }
        }
        
        if (shoppinglist.getManufacturer() == null) {
            errors.add("MANUFACTURERMISSING");
        } else {
            if (shoppinglist.getManufacturer().isEmpty()) {
                errors.add("MANUFACTURERMISSING");
            }
        }
            
         if (shoppinglist.getDiscount() == null) {
            errors.add("DISCOUNTMISSING");
        } else {
            if (shoppinglist.getDiscount()== 0) {
                errors.add("DISCOUNTMISSING");
            }
         }
        
        if (shoppinglist.getPrice() == null) {
            errors.add("PRICEFIELDMISSING");
        } else {
            if (shoppinglist.getPrice().compareTo(BigDecimal.ZERO) == 0) {
                errors.add("PRICEFIELDMISSING");
            }
        }
        if (shoppinglist.getCurrency() == null) {
            errors.add("CURRENCYFIELDMISSING");
        }
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = usersData.getByUsername(login);
        
        List<ShoppingGroupUser> groupUser = shoppingWallData.getShoppingGroupUsers(shoppinglist.getShoppinggroup());
        
        Boolean isMember = false;
        for (ShoppingGroupUser shoppingGroupUser : groupUser) {
            if(shoppingGroupUser.getUser().getId() == user.getId() && shoppingGroupUser.isApproved()){
                isMember=true;
            }
        }
        
        if(!isMember){
            errors.add("User is not in group");
        }
        
        return errors;
    }
    
    public boolean dateValid(String d) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date dx = sdf.parse(d);
            return d.equals(sdf.format(dx));
        } catch (ParseException e) {
            return false;
        }
    }

    public Date getCurrentDate() {
        return new Date();
    }

    public String getTime() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String getCurrentDateTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
    
    public String formatTransactionDateTime(String date) throws ParseException{
        Date formattedDate = null;
        String finalDate = null;
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy",Locale.ENGLISH);
        formattedDate = dateFormat.parse(date);
        DateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        finalDate = newDateFormat.format(formattedDate);
        return finalDate;
    }
    
    
    public Date formatTransactionDateTime1(String date) throws ParseException{
        Date formattedDate = null;
        Date finalDate = null;
        
        Date firstDate3 = new Date(2018,05,01,00,00,00);
       
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy",Locale.ENGLISH);
        formattedDate = firstDate3;
        DateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        finalDate = formattedDate;
        return finalDate;
    }
    
    
    
    
    public Date getFormattedCurrentDateTime() throws ParseException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String tempDate = dateFormat.format(date);
        return dateFormat.parse(tempDate);
    }
    
    
    
    
    
    
}