package mobi.puut.service.entities.users;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name="friends")
public class Friend implements Serializable{

private static final long serialVersionUID=1L;


@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id")
private Long id;


@ManyToOne(cascade= {CascadeType.REFRESH},fetch=FetchType.EAGER)
@JoinColumn(name="user") 
private User user;

@ManyToOne(cascade= {CascadeType.REFRESH},fetch=FetchType.EAGER)
@JoinColumn(name="friend") 
private User friend;

@ManyToOne(targetEntity=Preference.class,fetch=FetchType.EAGER)
@JoinColumn(name="prefriend") 
private Preference prefriend;

@ManyToOne(targetEntity=Preference.class,fetch=FetchType.EAGER)
@JoinColumn(name="preuser") 
private Preference preuser;


@Column(name="accepted")
private Boolean accepted;


public Long getId() {
	return id;
}


public void setId(Long id) {
	this.id = id;
}


public User getUser() {
	return user;
}


public void setUser(User user) {
	this.user = user;
}

@XmlTransient
public User getFriend() {
	return friend;
}


public void setFriend(User friend) {
	this.friend = friend;
}


public Preference getPrefriend() {
	return prefriend;
}


public void setPrefriend(Preference prefriend) {
	this.prefriend = prefriend;
}


public Preference getPreuser() {
	return preuser;
}


public void setPreuser(Preference preuser) {
	this.preuser = preuser;
}


public Boolean getAccepted() {
	return accepted;
}


public void setAccepted(Boolean accepted) {
	this.accepted = accepted;
}







}
