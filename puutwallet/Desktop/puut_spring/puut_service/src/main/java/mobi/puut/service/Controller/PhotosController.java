package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.documents.Type;
import mobi.puut.service.entities.photo.Photo;
import mobi.puut.service.entities.photo.PhotoAlbum;
import mobi.puut.service.services.DocumentService;
import mobi.puut.service.services.PhotosService;

@RestController
@RequestMapping("/photos")
public class PhotosController {

	@Autowired
	private PhotosService photosService;
			
	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/info/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
	public Photo  getPhotoInformation(@PathVariable("id") Long id){
		try {
			return photosService.getPhotoInformation(id);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	
	
	
	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/albums",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
	public List<PhotoAlbum>  getAlbums(){
		try {
			return photosService.getAlbums();
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	
	
	
	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/albums/{friend_id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
	public List<PhotoAlbum>  getFriendAlbums(@PathVariable("friend_id") Long friend_id){
		try {
			return photosService.getFriendAlbums(friend_id);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	

	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/{id}/{width}/{height}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
	public List<Photo>  getPhotos(@PathVariable("id") Long album_id,@PathVariable("width") int width,@PathVariable("height") int height){
		try {
			return photosService.getPhotos(album_id,width,height);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	
	
	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/share/{id}/{status}",method=RequestMethod.PUT,produces=MediaType.APPLICATION_JSON)
	public Result  sharePhoto(@PathVariable("id") Long id,@PathVariable("status") Long status){
		try {
			return photosService.sharePhoto(id,status);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	
	
	
	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/vote/{id}/{status}",method=RequestMethod.PUT,produces=MediaType.APPLICATION_JSON)
	public Result  voteForPhoto(@PathVariable("id") Long id,@PathVariable("status") Long status){
		try {
			return photosService.voteForPhoto(id,status);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	
	
	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/upload/{album_id}",method=RequestMethod.POST,consumes=MediaType.MULTIPART_FORM_DATA)
	public Response  uploadFile(@RequestParam("file") MultipartFile file,@PathVariable("album_id") Long album_id){
		try {
			return photosService.uploadFile(file,album_id);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	
	
	@Transactional
	@PreAuthorize("hasAuthority('puutpremium')or hasAuthority('puutclient')")
	@RequestMapping(value="/albums/create",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON,consumes=MediaType.APPLICATION_JSON)
	public Result  createAlbum(@RequestBody PhotoAlbum photoAlbum){
		try {
			return photosService.createAlbum(photoAlbum);
		}catch(Exception e) {
		StringWriter sw=new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exception=sw.toString();
		Logger.getLogger(DocumentsController.class).error(exception);
		throw new Error(exception);
		}
	}
	
	
	
	
	
	
	
}
