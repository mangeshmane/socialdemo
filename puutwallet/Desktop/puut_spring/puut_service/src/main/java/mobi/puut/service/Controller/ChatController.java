/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import mobi.puut.service.entities.chat.Conversation;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.chat.chatService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Development
 */
@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private chatService chatservice;

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/deleteConv/{convId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public Boolean delete(@PathVariable(value = "convId") String convId) {
        try {
            return chatservice.delete(convId);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ChatController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/chatCreation", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<User> chatCreation() {
        try {
            return chatservice.chatCreation();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ChatController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/validateChat/{name}/{friendIds}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    public Map submitUsers(@PathVariable(value = "name") String name, @PathVariable(value = "friendIds") String[] friendIds) {
        try {
            return chatservice.submitUsers(name, friendIds);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ChatController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/conversationList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Conversation> display() {
        try {
            return chatservice.display();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ChatController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/getConvList/{term}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Conversation> getConvList(@PathVariable("term") String term) {
        try {
            return chatservice.getConvList(term);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ChatController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
    public Conversation updateConverSation(@RequestBody Conversation conversation) {
        try {
            return chatservice.updateConverSation(conversation);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ChatController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/getUserQuery/{term}/{conv}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List getUserQuery(@PathVariable("term") String term, @PathVariable("conv") String conv) {
        try {
            return chatservice.getUserQuery(term, conv);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ChatController.class).error(exception);
            throw new Error(exception);
        }
    }
}
