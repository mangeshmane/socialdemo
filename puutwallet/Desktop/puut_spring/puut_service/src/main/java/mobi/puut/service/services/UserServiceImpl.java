package mobi.puut.service.services;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.users.Coverimage;
import mobi.puut.service.entities.users.Personal;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.entities.users.UserRole;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserData userdata;
	
    public UserServiceImpl() {
    }

//    @Override
//    public List<User> getUsers() {
//    	
//        return userdata.getAllUsers();
//    }

    @Override
    public User getUser(Long id) {
        return userdata.getById(id);
    }

	@Override
	public Personal updateImage(MultipartFile file) {
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userdata.getByUsername(login);
		Personal personal = new Personal();
		personal.setUser(user);
		try {
			personal.setImage(file.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 userdata.updateImage(personal);
		 return personal;
	}

	@Override
	public Coverimage updateCoverImage(MultipartFile file) {
		// TODO Auto-generated method stub
		String login=SecurityContextHolder.getContext().getAuthentication().getName();
		User user=userdata.getByUsername(login);
		
		Coverimage coverimage=new Coverimage();
		coverimage.setUser(user);
		
		try {
			coverimage.setImage(file.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		userdata.updateCoverImage(coverimage);
		
		
		return coverimage;
		
	}

//    @Override
//    public Response add(User user) {
//        
//    	userdata.addUser(user);
//        return Response.status(Response.Status.OK).build();
//    }
//
//    @Override
//    public User getByUsername(String username) {
//        
//       return userdata.getByUsername(username);
//    
//    }
//			
//    @Override
//    public Response update(Long id, User user) {
//       
//        User user1=userdata.getById(id);
//        user1.setName(user.getName());
//        user1.setPassword(user.getPassword());
//        
//        userdata.updateById(user1);
//        return Response.status(Response.Status.OK).build();
//    }
//
//    @Override
//    public UserRole getRole(User user) {
//        
//        return userdata.getRole(user);
//    }
//
//	@Override
//	public Response register(HttpServletRequest request, User user) {
//		// TODO Auto-generated method stub
//		System.out.println("Request ::::::::::"+request);
//		userdata.addUser(user);
//        return Response.status(Response.Status.OK).build();
//	}
//
//	@Override
//	public String test(HttpServletRequest request) {
//		return "ok";
//	}
  
}