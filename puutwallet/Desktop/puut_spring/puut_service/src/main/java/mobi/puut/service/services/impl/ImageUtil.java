package mobi.puut.service.services.impl;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import org.springframework.stereotype.Service;

@Service
public class ImageUtil {

	
	
	

	public static BufferedImage resizeImage(BufferedImage img, int type, int width, int height) {
		// TODO Auto-generated method stub
		
		   Image tmp = img.getScaledInstance(width, height, img.SCALE_SMOOTH);
	        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	        Graphics2D g2d = resized.createGraphics();
	        g2d.drawImage(img, 0, 0, width, height, null);
	        g2d.dispose();
	        return resized;
	}


}