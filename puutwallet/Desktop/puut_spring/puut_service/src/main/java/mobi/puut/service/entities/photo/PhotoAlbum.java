package mobi.puut.service.entities.photo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mobi.puut.service.entities.users.User;

@Entity
@Table(name="photoalbums")
public class PhotoAlbum implements Serializable{

private static final long serialVersionUID=1L;



@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id")
private Long id;


@Column(name="title")
private String title;

@Column(name="description")
private String description;

@Column(name="cover")
private byte[] cover;



@ManyToOne(cascade= {CascadeType.ALL})
@JoinColumn(name="user")
private User user;

@Transient
private byte[] picture;


public Long getId() {
	return id;
}


public void setId(Long id) {
	this.id = id;
}


public String getTitle() {
	return title;
}


public void setTitle(String title) {
	this.title = title;
}


public String getDescription() {
	return description;
}


public void setDescription(String description) {
	this.description = description;
}


public byte[] getCover() {
	return cover;
}


public void setCover(byte[] cover) {
	this.cover = cover;
}


public User getUser() {
	return user;
}


public void setUser(User user) {
	this.user = user;
}


public byte[] getPicture() {
	return picture;
}


public void setPicture(byte[] picture) {
	this.picture = picture;
}




	
}
