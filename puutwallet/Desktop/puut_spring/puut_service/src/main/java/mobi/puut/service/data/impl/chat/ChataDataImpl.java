
package mobi.puut.service.data.impl.chat;

import java.util.List;
import javax.annotation.PostConstruct;

import mobi.puut.service.data.def.chat.ChatData;
import mobi.puut.service.entities.chat.Chats;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Repository
public class ChataDataImpl implements ChatData {
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
    
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional(rollbackFor=Exception.class)
    public void save(Chats m) {
        sessionFactory.getCurrentSession().persist(m);
    }

    @Transactional(rollbackFor=Exception.class)
    public void update(Chats m) {
        sessionFactory.getCurrentSession().update(m);
    }


    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public Chats find(long id) {
        return (Chats) sessionFactory.getCurrentSession().get(Chats.class, id);
    }

    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Chats> findAll() {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT c FROM Chats c")
                .list();
    }   

    /**
     * Find a list of chats for a specific conversation
     * @param conversationId The conversation ID
     * @return A list of chats
     */
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Chats> findByConversationId(long conversationId) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT c FROM Chats c WHERE c.conversationIdMessages.id = :conversationId").setParameter("conversationId", conversationId)
                .list();
    }

    /**
     * Find the most recent chats for each conversation where a specific user is participating
     * @param currentUser The specific user username
     * @return A list of chats
     */
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Chats> findMostRecent(String currentUser) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT c FROM Chats c"
                + " ORDER BY c.messageDate DESC")
                .list();
    }

    /**
     * Find all the unread chats for a specific user
     * @param userId The specific user ID
     * @return A list of chats
     */
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Chats> findUnreadMessages(long userId) { 
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT ch " +
        "FROM Chats ch, User u, Conversation c, Participant p " +
        "WHERE ch.conversationIdMessages = c.id " +
        "AND p.conversationIdParticipants = c.id " +
        "AND p.userId = u.id " +
        "AND u.id = :userId " +
        "AND ch.messageDate > p.lastReadMessage").setParameter("userId", userId)
                .list(); 
    }
    
}
