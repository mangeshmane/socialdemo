package mobi.puut.service.config;

import java.util.List;

public class WebsocketMessage {

	
	private String name;
	private List<String> params;

	
	public WebsocketMessage() {
	}

	public WebsocketMessage(String name, List<String> params) {
		super();
		this.name = name;
		this.params = params;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getParams() {
		return params;
	}

	public void setParams(List<String> params) {
		this.params = params;
	}
	
	
	
	
}
