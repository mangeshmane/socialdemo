package mobi.puut.service.services;

import java.util.List;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.Message;

public interface MessagesService {

	List<Message> getInbox(Long id);

	List<Message> getSent(Long id);

	Result sendMessage(Message message);

	List getUnreadMessages();

	String getUnreadMessagesCount();

}
