
package mobi.puut.service.entities.chat;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
//@XmlRootElement
@Table(name="blockinterval")
public class BlockInterval implements Serializable{
     
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "relation")
    private UserToUser usertouserId;

    @Column(name = "timeblocked")
    private Date timeBlocked;
    
    @Column(name = "timeunblocked")
    private Date timeUnblocked;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public UserToUser getUsertouserId() {
        return usertouserId;
    }

    public void setUsertouserId(UserToUser usertouserId) {
        this.usertouserId = usertouserId;
    }
    
    public Date getTimeBlocked() {
        return timeBlocked;
    }

    public void setTimeBlocked(Date timeBlocked) {
        this.timeBlocked = timeBlocked;
    }

    public Date getTimeUnblocked() {
        return timeUnblocked;
    }

    public void setTimeUnblocked(Date timeUnblocked) {
        this.timeUnblocked = timeUnblocked;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BlockInterval)) {
            return false;
        }
        BlockInterval other = (BlockInterval) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "BlockInterval{" +
                    "id='" + id + '\'' +
                    ", usertouser='" + usertouserId.toString() + '\'' + 
                    ", timeblocked='" + timeBlocked + '\'' + 
                    ", timeunblocked='" + timeUnblocked + '\'' + 
                "}";
    }
}
