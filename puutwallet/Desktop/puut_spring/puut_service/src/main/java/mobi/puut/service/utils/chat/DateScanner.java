
package mobi.puut.service.utils.chat;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.stereotype.Component;


@Component
public class DateScanner {


    public String formatDate(DateEnum e, long time) {
        String result;
        Date d = new Date(time);
        SimpleDateFormat timeFormat;
        
        
      /*  switch (e) {
            case Time:          timeFormat = new SimpleDateFormat("HH.mm");
                                result = timeFormat.format(d);
                                break;
            case DateTime:      timeFormat = new SimpleDateFormat("EEEE HH.mm");
                                result = timeFormat.format(d);
                                break;
            case DayMonthTime:  timeFormat = new SimpleDateFormat("EEEE dd MMM HH.mm");
                                result = timeFormat.format(d);
                                break;
            case Full:          timeFormat = new SimpleDateFormat("dd MMM yyyy HH.mm");
                                result = timeFormat.format(d);
                                break;
            default:            result = "";
                                break;
        }
        */
        
        switch (e.toString()) {
        case "Time":           timeFormat = new SimpleDateFormat("HH.mm");
                               result = timeFormat.format(d);
                               break;
        case "DateTime":       timeFormat = new SimpleDateFormat("EEEE HH.mm");
                               result = timeFormat.format(d);
                               break;
        case "DayMonthTime":   timeFormat = new SimpleDateFormat("EEEE dd MMM HH.mm");
                               result = timeFormat.format(d);
                               break;
        case "Full":           timeFormat = new SimpleDateFormat("dd MMM yyyy HH.mm");
                               result = timeFormat.format(d);
                               break;
        default:               result = "";
                               break;
    }
        
        
        return result;
    }

    /**
     * Displays the date by comparing the last message date and new message date
     * @param newDate The date of the new message
     * @param oldDate The date of the last message
     * @return The date as a String
     */
    public String compareDates(long newDate, long oldDate) {
        long time = newDate-oldDate;
        if (time > 0L && time < 7200000L){
            return null;
        }
        else if (time < 86400000L){
            return formatDate(DateEnum.Time, newDate);
        }
        else if (time < 604800000L){
            return formatDate(DateEnum.DateTime, newDate);
        }
        else if (time < 31557600000L){
            return formatDate(DateEnum.DayMonthTime, newDate);
        }
        else{
            return formatDate(DateEnum.Full, newDate);
        }
    }

    /**
     * Displays the date comparing the current date and the message date
     * @param newDate The current date
     * @param oldDate The message date
     * @return The date as a String
     */
    public String compareDatesFirstMessage(long newDate, long oldDate) {
        long time = newDate - oldDate;
        if (time > 0L && time < 86400000L){
            return formatDate(DateEnum.Time, oldDate);
        }
        else if (time < 604800000L){
            return formatDate(DateEnum.DateTime, oldDate);
        }
        else if (time < 31557600000L){
            return formatDate(DateEnum.DayMonthTime, oldDate);
        }
        else{
            return formatDate(DateEnum.Full, oldDate);
        }
    }
    
}
