package mobi.puut.service.crypto.util;

import org.bitcoinj.wallet.Wallet;

public interface WalletSetupCompletedListener {

	void onSetupCompleted(Wallet wallet);
}
