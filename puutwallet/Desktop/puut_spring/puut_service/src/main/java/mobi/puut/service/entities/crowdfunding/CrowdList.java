package mobi.puut.service.entities.crowdfunding;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//import mobi.puut.constraints.StringDate;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.entities.cloudmoney.Currency;

@Entity
@Table(name="crowds")
public class CrowdList implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="projecttitle")
    private String projecttitle;
    
    @Column(name="projeclocation")
    private String projeclocation;
    
    @Column(name="promotername")
    private String promotername;
    
    @Column(name="email")
    private String email;
    
    @Column(name="phone")
    private String phone;
    
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="phase")
    private Phase phase;
    
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="area")
    private Area area;
    
    
    @Column(name="elevatorpitch")
    private String elevatorpitch;
    
    @Column(name="idealinvestor")
    private String idealinvestor;
    
    @Column(name="amount")
    private BigDecimal amount;
    
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="currency")
    private Currency currency;
    
    @Column(name="returninvestor")
    private String returninvestor;
    
    @Column(name="expiration")
    private String expiration;
    
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="user")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjecttitle() {
        return projecttitle;
    }

    public void setProjecttitle(String projecttitle) {
        this.projecttitle = projecttitle;
    }

    public String getProjeclocation() {
        return projeclocation;
    }

    public void setProjeclocation(String projeclocation) {
        this.projeclocation = projeclocation;
    }

    public String getPromotername() {
        return promotername;
    }

    public void setPromotername(String Promotername) {
        this.promotername = Promotername;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Phase getPhase() {
        return phase;
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String getElevatorpitch() {
        return elevatorpitch;
    }

    public void setElevatorpitch(String elevatorpitch) {
        this.elevatorpitch = elevatorpitch;
    }

    public String getIdealinvestor() {
        return idealinvestor;
    }

    public void setIdealinvestor(String idealinvestor) {
        this.idealinvestor = idealinvestor;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getReturninvestor() {
        return returninvestor;
    }

    public void setReturninvestor(String returninvestor) {
        this.returninvestor = returninvestor;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
}