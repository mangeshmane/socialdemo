package mobi.puut.service.entities.users;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@Table(name="cryptoinfo")
public class Cryptoinfo implements Serializable{

	private static final long serialVersionUID=1L;
	
	@Id
	@Column(name="id",unique=true,nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Column(name="code")
	private String code;
	
	@NotNull
	@Column(name="address")
	private String address;
	
	@Column(name="main")
	private Boolean main=Boolean.FALSE;
	
	@Transient
	private byte[] cryptologo;

	public byte[] getCryptologo() {
		return cryptologo;
	}

	public void setCryptologo(byte[] cryptologo) {
		this.cryptologo = cryptologo;
	}
	
	public Boolean getMain()
	{
		return main;
	}
	
	public void setMain(Boolean main)
	{
		this.main=main;
	}
	
	@NotNull
	@Column(name="currency")
	private String currency;
	
	@NotNull
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="user")
	private User user;
	
	@NotNull
	@Column(name="identifier")
	private String identifier;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

}
