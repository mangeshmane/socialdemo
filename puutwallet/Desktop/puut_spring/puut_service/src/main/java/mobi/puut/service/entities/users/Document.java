package mobi.puut.service.entities.users;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.documents.Availability;
import mobi.puut.service.entities.documents.Btempimg;
import mobi.puut.service.entities.documents.Category;
import mobi.puut.service.entities.documents.Program;
import mobi.puut.service.entities.documents.Rtempimg;
import mobi.puut.service.entities.documents.Status;
import mobi.puut.service.entities.documents.Subtype;
import mobi.puut.service.entities.documents.Tempimg;
import mobi.puut.service.entities.geographic.Nationality;
import mobi.puut.service.entities.documents.Type;



@Entity
@Table(name="documents")
public class Document implements Serializable {

	private static final long serialVersionUID=1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="type")
	private mobi.puut.service.entities.documents.Type type;
	
	
	@Column(name="name")
	private String name;
	
	@Column(name="holder")
	private String holder ;
	
	
	@Column(name="number")
	private String number;
	
	
	@Column(name="cvv")
	private String cvv;
	

	@Column(name="validfrom")
	private Date validfrom;
	
	
	@Column(name="validtill")
	private Date validtill;
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="country")
	private Country country;
	
	@Column(name="city")
	private String city;
	
	
	@Column(name="designation")
	private String designation;
	
	
	@Column(name="companyname")
	private String companyname;
	
	@Column(name="companyaddress")
	private String companyaddress;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="mobile")
	private String mobile;
	
	@Column(name="fax")
	private String fax;
	
	
	@Column(name="email")
	private String email;
	
	@Column(name="authority")
	private String authority;
	
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="subtype")
	private Subtype subtype;
	

	@Column(name="quantity")
	private Integer quantity;
	
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="category")
	private Category category;
	
	
	
	@Column(name="itinerary")
	private String itinerary;
	
	@Column(name="units")
	private String units;
	
	@Column(name="seats")
	private String seats;
	
	@Column(name="rate")
	private Integer rate;
	
	
	@Column(name="vaccinationname")
	private String vaccinationname;
	

	@Column(name="vaccinationdate")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date vaccinationdate;
	
	@Column(name="vaccinationcode")
	private String vaccinationcode;
	
	
	@Column(name="birthdate")
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date birthdate;
	
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="nationality")
	private Nationality nationality;

	@Column(name="debtor")
	private String debtor;
	
	
	@Column(name="creaditor")
	private String creaditor;
	
	
	
	@Column(name="vatnumber")
	private String vatnumber;
	
	
	@Column(name="invoicenumber")
	private String invoicenumber;
	
	@Column(name="amount")
	private String amount;
	
	

	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="currency")
	private Currency currency;
	
	@Column(name="payment")
	private Integer payment;
	
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="status")
	private Status status;
	
	
	@Column(name="dateofissue")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date dateofissue;
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="availability")
	private Availability availability;
	
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="client")
	private User user;
	
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="merchant")
	private User merchant;
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="partner")
	private User partner;
	
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="program")
	private Program program;
	
	
	@Transient
	private String userpuutnumber;
	
	
	@Transient
	private String userlogin;
	
	@Transient
	private Long countryId;
	
	@Transient
	private Long typeId;
	
	
	@Transient
	private Long subtypeId;
	
	@Transient
	private Long categoryId;
	
	@Transient
	private Long nationalityId;
	
	
	@Transient
	private Long currencyId;
	
	@Transient
	private Long backgroundId;
	
	@Transient
	private Long bBackgroundId;
	

	@Transient
	private Long rBackgroundId;
	
	@Transient
	private Date vfrom;
		
	
	
	@Column(name="shared",nullable=false,columnDefinition="TINYINT(1)")
	private Boolean shared;
	
	
	@Column(name="firstname")
	private String firstname;
	
	@Column(name="lastname")
	private String lastname;
	
	@Column(name="venue")
	private String venue;
	
	@Column(name="shop")
	private String shop;
	
	@Column(name="title")
	private String title;
	
	@Column(name="airline")
	private String airline;
	
	@Column(name="address")
	private String address;
	
	
	@Column(name="docdate")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date docdate;
	
	@Column(name="departure")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date departure;
	
	
	@Column(name="boarding")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date boarding;
	
	@Column(name="location")
	private String location;
	
	
	@Column(name="price")
	private String price;
	
	
	@Column(name="flightnumber")
	private String flightnumber;

	@Column(name="totalamount")
	private String totalamount;
	

	@Column(name="gate")
	private String gate;
	
	@Column(name="vatamount")
	private String vatamount;
	
	
	@Column(name="duedate")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date duedate;
	
	@Column(name="image")
	private byte[] image;
	
	@Column(name="photo")
	private byte[] photo;
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="tempimg")
	private Tempimg tempimg;
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="btempimg")
	private Btempimg btempimg;
	
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="rtempimg")
	private Rtempimg rtempimg;
	
	
	@Column(name="placeofbirth")
	private String placeofbirth;
	

	@Column(name="town")
	private String town;
	
	@Column(name="web")
	private String web;
	
	@Column(name="postzipcode")
	private String postzipcode;
	
	@Column(name="companyslogan")
	private String companyslogan;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public mobi.puut.service.entities.documents.Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHolder() {
		return holder;
	}

	public void setHolder(String holder) {
		this.holder = holder;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	
	
	
	
	@XmlElement
	public Date getValidfrom() {
		if(validfrom!=null){
			return new Date(validfrom.getTime());
		}
		return null;
			
		
	}

	@XmlElement
	public String getValidFromFormatted(){
		if(validfrom==null){
			return null;
		}else {
			DateFormat df=new SimpleDateFormat("MM/yy");
			return df.format(validfrom);
		}
	}
	
	@XmlElement
	public void setValidfrom(Date validfrom) {
		if(validfrom!=null) {	
			DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
			df.format(validfrom);
		this.validfrom = new Date(validfrom.getTime());
		}
	}
	
	
	@XmlElement
	public String getValidTillFormatted(){
		if(validtill==null){
			return null;
		}else {
			DateFormat df=new SimpleDateFormat("MM/yy");
			return df.format(validtill);
		}
	}
	
	@XmlTransient
	public Date getValidtill() {
		if(validtill!=null){
			return new Date(validtill.getTime());
		}
		return null;
	}

	@XmlElement
	public void setValidtill(Date validtill) {
		if(validtill!=null) {
		this.validtill = new Date(validtill.getTime());
		}
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getCompanyaddress() {
		return companyaddress;
	}

	public void setCompanyaddress(String companyaddress) {
		this.companyaddress = companyaddress;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Subtype getSubtype() {
		return subtype;
	}

	public void setSubtype(Subtype subtype) {
		this.subtype = subtype;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getItinerary() {
		return itinerary;
	}

	public void setItinerary(String itinerary) {
		this.itinerary = itinerary;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getSeats() {
		return seats;
	}

	public void setSeats(String seats) {
		this.seats = seats;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public String getVaccinationname() {
		return vaccinationname;
	}

	public void setVaccinationname(String vaccinationname) {
		this.vaccinationname = vaccinationname;
	}

	public Date getVaccinationdate() {
		return vaccinationdate;
	}

	public void setVaccinationdate(Date vaccinationdate) {
		this.vaccinationdate = vaccinationdate;
	}

	public String getVaccinationcode() {
		return vaccinationcode;
	}

	public void setVaccinationcode(String vaccinationcode) {
		this.vaccinationcode = vaccinationcode;
	}

	@XmlTransient
	public Date getBirthdate() {
		return birthdate;
	}

	@XmlElement
	public String getBirthdateFormatted() {
		if(birthdate==null) {
			return null;
		}else
		{
			DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
			return df.format(birthdate);
		}
	}
	
	
	public void setBirthdate(Date birthdate) {
		if(birthdate !=null) {
			this.birthdate = birthdate;
		}
	}

	public String getVatnumber() {
		return vatnumber;
	}

	public void setVatnumber(String vatnumber) {
		this.vatnumber = vatnumber;
	}

	public String getInvoicenumber() {
		return invoicenumber;
	}

	public void setInvoicenumber(String invoicenumber) {
		this.invoicenumber = invoicenumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Integer getPayment() {
		return payment;
	}

	public void setPayment(Integer payment) {
		this.payment = payment;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getDateofissue() {
		return dateofissue;
	}

	public void setDateofissue(Date dateofissue) {
		this.dateofissue = dateofissue;
	}

	public Availability getAvailability() {
		return availability;
	}

	public void setAvailability(Availability availability) {
		this.availability = availability;
	}



	public User getMerchant() {
		return merchant;
	}

	public void setMerchant(User merchant) {
		this.merchant = merchant;
	}

	public User getPartner() {
		return partner;
	}

	public void setPartner(User partner) {
		this.partner = partner;
	}

	public Program getProgram() {
		return program;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public String getUserpuutnumber() {
		return userpuutnumber;
	}

	public void setUserpuutnumber(String userpuutnumber) {
		this.userpuutnumber = userpuutnumber;
	}

	public String getUserlogin() {
		return userlogin;
	}

	public void setUserlogin(String userlogin) {
		this.userlogin = userlogin;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public Long getSubtypeId() {
		return subtypeId;
	}

	public void setSubtypeId(Long subtypeId) {
		this.subtypeId = subtypeId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getNationalityId() {
		return nationalityId;
	}

	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Long getBackgroundId() {
		return backgroundId;
	}

	public void setBackgroundId(Long backgroundId) {
		this.backgroundId = backgroundId;
	}

	public Long getbBackgroundId() {
		return bBackgroundId;
	}

	public void setbBackgroundId(Long bBackgroundId) {
		this.bBackgroundId = bBackgroundId;
	}

	public Long getrBackgroundId() {
		return rBackgroundId;
	}

	public void setrBackgroundId(Long rBackgroundId) {
		this.rBackgroundId = rBackgroundId;
	}

	
	public Date getVfrom() {
		return vfrom;
	}

	public void setVfrom(Date vfrom) {
		this.vfrom = vfrom;
	}

	public Boolean getShared() {
		return shared;
	}

	public void setShared(Boolean shared) {
		this.shared = shared;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getShop() {
		return shop;
	}

	public void setShop(String shop) {
		this.shop = shop;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDocdate() {
		return docdate;
	}

	public void setDocdate(Date docdate) {
		this.docdate = docdate;
	}

	public Date getDeparture() {
		return departure;
	}

	public void setDeparture(Date departure) {
		this.departure = departure;
	}

	@XmlTransient
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getBoarding() {
		return boarding;
	}

	public void setBoarding(Date boarding) {
		this.boarding = boarding;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFlightnumber() {
		return flightnumber;
	}

	public void setFlightnumber(String flightnumber) {
		this.flightnumber = flightnumber;
	}

	public String getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(String totalamount) {
		this.totalamount = totalamount;
	}

	public String getGate() {
		return gate;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}

	public String getVatamount() {
		return vatamount;
	}

	public void setVatamount(String vatamount) {
		this.vatamount = vatamount;
	}

	public Date getDuedate() {
		return duedate;
	}

	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public Tempimg getTempimg() {
		return tempimg;
	}

	public void setTempimg(Tempimg tempimg) {
		this.tempimg = tempimg;
	}

	public Btempimg getBtempimg() {
		return btempimg;
	}

	public void setBtempimg(Btempimg btempimg) {
		this.btempimg = btempimg;
	}

	public Rtempimg getRtempimg() {
		return rtempimg;
	}

	public void setRtempimg(Rtempimg rtempimg) {
		this.rtempimg = rtempimg;
	}

	public String getPlaceofbirth() {
		return placeofbirth;
	}

	public void setPlaceofbirth(String placeofbirth) {
		this.placeofbirth = placeofbirth;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getWeb() {
		return web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getPostzipcode() {
		return postzipcode;
	}

	public void setPostzipcode(String postzipcode) {
		this.postzipcode = postzipcode;
	}

	public String getCompanyslogan() {
		return companyslogan;
	}

	public void setCompanyslogan(String companyslogan) {
		this.companyslogan = companyslogan;
	}
	
	
	
	@XmlElement
	public String getDateofissueFormatted() {
		if(dateofissue==null){
			return null;
		}
		else {
			DateFormat df=new SimpleDateFormat("MM/yy");
			return df.format(dateofissue);
		}
	}
	
	
	
	
}
