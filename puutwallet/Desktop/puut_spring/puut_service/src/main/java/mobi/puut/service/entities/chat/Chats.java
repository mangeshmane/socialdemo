/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.entities.chat;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import mobi.puut.service.entities.users.User;

/**
 * A message which is included in a conversation
 Contains a content, a date and a boolean field which indicates if the message has been deleted
 * @author Nathan Audin
 */
@Entity
//@XmlRootElement
@Table(name="chat")
public class Chats implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;
    
    @Column(name = "content", columnDefinition="VARCHAR(500)")
    private String content;
    
    @Column(name = "timesent")
    private Date messageDate;
    
    @ManyToOne
    @JoinColumn(name = "conversation")
    private Conversation conversationIdMessages;
    
    @Column(name = "deleted", columnDefinition="TINYINT(1)")
    private Boolean deleted;
    
    @ManyToOne
    @JoinColumn(name = "user")
    private User userId;
    
    @Lob
    @Column(name = "chatPicture",length=16777215)
    private  byte[] chatPicture;

    public byte[] getChatPicture() {
        return chatPicture;
    }

    public void setChatPicture(byte[] chatPicture) {
        this.chatPicture = chatPicture;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getContent(){
        return content;
    }
    
    public void setContent(String content){
        this.content = content;
    }
    
    public Date getDate(){
        return messageDate;
    }
    
    public void setDate(Date messageDate){
        this.messageDate = messageDate;
    }
    
    public Conversation getConversation(){
        return this.conversationIdMessages;
    }
    
    public void setConversation(Conversation conversationId){
        this.conversationIdMessages = conversationId;
    }
    
    public Boolean getDeleted(){
        return this.deleted;
    }
    
    public void setDeleted(Boolean deleted){
        this.deleted = deleted;
    }
    
    public User getUser(){
        return this.userId;
    }
    
    public void setUser(User userId){
        this.userId = userId;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Chats)) {
            return false;
        }
        Chats other = (Chats) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "Message{" +
                    "id='" + id + '\'' +
                    ", content='" + content + '\'' + 
                    ", messageDate='" + messageDate + '\'' + 
                    ", conversationIdMessages='" + conversationIdMessages.toString() + '\'' +
                    ", deleted='" + deleted + '\'' +    
                    ", userId='" + userId.toString() + '\'' +
                "}";
    }
}
