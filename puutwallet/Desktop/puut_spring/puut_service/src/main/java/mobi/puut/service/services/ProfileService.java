package mobi.puut.service.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.mail.MessagingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.Language;
import mobi.puut.service.entities.users.Login;
import mobi.puut.service.entities.users.Preference;
import mobi.puut.service.entities.users.User;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.multipart.MultipartFile;

public interface ProfileService {

    public User getProfile() throws IOException;

    public Result editProfile(User request) throws UnsupportedEncodingException, NoSuchAlgorithmException;

    public Login checkLogin();

    public Result remindPassword(String login) throws MessagingException;

    public AccountSetting getProfileSettings();

    public Result saveProfileSettings(AccountSetting settings);

    public Preference getProfilePreference();

    public Result saveProfilePreference(Preference preference);

    public List<Language> getLanguages();

    public Response uploadAccountPicture(MultipartFile uploadedInputStream) throws IOException;

    public Response uploadUploadIds(MultipartFile uploadedInputStream) throws IOException;

    public Response uploadUbill(MultipartFile uploadedInputStream) throws IOException;

    public Response uploadSocialcs(MultipartFile uploadedInputStream) throws IOException;

    public Response uploadCoverimage(MultipartFile uploadedInputStream) throws IOException;

    public Response uploadDlicenses(MultipartFile uploadedInputStream) throws IOException;

    public Response uploadPassports(MultipartFile uploadedInputStream) throws IOException;

    public Result updatePin(User user);

    public Result updatePassword(User user);


    public Result verifyPin(String pin);

	public Result remindPassword1(User user);
}
