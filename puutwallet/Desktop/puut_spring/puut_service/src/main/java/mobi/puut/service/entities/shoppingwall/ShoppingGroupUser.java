package mobi.puut.service.entities.shoppingwall;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import mobi.puut.service.entities.users.User;

@Entity
//@XmlRootElement
@Table(name="shoppinggroupusers")
public class ShoppingGroupUser implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="shoppinggroup")
    private ShoppingGroup shoppinggroup;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="user")
    private User user;
    
    @Column(name="moderator")
    private boolean moderator;

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
    
    @Column(name="approved")
    private boolean approved;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShoppingGroup getShoppinggroup() {
        return shoppinggroup;
    }

    public void setShoppinggroup(ShoppingGroup shoppinggroup) {
        this.shoppinggroup = shoppinggroup;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
         public boolean isModerator() {
        return moderator;
    }

    public void setModerator(boolean moderator) {
        this.moderator = moderator;
    }
}