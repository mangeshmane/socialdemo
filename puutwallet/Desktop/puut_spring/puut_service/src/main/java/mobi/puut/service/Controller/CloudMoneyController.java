/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import javax.ws.rs.core.MediaType;
import mobi.puut.service.entities.cloudmoney.CloudMoney;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.BankWithdrawal;
import mobi.puut.service.entities.shoppingwall.Transaction;
import mobi.puut.service.entities.users.Document;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.CloudMoneyService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Development
 */
@RestController
@RequestMapping("/cloudmoney")
public class CloudMoneyController {

    @Autowired
    private CloudMoneyService cloudMoneyService;

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/currencies", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Currency> getCurrencies() {
        try {
            return cloudMoneyService.getCurrencies();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CloudMoneyController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/agents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<User> getAgents() {
        try {
            return cloudMoneyService.getAgents();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CloudMoneyController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/balance", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<CloudMoney> getAccountBalance() {
        try {
            return cloudMoneyService.getAccountBalance();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CloudMoneyController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Transaction> getTransactions() {
        try {
            return cloudMoneyService.getTransactions();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CloudMoneyController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/unpaidbills", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Document> getBills() {
        try {
            return cloudMoneyService.getBills();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CloudMoneyController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/validcreditcards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Document> getCreditCards() {
        try {
            return cloudMoneyService.getCreditCards();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CloudMoneyController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/send", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
    public Result sendMoney(@RequestBody Transaction transaction) {
        try {
            return cloudMoneyService.sendMoney(transaction);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CloudMoneyController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/withdraw", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
    public Result withdrawMoney(@RequestBody BankWithdrawal bankWithdrawal) throws IOException {
        try {
            return cloudMoneyService.withdrawMoney(bankWithdrawal);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CloudMoneyController.class).error(exception);
            throw new Error(exception);
        }

    }

}
