
package mobi.puut.service.data.impl.chat;

import java.util.List;
import javax.annotation.PostConstruct;
import mobi.puut.service.data.def.chat.BlockIntervalData;
import mobi.puut.service.entities.chat.BlockInterval;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;


@Repository
public class BlockIntervalDataImpl implements BlockIntervalData{

    @Autowired
    private SessionFactory sessionFactory;
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
 
    @Override
    @Transactional(rollbackFor=Exception.class)
    public void save(BlockInterval blockInterval) {
        //sessionFactory.getCurrentSession().persist(blockInterval);
        sessionFactory.getCurrentSession().saveOrUpdate(blockInterval);
    }
    
    @Override
    @Transactional(rollbackFor=Exception.class)
    public void update(BlockInterval blockInterval) {
        sessionFactory.getCurrentSession().update(blockInterval);
    } 
        
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public BlockInterval find(long id){
        return (BlockInterval) sessionFactory.getCurrentSession().get(BlockInterval.class, id);
    }
    
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<BlockInterval> findAll() {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT b FROM BlockInterval b")
                .list();
    }

    /**
     * To find the active blocking intervals for two given users
     * @param userId1 First user
     * @param userId2 Second user
     * @return The active interval if it exists
     */
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<BlockInterval> findCurrentByUsersIds(long userId1, long userId2) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT b FROM BlockInterval b WHERE b.usertouserId.user1Id.id = :userId1 AND b.usertouserId.user2Id.id = :userId2 AND b.timeUnblocked IS NULL")
                .setParameter("userId1", userId1)
                .setParameter("userId2", userId2)
                .list();
    }

    /**
     * To find all the blocking intervals for two given users
     * @param userId1 First user
     * @param userId2 Second user
     * @return All the blocking intervals between these two users
     */
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<BlockInterval> findAllByUsersIds(long userId1, long userId2) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT b FROM BlockInterval b WHERE b.usertouserId.user1Id.id = :userId1 AND b.usertouserId.user2Id.id = :userId2")
                .setParameter("userId1", userId1)
                .setParameter("userId2", userId2)
                .list();        
    }
    
    @Override
     @Transactional(readOnly = true, rollbackFor=Exception.class)
    public BlockInterval findByRelation(long usertouserId) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT b FROM BlockInterval b WHERE b.usertouserId.id =:usertouserId",BlockInterval.class)
                .setParameter("usertouserId", usertouserId)
                .uniqueResult();
    }
    
}
