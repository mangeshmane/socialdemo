package mobi.puut.service.entities.crypto.btc;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SendMoney {

	String address;
	String amount;
	String phone;
	
	public SendMoney(String amount,String address) {
		this.address=address;
		this.amount=amount;
		this.phone=phone;
	}
	
	public SendMoney()
	{
		
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	
}
