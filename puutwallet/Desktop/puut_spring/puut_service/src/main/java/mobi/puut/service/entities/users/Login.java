package mobi.puut.service.entities.users;

import javax.xml.bind.annotation.XmlRootElement;
import mobi.puut.service.entities.users.Role;

//@XmlRootElement
public class Login {
    private Role role;
    private Language language;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}