
package mobi.puut.service.data.impl.chat;

import java.util.List;
import javax.annotation.PostConstruct;
import mobi.puut.service.data.def.chat.ConversationData;
import mobi.puut.service.entities.chat.Conversation;
import mobi.puut.service.entities.users.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;


@Repository
public class ConversationDataImpl implements ConversationData{
    
    @Autowired
    private SessionFactory sessionFactory;

    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
    
    @Override
    @Transactional(rollbackFor=Exception.class)
    public void save(Conversation c) {
        sessionFactory.getCurrentSession().persist(c);
    }

    
    @Override
    @Transactional(rollbackFor=Exception.class)
    public void update(Conversation c) {
        sessionFactory.getCurrentSession().update(c);
    }
    
    
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public Conversation find(long id) {
        return (Conversation)sessionFactory.getCurrentSession().get(Conversation.class, id);
    }

    
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Conversation> findAll() {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT c FROM Conversation c")
                .list();
    }
    
    /**
     * To find conversations with a name
     * @param conv The conversation name
     * @return A list of conversations
     */
    
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Conversation> findByName(String conv) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT c FROM Conversation c WHERE c.name = :name AND c.owner IS NULL").setParameter("name", conv)
                .list();
    }
    
    /**
     * To find a conversation with a name and an owner
     * @param conv The conversation name
     * @param owner The owner of the conversation
     * @return The corresponding conversation (as a list)
     */
    
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Conversation> findByNameAndOwner(String conv, Long owner) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT c FROM Conversation c WHERE c.name = :name AND c.owner.id = :owner")
                .setParameter("name", conv).setParameter("owner", owner)
                .list();
    }
    
    /**
     * Find conversations for a given term
     * @param term The term that we want to find
     * @return A list of conversations which can correspond to the term parameter
     */
    
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Conversation> findConvByTerm(String term) {       
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT c FROM Conversation c WHERE c.name LIKE :name").setParameter("name", "%"+term+"%")
                .list();
    }
    
    /**
     * Find conversations for a given term
     * @param term The term that we want to find
     * @return A list of conversations which can correspond to the term parameter
     */
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Conversation> findParticipantsByTerm(String term) {       
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT c FROM Conversation c JOIN c.participants cp WHERE cp.userId.firstname LIKE :name OR cp.userId.lastname LIKE :name").setParameter("name", term)
                .list();
    }

    
    @Override
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Conversation> findConversationByUser(User user) {
        List<Conversation> con = sessionFactory.getCurrentSession()
                .createQuery("SELECT c FROM Conversation c WHERE c.owner=:user").setParameter("user", user)
                .list();
        return con;
    }


}