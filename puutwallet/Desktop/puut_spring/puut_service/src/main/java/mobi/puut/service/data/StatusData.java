package mobi.puut.service.data;

import java.util.List;

import mobi.puut.service.entities.users.Cryptostatus;
import mobi.puut.service.entities.users.TransactionTypes;

public interface StatusData {

	List<Cryptostatus> getByWalletId(long id);

	TransactionTypes getTransactionTypeById(long l);

	Cryptostatus saveStatus(Cryptostatus status);

	Cryptostatus getByTransaction(String object, Long walletid);
	
	
	

}