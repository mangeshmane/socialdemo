package mobi.puut.service.data;

import java.util.List;

import mobi.puut.service.entities.users.Cryptoinfo;
import mobi.puut.service.entities.users.Secure;
import mobi.puut.service.entities.users.User;

public interface WalletInfoData {

	//Cryptoinfo getById(final Long id);
	List<Cryptoinfo> getAddressByUser(User user);
	Cryptoinfo create(String code, String currency, String address, User user, Boolean isDefaultAddress,
			String identifier);
	
	Cryptoinfo getWalletById(long id);
	boolean checkWalletAndUser(long id, User user);
	Cryptoinfo getByAddress(String address);
	public List<Secure> getSecureByUser(User user);
	
	public void updateSecure(Secure secure);
	public Cryptoinfo getDefaultWallet(User phoneUser);
	public void deleteWalletInfoByWalletId(Long id);
	 public List<Cryptoinfo> getAllWallets();
	 public Cryptoinfo getWalletInfoByCurrencyAndAddress(String currency, String address);
  public Cryptoinfo getById(Long id);
  public Cryptoinfo updateWallet(Cryptoinfo cryptoInfo);
  public Boolean checkWalletAndUser(Long id,User user);
  public Long getWalletIdByddress(String address);
}
