package mobi.puut.service.entities.shoppingwall;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import mobi.puut.service.entities.users.User;

@Entity
//@XmlRootElement
@Table(name="messages")
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
    @Column(name="title")
    private String title;
    
    @Column(name="body")
    private String body;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="userfrom")
    private User userfrom;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="userto")
    private User userto;
    
    @Column(name="messagedate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date messagedate;
    
    @Column(name="support")
    private Boolean support;

    @Column(name="readmessage")
    private Boolean readmessage;    

    public Boolean getReadmessage() {
        return readmessage;
    }

    public void setReadmessage(Boolean readmessage) {
        this.readmessage = readmessage;
    }
    
    public Boolean getSupport() {
        return support;
    }

    public void setSupport(Boolean support) {
        this.support = support;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlTransient
    public Date getMessagedate() {
        return new Date(messagedate.getTime());
    }
    
    @XmlElement
    public String getMessageDateFormatted() {
        if (messagedate == null) {
            return null;
        }
        else {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            return df.format(messagedate);
        }
    }

    public void setMessagedate(Date messagedate) {
        this.messagedate = new Date(messagedate.getTime());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @XmlTransient
    public User getUserfrom() {
        return userfrom;
    }
    
    @XmlElement
    public String getUserFrom() {
        return userfrom.getUsername();
    }

    public void setUserfrom(User userfrom) {
        this.userfrom = userfrom;
    }

    @XmlTransient
    public User getUserto() {
        return userto;
    }

    public void setUserto(User userto) {
        this.userto = userto;
    }
    
    @XmlElement
    public String getUserTo() {
        return userto.getUsername();
    }
    
    public void setUserTo(String login) {
        this.userto = new User();
        this.userto.setUsername(login);
    }
}