package mobi.puut.service;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class ImageUtil {
    
    public static BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height){
        int originalWidth = originalImage.getWidth();
        int originalHeight = originalImage.getHeight();
        
        Logger.getLogger(ImageUtil.class).error("Image height: " + height);
        Logger.getLogger(ImageUtil.class).error("Image width: " + width);
        
        float widthRatio = originalWidth / width;
        float heightRatio = originalHeight / height;
        
        if (widthRatio > heightRatio) {
            float newThumbnailHeight = (float)originalHeight / (float)originalWidth * width;
            height = (int)newThumbnailHeight;
        } else {
            float newThumbnailWidth = (float)originalWidth / (float)originalHeight * height;
            width = (int)newThumbnailWidth;
        }
        
	//BufferedImage resizedImage = new BufferedImage(width, height, type);
        BufferedImage resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	Graphics2D g = resizedImage.createGraphics();
        g.setComposite(AlphaComposite.Src);
	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	RenderingHints.VALUE_INTERPOLATION_BICUBIC);
//        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
//	RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	g.setRenderingHint(RenderingHints.KEY_RENDERING,
	RenderingHints.VALUE_RENDER_QUALITY);
	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	RenderingHints.VALUE_ANTIALIAS_ON);
	g.drawImage(originalImage, 0, 0, width, height, null);
	g.dispose();
        
	return resizedImage;
    }
    
    public static BufferedImage rotate(BufferedImage bi, int screen_width, int screen_height)
    {
            int width = bi.getWidth();
            int height = bi.getHeight();
            
            int screen_dif = screen_width - screen_height;
            int dif = width - height;
            
            if ((screen_dif > 0 && dif < 0) || (screen_dif < 0 && dif > 0)) {
                BufferedImage biFlip = new BufferedImage(height, width, bi.getType());

                for(int i=0; i<width; i++) {
                    for(int j=0; j<height; j++) {
                        biFlip.setRGB(j, width-1-i, bi.getRGB(i, j));
                    }
                }

                return biFlip;
            } else {
                return bi;
            }
    }
    
    public static void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) {
        try {
            OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
            int read;
            byte[] bytes = new byte[1024];

            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            
            out.flush();
            out.close();
        } catch (IOException e) {
        }
    }
    
    public byte[] getFormattedImage(byte[] img, mobi.puut.service.entities.users.Document document) {
        try {
            InputStream in = new ByteArrayInputStream(img);
            URL url = new URL("");
            URL chip = new URL("https://puutwallet.com/resources/images/chip.png");
            BufferedImage image = ImageIO.read(in);
            BufferedImage photo = ImageIO.read(url);
            BufferedImage chip_image = ImageIO.read(chip);
            url.getPath();
            Graphics2D gfx = (Graphics2D) image.getGraphics();
            
            gfx.drawImage(photo, null, 20, 10);
            gfx.drawImage(chip_image, null, 20, 50);
            gfx.setColor(Color.WHITE);      
            gfx.setFont(new Font("Tahoma", Font.LAYOUT_LEFT_TO_RIGHT, 24));
            gfx.drawString(document.getNumber(), 20, 130);
         
            gfx.setFont(new Font("Tahoma", Font.PLAIN, 20));
            gfx.drawString(document.getUser().getFirstname()
                    + " " + document.getUser().getLastname(), 20, 180);
            
            gfx.setFont(new Font("Tahoma", Font.BOLD, 14));
            gfx.drawString("Val From" , 183, 155);
            gfx.drawString("Exp On" , 260, 155);
            
            gfx.setFont(new Font("Tahoma", Font.PLAIN, 20));
            gfx.drawString(document.getValidFromFormatted(), 187, 180);
            gfx.drawString("-" , 245, 180);
            gfx.drawString(document.getValidTillFormatted(), 258, 180);

            ByteArrayOutputStream bas = new ByteArrayOutputStream();
            ImageIO.write(image, "png", bas);
            bas.flush();
            byte[] result = bas.toByteArray();
            bas.close();

            return result;
        } catch (IOException ex) {
            Logger.getLogger(ImageUtil.class).error(ex);
            return null;
        }
    }
}