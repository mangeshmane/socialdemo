package mobi.puut.service.Controller;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.security.auth.login.LoginException;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.entities.users.UserRole;
import org.apache.commons.lang.time.DateUtils;

@RestController
public class LoginController {

	 @Autowired
	 private UserData userdata;
	 
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public Map authenticate(@RequestBody User user) throws LoginException {
		String username = null;
		String password = null;
		String exceptionMessege = null;		
		username = user.getUsername();
        password = user.getPassword();        
        UserDetails uDetails=null;
        String token = null;
		Map<String, Object> tokenMap = new HashMap<String, Object>();
		
        user = userdata.getByUsername(username);

        if (user == null) {
            exceptionMessege = "no user found for given user name";
            throw new LoginException(exceptionMessege);

        } else {
            uDetails=verifyUsingUsernamePassword(username, password, user);
        }
		
		
        if(uDetails != null) {
        	
        	int addMinuteTime =100;
			Date ExpirationTime = new Date(); //now
			ExpirationTime = DateUtils.addMinutes(ExpirationTime, addMinuteTime);
			token = Jwts.builder().setSubject(user.getUsername()).claim("roles", uDetails.getAuthorities())
					.setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, "secretkey").setExpiration(ExpirationTime).compact();
			user.setAuthtoken(token);
			userdata.edit(user);
			tokenMap.put("token", token);
			tokenMap.put("user", uDetails);
			
			
			
        }
        
		
		return tokenMap;
	}
	
	
	
	@RequestMapping(value="/logout",method=RequestMethod.POST)
	public Result logout() {
		
		Result result=new Result();
		
		String username=SecurityContextHolder.getContext().getAuthentication().getName();
		User user=userdata.getByUsername(username);
		
		if(user!=null) {
			user.setAuthtoken(null);
			userdata.edit(user);
			result.setSuccess(Boolean.TRUE);
			Jwts.builder().setExpiration(new Date());
			return result;
		}else {
			
			result.setSuccess(Boolean.FALSE);
			result.setError(username+"Not found");
			return result;
		}
		
	}
	
	
	 public String getMD5(String input) {
	        byte[] source;
	        try {
	            //Get byte according by specified coding.
	            source = input.getBytes("UTF-8");
	        } catch (UnsupportedEncodingException e) {
	            source = input.getBytes();
	        }
	        String result = null;
	        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7',
	                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
	        try {
	            MessageDigest md = MessageDigest.getInstance("MD5");
	            md.update(source);
	            //The result should be one 128 integer
	            byte temp[] = md.digest();
	            char str[] = new char[16 * 2];
	            int k = 0;
	            for (int i = 0; i < 16; i++) {
	                byte byte0 = temp[i];
	                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
	                str[k++] = hexDigits[byte0 & 0xf];
	            }
	            result = new String(str);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return result;
	    }
	
	private UserDetails verifyUsingUsernamePassword(String username,String password,User user)
    {
        UserDetails ud=null;
        if(user.getName().equals(username) && user.getPassword().equals(getMD5(password)))
        {
            ud=buildUserFromUserEntity(user);
        }
        return  ud;
    }
    
    
    
  private UserDetails buildUserFromUserEntity(mobi.puut.service.entities.users.User userEntity)
  {
      Set<GrantedAuthority> authorites=new HashSet<GrantedAuthority>();
      UserRole role=userdata.getRole(userEntity);
      GrantedAuthority grantedAuthority=new SimpleGrantedAuthority(role.getRole().getName());
      authorites.add(grantedAuthority);
      
      String username=userEntity.getName();
      String password=getMD5(userEntity.getPassword());
      org.springframework.security.core.userdetails.User springUser=new org.springframework.security.core.userdetails.User(username, password, authorites);
      
      return  springUser;
  }
}