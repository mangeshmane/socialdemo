package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mobi.puut.service.config.RPCClient;

@RestController
@RequestMapping("/rpc")
public class RPCController {

	@Autowired
	RPCClient client;
	
	@PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/generateAddress", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public String generate() {
        try {
            return client.getNewAddress("address-label");
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ChatController.class).error(exception);
            throw new Error(exception);
        }

    }
	
	@PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/balance/{address}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public Double getBalance(@PathVariable(value = "address") String address) {
        try {
        	String account = client.getAccount(address);
            return client.getBalance(account);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ChatController.class).error(exception);
            throw new Error(exception);
        }

    }
}
