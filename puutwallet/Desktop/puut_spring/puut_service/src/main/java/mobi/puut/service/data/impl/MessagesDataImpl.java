package mobi.puut.service.data.impl;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import mobi.puut.service.data.MessagesData;
import mobi.puut.service.entities.chat.Chats;
import mobi.puut.service.entities.chat.Conversation;
import mobi.puut.service.entities.shoppingwall.Message;
import mobi.puut.service.entities.users.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Repository
public class MessagesDataImpl implements MessagesData {
    
    @Autowired
    SessionFactory sessionFactory;
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Transactional(rollbackFor=Exception.class)
    public List<Message> getInboxMessages(User user) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<Message> messages = session.createQuery("from Message as message where message.userto=:userto order by message.messagedate")
                .setParameter("userto", user)
                .getResultList();
        
        return messages;
    }

    @Transactional(rollbackFor=Exception.class)
    public List<Message> getSentMessages(User user) {
        this.init();
        
        
        Session session = sessionFactory.getCurrentSession();
        
        List<Message> messages = session.createQuery("from Message as message where message.userfrom=:userfrom order by message.messagedate")
                .setParameter("userfrom", user)
                .getResultList();
        
        return messages;
    }

    @Transactional(rollbackFor=Exception.class)
    public void add(Message message) {
        this.init();
        
        
        sessionFactory.getCurrentSession()
                .save(message);
    }

    @Override
    public List<Chats> findByConversationId(Long convId){
        this.init();
        Session session = sessionFactory.getCurrentSession();
        Conversation conversation =  session.createQuery("from Conversation as con where con.id=:convId",Conversation.class).setParameter("convId", convId).uniqueResult();
        List<Chats> messages = session.createQuery("from Chats as chat where chat.conversationIdMessages=:conversation")
                .setParameter("conversation", conversation)
                .getResultList();
        
        return messages;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public List<Message> getUnreadMessages(User user) {
         this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<Message> messages = session.createQuery("from Message as message where message.userto=:userto and message.readmessage=:readmessage order by message.messagedate")
                .setParameter("userto", user)
                .setParameter("readmessage", Boolean.FALSE)
                .getResultList();
        
        return messages;   
    }
    
}