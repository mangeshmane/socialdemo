import React from 'react';
import { shallow } from 'enzyme';
import RoutComp from './RoutComp';

describe('<RoutComp />', () => {
  test('renders', () => {
    const wrapper = shallow(<RoutComp />);
    expect(wrapper).toMatchSnapshot();
  });
});
