import React, { Component } from 'react';

import { BrowserRouter as Router, Switch, Link, Route } from 'react-router-dom';
import Register from '../Register/Register';
import Login from '../Login/Login';
class RoutComp extends Component {
  render() {
    return <div>
      <Router>
        <Route exact path="/login" component={Login} />
        <Route path="/register" component={Register} />
      </Router>

    </div>;
  }
}

export default RoutComp;
