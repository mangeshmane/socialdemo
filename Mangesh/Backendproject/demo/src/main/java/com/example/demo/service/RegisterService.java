package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.User;

@Service
public interface RegisterService {


	public User save(User usr);
	public void delete(int id);
	public List<User> getUsers();
}
