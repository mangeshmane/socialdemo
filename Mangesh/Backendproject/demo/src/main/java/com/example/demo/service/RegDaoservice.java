package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.RegDao.RegistrationDao;
import com.example.demo.model.User;

@Component
public class RegDaoservice implements RegisterService{
	
	@Autowired
   private RegistrationDao dao;
	

	public User save(User usr) {
		return dao.save(usr);
			
	}

	@Override
	public void delete(int id)  {
		 dao.deleteById(id);
	}

	@Override
	public List<User> getUsers() {
		
		return (List<User>) dao.findAll();
	}
	
}
