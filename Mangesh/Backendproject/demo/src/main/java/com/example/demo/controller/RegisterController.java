package com.example.demo.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.model.User;

import com.example.demo.service.RegisterService;



@Controller
@CrossOrigin(origins="http://localhost:4200",allowedHeaders = "*")
public class RegisterController {
	
	@Autowired
	private RegisterService registrationService; 
	
	
	
	@RequestMapping("/api")
	public String registration() {
		System.out.println("Register Here");
		return "app.html";
		
	}
	@RequestMapping(value="registration", method= RequestMethod.POST)	
	public User registration(@RequestBody User usr) {
		return registrationService.save(usr);

		}
	
	
	 @DeleteMapping(value="/Regstration/{id}")
	    public void delete(@PathVariable("id") int id,User usr) {
		 registrationService.delete(id);
	    }

	
	 @GetMapping(value="/getAllUser")
	 public List<User> getUsers(){
		 return registrationService.getUsers();
		 
		 
	 }
	 
	 
	 
	 
	 
	 
	 
	 
}

