package com.social.demo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.social.demo.dao.UserDao;
import com.social.demo.model.User;

@Component
public class UserDaoService implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Override
	public User save(User user) {
	
		return userDao.save(user)  ;
	}

	@Override
	public void delete(int id) {
		userDao.deleteById(id);
		
	}

	@Override
	public User update(User user) {
		return null;
	}
	public List<User> getUsers(){
		return null;
		
	}

}
