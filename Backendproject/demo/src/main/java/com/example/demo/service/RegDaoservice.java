package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.model.User;

@Component
public class RegDaoservice implements RegisterService {

	@Autowired
	private com.example.demo.registrationDao.RegistrationDao dao;

	public User save(User user) {
		return dao.save(user);

	}

	@Override
	public void delete(int id) {
		dao.deleteById(id);
	}

	@Override
	public List<User> getUsers() {

		return (List<User>) dao.findAll();
	}

	@Override
	public User update(User user) {
		return dao.save(user);
	}

	
}
