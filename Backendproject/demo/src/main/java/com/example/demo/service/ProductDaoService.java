package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.model.Product;
import com.example.demo.registrationDao.ProductDao;
@Component
public class ProductDaoService implements ProductService {

	@Autowired
	ProductDao dao;
	
	@Override
	public Product addProduct(Product product) {
		
		return dao.save(product);
	}

}
