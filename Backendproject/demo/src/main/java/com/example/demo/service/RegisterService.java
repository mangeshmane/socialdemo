package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.model.User;

@Service
public interface RegisterService {
	
	public User save(User user);
	public void delete(int id);
	public List<User> getUsers();
	public User update(User user);
//	public Optional<User> findByUsername(User user);
	
		
	}
