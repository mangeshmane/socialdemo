package com.example.demo.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;

import com.example.demo.service.RegisterService;
//@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")



@RestController
public class RegisterController {

	@Autowired
	private RegisterService registrationService;

	@RequestMapping("/api")
	public String registration() {
		System.out.println("Register Here");
		return "app.html";
	}

	@PostMapping("/saveUser")
	public User registration(@RequestBody User usr) {
		return registrationService.save(usr);
	}

	@DeleteMapping("/deleteUser/{id}")
	public void delete(@PathVariable("id") int id) {
		registrationService.delete(id);
	}

	@GetMapping("/getAllUser")
	public List<User> getUsers() {
		return registrationService.getUsers();
	}

	@PutMapping("/updateUser")
	public User update(@RequestBody User user) {
		return registrationService.update(user);
	}
	
	
//	@GetMapping("/Login")
//	public User login(User user) {
//		Optional<User> tempUser = registrationService.findByUsername(user);
//		if(user.getUserName()==)) {
//			return user;
//		}
//		return null;
//	}
	
//	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
//	public ResponseEntity<Map<String, Object>> login(@RequestBody User user,
//			HttpServletResponse response) throws IOException {
//
//		
//		String deviceType = "unknown";
//		
//	            try {
//	    			String token = null;
//	    			Map<String, Object> tokenMap = new HashMap<String, Object>();
//	    			User appUser = registrationService.findByUsernameAndPassword(user.getUserName(), user.getPassword());
//	    			
//	    			if (appUser != null) {
//	    				if (appUser.isVerify() == true) {
//	    					
//	    					token = Jwts.builder().setSubject(user.getUserName()).claim("roles", appUser.getRole())
//	    							.setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, "secretkey")
//	    							.compact();
//	    					tokenMap.put("token", token);
//	    					tokenMap.put("user", appUser);
//	    					//result.setResult(tokenMap);
//	    					
//	    		}
//	    		System.out.println("Hello " + deviceType + " browser!");
//	    		//return ResponseEntity.ok(result);
//	        
//	    			
//	    		//return ResponseEntity.ok(result);
//	    		
//	        }
//		
//	}catch(Exception e) {
//		System.out.println(e);
//		}
//	            return null;
//	}

}
